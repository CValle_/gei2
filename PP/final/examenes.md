# Exámenes
### Examen enero 2023
2. Realice una implementación recursiva terminal de la forma más sencilla posible para la función drop: int -> 'a list -> 'a list de modo que drop n l devuelva la lista que resulta de descartar los n primeros elementos de l

```
let rec drop n l = 
    if n = 0 then l else
    match l with
        h::t -> drop (n-1) t;;
```

3. Realice una implementación recursiva terminal de la forma más sencilla posible para la función take: int -> 'a list -> 'a list de modo que take n l devuelva la lista de los n primeros elementos de l. Si n es negativo debe provocarse la excepción invalid argument 'take'. Si l no tiene los suficientes elementos debe provocarse la excepción Failure 'take'.

```
let take n l = 
    let rec loop aux n l = match l with
        [] -> if n = 0 then List.rev aux
            else raise (Invalid_argument "take")
        | h::t -> if n < 0 then 
                        raise (Invalid_argument "take") 
                  else
                        if n = 0 then
                            List.rev aux
                        else
                            loop (h::aux) (n-1) t
    in loop [] n l;;
```

4. Considere la función ord a continuación de modo imperativo:

```
let ord l =
    if l = [] then
        true 
    else
        let l = ref l in
            while List.tl !l <> [] && List.hd !l <= List.hd (List.tl !l) 
            do
                l := List.tl !l
            done;
            List.length !l = 1;;
```

(List.tl = t, List.hd = h1. List.hd(List.tl) = h2)

- Cuál es el tipo?
` val ord : 'a list -> bool = <fun>`

- Defina ord de modo funcional

```
let rec ord = function
    [] -> true |
    h1::h2::t -> if (h1 <= h2) then 
                    ord (h2::t) 
                 else 
                    false|
    _ -> true;;
```

- Es recursiva terminal?

Si por que la operación de concatenar no se deja como cuenta pendiente si no que se se le pasa ya concatenada a la siguiente iteración

5. Dado el tipo 'a tree definido a continuación para representar árboles binarios con nodos de tipo 'a, define una función inner_nodes: 'a tree -> 'a list que de para cada árbol, la lista en inorden de sus nodos internos (los que no son hojas)

`type 'a tree = E | N of 'a tree * 'a * 'a tree;;`

```
let rec list_of_tree = function
    E -> [] |
    N (E,r,E)-> [] |
    N (l,x,r) -> list_of_tree l @ [x] @ list_of_tree r;;
```

### Examen enero 2021

2. Redefine la siguiente función comb 

```
let rec comb f = function
    h1::h2::t -> f h1 h2 :: comb f t |
    l -> l;;
```
para que sea recursiva terminal

```
let comb f l = 
    let rec loop aux f = function
        h1::h2::t -> loop ((f h1 h2) :: aux) f t |
        h1::t -> loop (h1::aux) f t |
        l -> List.rev aux
    in loop [] f l;;
```

3.  Realiza la siguiente implementación de sum

```
let rec sum = function
      T (x, []) -> x
    | T (r, T(r1, l)::t) -> r + sum (T (r1, l@t));;
```

de forma recursiva terminal

```
let sum tree =
    let rec loop aux = function
      T (x, []) -> aux+x
    | T (r, T(r1, l)::t) -> loop (aux + r)(T (r1, l@t))
    in loop 0 tree;;
```

### Examen enero 2019

2 Considere la siguiente definición en Ocaml.
```
let rec trivide = function
    [] -> [], [], []
    | h::t -> let t1, t2, t3 = 
        trivide t 
    in h::t3,t1,t2;;
```
- Indique el tipo de la función trivide.
```
val trivide : 'a list -> 'a list * 'a list * 'a list = <fun>
```
- Realice una implementación alternativa que sea recursiva terminal.

```
let trivide l =
    let rec aux s1 s2 s3 = function
        [] -> List.rev s1, List.rev s2, List.rev s3
        | h1::h2::h3::t -> aux (h1::s1) (h2::s2) (h3::s3) t
        | h1::h2::t -> aux (h1::s1) (h2::s2) s3 t
        | h1::t -> aux (h1::s1) s2 s3 t
    in aux [] [] [] l;;
```

3. Considere la siguiente definición en Ocaml:

`type ('a,'b) tree = S of 'b|T of 'a* ('a,'b) tree*('a,'b)tree;;`

Considere también la siguiente definición en Ocaml:
```
let rec eval = function
S x -> x
| T (op,t1,t2) -> op (eval t1) (eval t2);;
```
- Indique el tipo de la función eval

`val eval : ('a -> 'a -> 'a, 'a) tree -> 'a = <fun>`

- Considere las siguientes definiciones
Indique el tipo de e y el valor de x.

`# let e = T(( * ), T((+), S 2, S 3), S 5);;`
```
val e : (int -> int -> int, int) tree = T (<fun>, T (<fun>, S 2, S 3), S 5)
```
`# let x = eval e;;`
```
val x : int = 2
```

