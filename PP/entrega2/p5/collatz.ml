let f n =
    if n mod 2 = 0 then 
        n/2 
    else 3*n+1;;
    
let rec orbit n =
    if n > 1 then
        (string_of_int(n) ^ ", " ^
        orbit (f n))
     else
        "1";;

let rec length n =
    if f n > 1 then
        length(f n) + 1
    else
        1;;

let rec top n =
    if n > 1 then
        max n (top (f n))
    else
        1;;

let rec length'n'top n =
    if n > 1 then
        (function (x, y) -> (x + 1, max n y))
        (length'n'top (f n))
    else
        (0, 1);;

let rec longest_in m n =
    let rec aux i =
        if i<=m then 
            (m,length m)
        else 
            let (j,lj) = aux (i-1) in
            let li = length i in
            if lj >= li then (j,lj) else (i,li)
    in aux n;;

let rec highest_in m n =
    let rec aux i =
        if i <= m then 
            (m, top m)
        else 
            let (j,tj) = aux (i-1) in
            let ti = top i in
            if ti > tj then (i,ti) else (j,tj)
    in aux n;;
