let rec powmod m b e = 
    if e = 0 then 
        1
    else if e = 1 then
        b mod m
    else
        b mod m * (powmod m (b mod m) (e - 1)) mod m;;