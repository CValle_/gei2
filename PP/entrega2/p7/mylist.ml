let rec rev_append l1 l2 = match l1 with
    [] -> l2 |
    h::t -> rev_append t (h::l2);;

let rev l = rev_append l [];;

let hd l = match l with
    h::_ -> h |
    [] -> raise (Failure "hd");;

let tl l = match l with
    h::t -> t |
    [] -> raise (Failure "tl");;

let length l = 
    let rec aux n l = match l with
        [] -> n
        | _::t -> aux (n+1) t 
    in aux 0 l;;

let rec compare_lengths l1 l2 = match (l1,l2) with
    [],[] -> 0 |
    _,[] -> 1 |
    [],_ -> -1 |
    l1::t1,l2::t2 -> compare_lengths t1 t2;;

let rec nth l n = match (l,n) with
    [],_ -> raise (Failure "nth") |
    h::_,0 -> h |
    _::t,_ -> nth t (n-1) ;;

let append l1 l2 = rev_append (rev l1) l2;;

let rec find f l = match l with
    [] -> raise Not_found |
    h::t -> if f h then 
                h 
            else
                find f t;;

let rec for_all p = function
    [] -> true
    | h::t -> (p h) && (for_all p t);;

let rec exists p l = match l with
    [] -> false |
    h::t -> (p h) || (exists p t);;

let rec mem e l = match l with
    [] -> false |
    h::t -> if e = h then 
                true
            else 
                mem e t;;

let filter f l =
    let rec aux l res = match l with
        [] -> rev res |
        h::t -> if f h then 
                        aux t (h::res)
                    else 
                        aux t res
    in aux l [];;

let find_all = filter;;

let partition f l =
    let rec aux f l res res2 = match l with
        [] -> (rev res, rev res2) |
        h::t -> if f h then 
                        aux f t (h::res) res2
                    else 
                        aux f t res (h::res2)
    in aux f l [] [];;

let split l =
    let rec aux l res1 res2 = match l with
        [] -> ((rev res1),(rev res2)) |
        (h1,h2)::t -> aux t (h1::res1) (h2::res2)
    in aux l [] [];;

let combine l1 l2 =
    let rec aux l1 l2 res = match (l1,l2) with
        [],[] -> rev res |
        _::_,[] | [],_::_ -> raise (Invalid_argument "List.combine") |
        h1::t1,h2::t2 -> aux t1 t2 ((h1,h2)::res)
    in aux l1 l2 [];;

let init len f =
    let rec aux len res = match len with
        0 -> res |
        len -> aux (len-1) ((f (len-1))::res)
    in aux len [];;

let concat l =
    let rec aux res = function
        [] -> rev res | 
        h::t -> aux (rev_append h res) t
    in aux [] l;;

let flatten = concat;;

let map f l =
    let rec aux l res = match l with
        [] -> res |
        h::t -> aux t (f h::res)
    in aux l [];;

let rev_map f l =
    let rec aux l res = match l with
        [] -> res |
        h::t -> aux t (f h::res)
    in aux l [];;

let map2 f l1 l2 =
    let rec aux l1 l2 res = match (l1,l2) with
        [],[] -> rev res |
        _::_,[] | [],_::_ -> raise (Invalid_argument "List.map2") |
        h1::t1,h2::t2 -> aux t1 t2 ((f h1 h2)::res)
    in aux l1 l2 [];;

let rec fold_left f a = function
    [] -> a
    | h::t -> fold_left f (f a h) t;;

let rec fold_right f l a = match l with
        [] -> a
        | h::t -> f h (fold_right f t a);;

