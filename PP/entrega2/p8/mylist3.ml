let rec remove x l = match l with
    [] -> [] |
    h::t -> if x = h then 
                t 
            else 
                h :: remove x t;;


let remove_all n l =
    let rec aux l res = match l with
        [] -> List.rev res |
        h::t -> if h <> n then
                    aux t (h::res)
                else
                    aux t res
    in aux l [];;

let ldif l1 l2 =
    let rec aux l1 res = match l1 with
        [] -> List.rev res |
        h1::t1 -> if List.exists (fun i -> i = h1) l2 then
                        aux t1 res
                    else
                        aux t1 (h1::res)
    in aux l1 [];;

let lprod l1 l2 =
    let rec aux cont a b = match (a,b) with 
        [], _ -> List.rev cont
        | ha::ta, [] -> aux cont ta l2
        | ha::ta, hb::tb -> aux ((ha, hb)::cont) a tb
    in aux [] l1 l2;;

let divide l =
    let rec aux l1 l2 pos = function
        [] -> (List.rev l1,List.rev l2) |
        h::t -> if (pos=0) then 
                    aux (h::l1) l2 1 t
                else 
                    aux l1 (h::l2) 0 t
    in aux [] [] 0 l;;
