let rec power x y=
    if y != 0 then
        x * (power x (y-1))
    else 1;;

let rec power' x y=
    if y != 0 then
        if y mod 2 = 0 then
            power' (x * x) (y/2)
        else
            x * (power' (x * x) (y/2))
    else
        1;;

(* al dividir la función en números pares o impares se hace un número menor de llamadas recursivas*)

let rec powerf x y =
    if y != 0 then
        if y mod 2 = 0 then
            powerf (x *. x) (y/2)
        else
            x *. powerf (x *. x) (y/2)
    else
        1.;;