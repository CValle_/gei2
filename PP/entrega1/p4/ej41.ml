let rec sum_cifras n =
    if (n > 0) then
        (n mod 10) + (sum_cifras (n / 10))
    else
        0 ;;

let rec num_cifras n =
    if (n > 0) then 
        1 + (num_cifras (n / 10))
    else 
        0 ;;

let rec exp10 n = 
    if n > 0 then 
        10 * (exp10 (n-1))
    else 
        1 ;;

let rec reverse n =
    if n >= 1 then
        (n mod 10) * exp10 (num_cifras n) + reverse (n / 10)
    else
        0;;

let rec reverse n =
    if n >= 1 then
        (n mod 10) * exp10 ((num_cifras n) - 1) + reverse (n / 10)
    else
        0;;

let palindromo s =
    let rec is_palin i j =
        if i >= j then true
        else s.[i] = s.[j] && is_palin (i+1) (j-1)
    in is_palin 0 (String.length s-1);;