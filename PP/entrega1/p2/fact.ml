if Array.length Sys.argv = 2 then
    let rec fact = function 
        0 -> 1 | 
        n -> n * fact (n-1) in
            Sys.argv.(1) |> int_of_string |> fact |> string_of_int |> print_endline
else 
 print_endline "Número inválido de argumentos"