# APUNTES OCAML

### Compiladores habituales
- `Ocamlopt` -> Bytecode 
- `Ocamlc` -> Código nativo

- `ledit ocaml` compilador interactivo (top level)

### Ejecución
`ocamlc nombre.ml -o nombre`

`ocamlc -o nombre nombre.ml`

`./test`

- `time ./test` tiempo de ejecución

- `# quit;;` salir del programa



### Tipos
- `2` tipo int
- `2.` tipo float
- `'A'` (char entre comillas simples)
- `"A"` (string entre comilla dobles)
- `()` valor de tipo UNIT, tipo con un único valor

### Operadores
- `*.` (multiplica dos valores flotantes)
- `*`  (multiplica valores enteros)
- `/`  (cociente división entera)
- `=` compara dos valores del mismo tipo
- `&&` conjunción lógica
- `||` disyunción lógica

### Operadores básicos

- `^` concatena strings
```
# (^);;
- : string -> string -> string = <fun>

# "pe" ^ "pa";;
- : string = "pepa"
```

Los operadores son funciones con "dos entradas" ('a -> 'a) y que devuelven un resultado, en este caso del mismo tipo (-> 'a)

### Funciones básicas
Las funciones en general no llevan paréntesis, estos solo se ponen para resolver ambigüedades

Escribir el nombre de una función en oCaml devuelve su tipo:

`-: entrada -> salida = <fun>`

- Crear una función que sume dos numeros:

```
# function x -> x + x;; 
- : int -> int = <fun>
```

A estas funciones se les puede asignar un nombre, en este caso devuelve su valor y su tipo:

```
# let f x = x + x;;  
val f : int -> int = <fun>
```

Podemos llamar a una función de la siguiente forma

```
# f 2;;
- : int = 4
```

### Expresiones (- :)

En oCaml podemos encontrar distintas expresiones predefinidas:

- Expresiones de impresión por pantalla

Todas las expresiones que imprimen por pantalla devuelven un elemento `Unit ()` ya que no devuelve ningun valor a efectos prácticos:

Para imprimir texto usamos (e imprime un salto de línea al final)
```
# print_endline;;
- : string -> unit = <fun>

# print_endline "Camels are bae";;
Camels are bae
- : unit = ()
```
También podemos imprimir números decimales
```
# print_float;;
- : float -> unit = <fun>

# print_float 7.;;
7.- : unit = ()
```

- Expresiones matemáticas:

Podemos comprobar que `asin` es una función de float en float

```
# asin;;
- : float -> float = <fun>
```
```
# asin 1.0;;
- : float = 1.57079632679489656
```
Podemos usar las expresiones dentro de funciones:

```
let pi = 2.0 *. asin 1.0;;
val pi : float = 3.14159265358979312
```
También tenemos la función valor absoluto:

```
# abs;;
- : int -> int = <fun>

# abs(-7);;
- : int = 7
```

### Módulos predefinidos
Los módulos contienen y agrupan ciertas funciones relacionadas

- Módulo Char

```
# Char.code;;
- : char -> int = <fun>

# Char.code '\n';;
- : int = 10
```
- Módulo string
```
# String.length;;
- : string -> int = <fun>

# String.length "aaa";;
- : int = 3
```

- Std lib (librería estandar)
```
# int_of_char;;
- : char -> int = <fun>
```

## --------------------- mie 14 sept

### Asignación y reasignación de nombres

```
# let name = 2;;
val name : int = 2

name + name;;
- : int = 4

# let name = 0.1;;
val name : float = 0.1

name +. name;;
- : float = 0.2
```

Como podemos comprobar, el valor se sobreescribe si le damos el mismo nombre.

### Definiciones locales

Se define un valor a la izquierda de la palabra `in` que será usado en la expresión de la derecha.

Por lo tanto, una expresión, aún que tenga una definición local, siendo una expresión (no una definición):

```
# let pi = 2. *. asin 1. 
        in pi *. pi;;
- : float = 9.86960440108935799
```

Por otro lado las definiciones también pueden contener expresiones locales;

```
# let y = let x = 10 * 10 
        in x * x;;
val y : int = 10000
```

El compilador te va a avisar si una definición local no se usa (ya que solo puede ser usada dentro de la función)

```
# let w = 9 in 2 * 3;;
Warning 26 [unused-var]: unused variable w.
- : int = 6
```

### Funciones booleanas

Existen funciones booleanas predefinidas como `not`:

```
# not;;
- : bool -> bool = <fun>

# not true;;
- : bool = false
```

- Crear función not:

```
# function true -> false | false -> true;;
- : bool -> bool = <fun>

# (function true -> false | false -> true) true;;
- : bool = false

# (function true -> false | false -> true) false;;
- : bool = true
```

Podríamos guardar la función con un nombre

```
# let myNot b = (function true -> false | false -> true) (b);;
val myNot : bool -> bool = <fun>

# myNot true;;
- : bool = false
```
- Crear una función que convierta todo bool en true:

```
# let allTrue = function true -> true | false -> true;;
val allTrue : bool -> bool = <fun>
```

Se pueden simplificar las dos reglas de la función de la siguiente forma, aún que NO ES RECOMENDABLE indicar el tipo:

```
# let allTrue = function (x:bool) -> true;;
val allTrue : bool -> bool = <fun>
```

Lo más recomendable es indicar que "cualquier cosa", es decir un elemento `'a'` o `alpha`, devuelva true

```
# let allTrue = function _ -> true;;
val allTrue : 'a -> bool = <fun>
```
Esto es llamado polimorfismo (se ve más adelante en el apartado `comodines`).

### Funciones y argumentos

Los argumentos se declaran a la izquierda de la flecha

- Crear función f que lleva un número n a su doble

```
# function n -> n * 2;;
- : int -> int = <fun>
```

Si le ponemos un nombre...

```
# let doble = (function n -> n * n);;
val doble : int -> int = <fun>

# doble 4;;
- : int = 16
```


### Funciones parciales
Una función parcial deja casos sin contemplar que pueden dar lugar a errores de ejecución

```
# let f =  function true -> false;;

Warning 8 [partial-match]: this pattern-matching is not exhaustive. Here is an example of a case that is not matched: false

val f : bool -> bool = <fun>
```

En este caso el compilador avisa de que, por ejemplo, el caso `false` no está contemplado. Por lo tanto si hago:

```
# f true;;
- : bool = false
```

funciona, pero...

```
# f false;;
Exception: Match_failure ("//toplevel//", 1, 9).
```

salta una excepción.

## --------------------- vie 16 sept

### Definiciones (no variables)

En los lenguajes funcionales no existen las variables, solo definimos nombres a valores:

```
# let n = 3;;
val n : int = 3

# let sumTres = function x -> x + n;;
val sumTres : int -> int = <fun>
```

El valor `n` simplemente representa el argumento cuando la función se aplica, y si había un n definido no se altera:

```
# sumTres 5;;
- : int = 8

# n;;
- : int = 3
```

Es importante saber que si modifico el valor de `n`, el valor de `n` en `sumTres` NO SE MODIFICA;

```
# let n = n + 1;;
val n : int = 4

# sumTres 5;;
- : int = 8
```

Si queremos darle nombre al valor `x`, estamos redefiniendo `sumTres`, por lo que en este caso `n` cogerá su último valor, que es 4:

```
# let sumTres x = x + n;;
val sumTres : int -> int = <fun>

# sumTres 5;;
- : int = 9
```

### Comodines (cualquier cosa de cualquier tipo)

Los comodines `_` se usan para nombrar cualquier valor de cualquier tipo, generalmente cuando ese valor no va a ser usado en la funcion y no tiene por que ponérsele nombre:

```
# let allTrue = function _ -> true;;
val allTrue : 'a -> bool = <fun>
```

- Crea una función que indica cuando un valor no es cero

En este caso solo necesitamos saber que 0 va a dar falso, y cualquier otra cosa verdadero

```
# let notzero = function 0 -> false | _ -> true;;
val notzero : int -> bool = <fun>
```
cabe destacar que es una función int en bool ya que la primera regla está definiendo los dos valores que componen la función.

Si probamos a cambiar las reglas de sitio daría un caso redundante, la segunda norma nunca se ejecutaría ya que cualquier cosa daría true en el primer caso

```
# let notzero = function _ -> true | 0 -> false;;
Warning 11 [redundant-case]: this match case is unused.
val notzero : int -> bool = <fun>
```

### Polimorfismo

Podemos definir funciones de cualquier tipo en si mismas, lo que sería `cualquier valor` a `cualquier valor del mismo tipo` estos valores se definen como `'a` o `alpha`

- Función de cualquier tipo en si mismo (identidad)

```
# let id x = x;;
val id : 'a -> 'a = <fun>
```

### Orden de aplicación de funciones
La aplicación de funciones es asociativa por la izquierda (primero se ejecutan las que hay a la izquierda)

- Función que aplica abs a 7 y luego la identidad al resultado (sería como `id (abs 7)`)

```
# id abs 7;;
- : int = 7
```

- Función que aplica el valor absoluto a si mismo y luego a 7

```
# (id abs) (-7);;
- : int = 7
```
- Ejemplo con función `doble`

```
# let doble x = x *. x;;
val doble : float -> float = <fun>
```
Si intento aplicar sqrt a doble 10 (`sqrt (doble 10)`) da error ya que sqrt se aplica a ints, no a funciones:

```
# sqrt doble 10.;;
Error: This function has type float -> float
```

En cambio si primero hacemos el cálculo del doble de 10 y a continuación sqrt:

```
# sqrt (doble 10.);;
- : float = 10.
```

### Alpha conversión

Un valor alpha, si cambia de la misma dorma a los dos lados de la asignación no varía. Por eso sabemos que `let sqr x = x * x;;` y `let sqr y = y * y;;` son equivalentes.

Al contrario,
```
let f x = x + y;;
```

no es equivalente a

```
let f y = y + y;;
```
Solo se pueden cambiar por nombres que no se usan en la definición

## --------------------- mie 21 sept

### Funciones lambda (funciones anónimas)
Una función anónima (no definida, sin nombre) es del tipo:
- `(function <pattern> -> <expr>) (arg)`

Por ejemplo la función que multiplica por dos un número indicado por el argumento:
```
# (function x -> 2 * x) (3+1);;
- : int = 8
```
En sexpresiones de este tipo siempre se evalúa primero el argumento que se usará a continuación en la expreisón.

Se evalúa igual que en una definición local, con la diferencia de que en estas guardamos el valor resultado
- `let <pattern> = <arg> in <expr>`


por ejemplo:
```
# let x = 3+1 in 2*x;;
- : int = 8
```

Se le pueden aplicar varias reglas a una función de la siguiente forma:

- `(function <p1> -> <e1> | ... | <pn> -> <en>) (<e>)`

o usando el comando `match - with` (los tipos de p_n deben ser compatibles entre ellos)
- ` match <expr> with <p1> -> <e1> | ... | <pn> -><en>`

El operador `|>` permite organizar las funciones de una forma más legible

Por ejemplo la función :
```
let rec fact = function 0 -> 1 | n -> n * fact (n-1) in

print_endline(string_of_int(fact(int_of_string(Sys.argv.(1)))))
```

puede escribirse de la misma forma como:

```
let rec fact = function 0 -> 1 | n -> n * fact (n-1) in

Sys.argv.(1) |> int_of_string |> fact |> string_of_int |> print_endline
```

Sys.argv(n) coge los valores en la posición `n` que se pasan como argumento al ejecutar el programa.

- otro ejemplo
```
print_int(f(g(f(2+1*7)))) == 2+1*7 |> f |> g |> f |> print_int
```

### Estructura `if-then-else`
Es de la forma:

- `if <b> then <e1> else <e2>;;`

```
# let abs = function x -> (if x < 0 then -x else x );;
val abs : int -> int = <fun>

# abs (-2);;
- : int = 2

```

Realmente esta estructura es una función anínima (se evalúa primero el argumento y luego la función):

- `(function true -> <e1> | false <e2>) (<b>)`

por lo que podríamos definir la función anterior de la forma siguiente:

```
# let abs x = (function true -> -x | false -> x) (x < 0);;
val abs : int -> int = <fun>

# abs (-2);;
- : int = 2
```

### Operadores lógicos `&&` y `||`

En una sentencia con operadores primero se evalúa el primer térmimo y solo en caso encesario se evalúa el segundo (evaluación atajo). Por ejemplo:

- `<bool1> && <bool2> `

En caso de que `bool1` fuese falso, ya sabríamos que el resultado de ese and es falso y no se evarlúa bool2.

la función lógica `&&` puede ser interpretada como la sigueinte sentencia:

```
# let conj b1 b2 = if b1 then b2 else false;;
val conj : bool -> bool -> bool = <fun>

# conj true false;;
- : bool = false

# conj true true;;
- : bool = true
```

## --------------------- vie 23 sept

### Ejercicios para pensar

- Cual es la eficiencia de los distintos argumentos? Que pasa cada vez que se aplica l?

```
# let l = function r -> 
        let pi = 2. *. asin 1. 
        in 2. *. pi *. r;;

val l : float -> float = <fun>
```
Es una funcion que para cada valor del radio calcula una circunferencia

```
# l 1.;;
- : float = 6.28318530717958623
```

Cada vez que se ejecuta la funcion se calcula el valor de pi.

Entonces habría que definirlo antes y usarlo dentro de la función. 

En caso de que quisiesemos dejar la definición de pi dentro de l (local) sin que se repitiese el cálculo cada vez

```
# let l = 
        let pi = 2. *. asin 1. 
        in function r -> 2. *. pi *. r;;
val l : float -> float = <fun>
```

Así la definición de pi se calcula cuando se define la función l

- Que tipo tiene f y que es realmente esa f, que representa? (función sumar x)
```
# let f = function x -> 
        (function y -> x + y);;
```

Es de tipo:

```
val f : int -> int -> int = <fun>
```

Se al empezar a leer por la derecha no se ponen los paréntesis, sería como `val f : int -> (int -> int) = <fun>`

Esta función consta de "dos argumentos", x e y:

```
# f 4 2;;
- : int = 6
```

Pero en oCaml no existen funciones de dos argumentos, realmente es una función aplicada a otro argumento función.

A las funciones que devuelven funciones se les dice que tienen forma `Curry` por Haskell Curry

### Operadores infijos (Forma Curry de una operación binaria)
Operador suma

```
# (+) 1 7;;
- : int = 8
```
- Creo función sum
```
# let sum = (+) 1;;
val sum : int -> int = <fun>

# sum 7;;
- : int = 8
```

- Creo función pred

```
# let pred = (+) (-1);;
val pred : int -> int = <fun>

# pred 7;;
- : int = 6
```

- Creo función p
```
# let p= (-) 1;;
val p : int -> int = <fun>

# p 7;;
- : int = -6
```
- Creo función contr (hay que poner espacios entre el asterisco y el paréntesis para que no lo entienda como un comentario)

```
# let contr = ( * ) (-1);;
val contr : int -> int = <fun>

# contr 7;;
- : int = -7

# contr (-7);;
- : int = 7
```

Otra forma:

```
# let contr = (-) 0;;
val contr : int -> int = <fun>

# contr 8;;
- : int = -8
```

### Productos cartesianos (a*a)

- Definimos función f (suma de dos valores)

```
# let f x = function y -> x + y;;

val f : int -> int -> int = <fun>
```

o abreviado

```
# let f x y = x + y;;

val f : int -> int -> int = <fun>
```

- Los productos cartesianos tienen tipo alpha * alpha

```
# (3, 2);;
- : int * int = (3, 2)
```

```
# 3.0, 2;;
- : float * int = (3., 2)
```

- Definimos función s (suma de los pares)

```
# let s = function x,y -> x + y;;

val s : int * int -> int = <fun>
```

o abreviado

```
# let s (x,y) = x + y;;

val s : int * int -> int = <fun>

# s (1,2);;
- : int = 3
```
En este caso se necesitan paréntesis para definir el par

- Definimos un par (int, función)

```
# 3, abs;;
- : int * (int -> int) = (3, <fun>)
```

### Estructura de las funciones

- estructura compleja

`let function nom <pattern> -> <e2>`

- estructura simplificada (anónima)

`let func x y = if x > y then x else y;;`

## --------------------- mie 28 sept

### Operadores infijos como funciones (forma Curry)

- Operador `(+)`
```
# (+);;
- : int -> int -> int = <fun>

# (+) 5 1;;
- : int = 6
```

- Operador concatenar strings `(^)`

```
# (^);;
- : string -> string -> string = <fun>

# (^) "hola";;
- : string -> string = <fun>

# (^) "ho" "la";;
- : string = "hola"
```

- función con operador (=)
```
# (=);;
- : 'a -> 'a -> bool = <fun>

# (=) 3;;
- : int -> bool = <fun>

(* si guardo este valor en una variable *)

# let isThree = (=) 3;;
val isThree : int -> bool = <fun>

(* isThree devuelve false en todos los casos menos en 3 *)

# isThree 8;;
- : bool = false

# isThree 3;;
- : bool = true
```

- Función con operador (<=)

```
# (<=);;
- : 'a -> 'a -> bool = <fun>

# let positivo = (<=) 0;;
val positivo : int -> bool = <fun>

# positivo (-5);;
- : bool = false
```

- Función max (devuelve el valor máximo de entre dos valores)

```
# max;;
- : 'a -> 'a -> 'a = <fun>

# max 3 7;;
- : int = 7
```
bb
- Definimos la función max 
```
# let max = function x -> 
              function y -> 
                if x > y then x else y;;
```
o
```
# let max x -> function y -> 
                if x > y then x else y;;
```
o
```
# let max x y = if x > y then x else y;;
```

### Funciones con productos cartesianos

- Par por string (falso trio)

```
# let p = (true, ()), "falso trio";; 

val p : (bool * unit) * string = ((true, ()), "falso trio")
```

- trio

```
# let t = true, (), "falso trio";; 

val t : bool * unit * string = (true, (), "falso trio")
```

- Función fst (first)

Devuelve la primera componente del par (tiene prioridad)

```
# fst;;

- : 'a * 'b -> 'a = <fun>

# fst (2,true);;

- : int = 2
```

- Usando los ejemplos de los pares/ternas de antes

```
# fst p;;

- : bool * unit = (true, ())
```

```
fst t;;
(* falla, solo funciona para pares *)
```

- Función snd (second)

```
# snd;;

- : 'a * 'b -> 'b = <fun>
```

- Definimos la función first

```
# let fst = function (x,y) -> x;;
```

o simplificado

```
# let fst (x,y) = x;;
```

más elegante (al no usarse la y)

```
# let fst (x,_) = x;;
```

### funciones recursivas
Si no se pone rec, la función recursiva que se llama dentro no estaría definida

- Definimos una función recursiva
```
let rec fact n =
        if n = 0 then 1
        else n * fact(n-1);;

val fact : int -> int = <fun>

# fact 0;;

- : int = 1

# fact 3;;

- : int = 6
```

Esta función siempre termina, si n es positivo

Hay valores de n que agotan el stack

```
# fact 1000000;;
Stack overflow during evaluation (looping recursion?).
```

- Casos negativos

Llegaría un momento en el que pasaría el valor mínimo de un int (int es (Z mod 2)^63) (álgebra modular) por lo tanto si al mayor Z le sumo uno, me da el min int.

```
# max_int;;
- : int = 4611686018427387903

# max_int +1;;
- : int = -4611686018427387904
```

```
# fact 55;;
- : int = -2511882692165894144
```

- Caso factorial de 64

factorial de 64 da 0 ya que 64 es `2^6` que es 2 63 veces por lo que módulo 63 es 0.

```
# fact 64;;
- : int = 0

# 64 mod 2;;
- : int = 0
```

Hay definiciones recursivas que siempre usan el mismo espacio en el stack (no tienen cuentas pendientes), con estas nunca tendremos problemas con el stack (operaciones finales, terminales, tail ...)

## --------------------- vie 30 sept

### Funciones recursivas con pares

Primero vamos a hacer una unción que devuelve el número anterior y el siguiente

```
# let f x = x -1, x +1;;
val f : int -> int * int = <fun>

# let g x y = x + 1, x * y;;
val g : int -> int -> int * int = <fun>

# g 2;;
- : int -> int * int = <fun>

# g 2 3;;
- : int * int = (3, 6)
```

Vamos a ver que sucede cuando quiero hacer una función recursiva con una función que devuelve una tupla

Solo para x >= 0 e Y > 0

- Creo una implementación de cociente (recursividad no final)

```
# let rec quo x y = 
          if x < y then 0
          else 1 + quo (x-y) y ;;
val quo : int -> int -> int = <fun>

# quo 2 4;;
- : int = 0

# quo 6 3;; 
- : int = 2

# quo max_int 2;; 
Stack overflow during evaluation (looping recursion?).
```

- Creo una implementación de resto (recursividad final)
```
# let rec rem x y = 
          if x < y then x
           else rem (x-y) y;;
val rem : int -> int -> int = <fun>

# rem 4 5;;
- : int = 4
```

En este caso no hay cuentas pendientes por lo que nunca va a haber stack overflow (no hay ese 1 + rec)

- Creo función división (que devuelve un par cociente * resto)

```
# let div x y = quo x y, rem x y;;
val div : int -> int -> int * int = <fun>
```

Ya que quo y rem usan lo mismo puedo simplificarlo (como uso una función recursiva que devuelve una tupla??)

```
# let rec div x y =
        if x < y then 0, x (* par 0, x *)
        else 1 + fst(div (x-y) y), snd(div (x-y) y) (* par cociente, resto *);;
val div : int -> int -> int * int = <fun>

# div 40 2;;
- : int * int = (20, 0)
```

Pero en esta expresión estoy calculando dos veces div (x-y) de y (se hace la recursividad el doble de veces! (2^40 pasos)) `FATAL`.

Por lo tanto, si lo definimos antes:

```
# let rec div x y =
        if x < y then 0, x
        else let p = div (x-y) y in
                1 + fst p, snd p;;
val div : int -> int -> int * int = <fun>
```

`SOLO LOS NOVATOS USAN FST Y SND` (definimos dos nombres, uno para parte del par)

Por fin encontramos una definición que esté bien

```
# let rec div x y =
        if x < y then 0, x
        else let q,r = div (x-y) y in
                1 + q, r;;
val div : int -> int -> int * int = <fun>
```

- Ahora intentaremos crear una función recursiva de fibonacci

Con el sigueinte algoritmo volvemos a tener una recursividad doble `FATAL`

```
(* dónde n es el número de iteración *)

# let rec fib n = 
        if n <= 1 then n
        else fib (n-1) + fib (n-2);;
val fib : int -> int = <fun>
```

Con esta definición recursiva al final se crea un arbol binario de recursividad del que cada rama se divide en 2 hasta llegar a 1 y 0.

Una idea para arreglar este, creamos una función que devuelva el fibonacci de ese número y el anterior, que para la recursividad es mucho más facil y evitamos la doble recursividad

- Creamos una función fib2 a la que se le pase un valor `int` y devuelva una tupla con el resultado y el resultado anterior, o sea que la función es d tipo `int -> int * int`

```
# let rec fib2 = function
          1 -> 1, 0 |
          n -> let fibnmenos1, fibnmenos2 = fib2 (n-1) in
                  fibnmenos1 + fibnmenos2, fibnmenos1;;

val fib2 : int -> int * int = <fun>

# fib2 40;;
- : int * int = (102334155, 63245986)
```

Esta recursividad deja cuentas pendientes en el stack

## --------------------- vie 05 oct

### Falsa función Sys.time (no es verdaderamente funcional)
`Sys.time` devuelve lo que dice el sistema operativo que lleva consumido de cpu ese proceso en segundos (en este caso el proceso de la máquina de ocaml)

```
# Sys.time ();;
- : float = 0.0478979999999999961
```

- Podemos calcular el tiempo que le lleva a fib de 40 calcularlo:
```
# let t = Sys.time ();;
val t : float = 0.159994

# fib 40;;
- : int = 102334155

# Sys.time () -. t;;
- : float = 5.089243

```

- También podemos crear una función cronómetro:

```
let crono f x = 
        let t = Sys.time () in
        f x;
        Sys.time () -. t;;
```
o
```
let crono f x = 
        let t = Sys.time () in
        let _ = f x in
        Sys.time () -. t;;
```

o si queremos que devuelva el tiempo y el valor de f(x)
```
let crono_y_func f x = 
        let t = Sys.time () in
        let y = f x in
        Sys.time () -. t, y;;
```

### Funciones separadas (;)

El punto y coma une dos funciones en una, el primer valor lo ignora o se usa en la siguiente función en orden.

Por lo que si asignamos un valor a un nombre, el valor que obtiene es el de la segunda expresión

```
# let z = 2 + 1; "hola" ^ "adios";;
Warning 10 [non-unit-statement]: this expression should have type unit.
val z : string = "holaadios"
```

Además da un warning ya que el valor de la primera expresión no se usa

Para el próximo día nuevos tipos de datos (lista (secuencia finita), )

- Ejercicio 4.1 (función no recursiva pero funcional)

```
let sum_cifras n =
    let rec aux acc resto =
        if String.length resto = 1 then
            acc + (int_of_string resto)
        else
            aux (acc + int_of_string (String.sub resto 0 1)) (String.sub resto 1 ((String.length resto) - 1))
    in
        aux 0 (string_of_int n)
;;
```

## --------------------- vie 14 oct

### Listas

- Listas de ints

`let l = [1; 2; 3; 100];;`

- Listas de chars

`let c = ['a'; 'e'; 'i'; 'o'; 'u'];;`

- Lista con un elemento char

`['a'];;`

- Lista vacía (alpha list)

`[];;`

### Funciones del módulo list

- `length` devuelve la longitud
```
# let l = [1; 2; 3; 100];;
val l : int list = [1; 2; 3; 100]
# List.length l;;
- : int = 4
```

- `hd` devuelve la cabeza
```
# let c = ['a'; 'e'; 'i'; 'o'; 'u'];;
val c : char list = ['a'; 'e'; 'i'; 'o'; 'u']
# List.hd c;;
- : char = 'a'

```

- `tl` devuelve la cola
```
# let c = ['a'; 'e'; 'i'; 'o'; 'u'];;
val c : char list = ['a'; 'e'; 'i'; 'o'; 'u']
# List.tl c;;
- : char list = ['e'; 'i'; 'o'; 'u']

```
- Definimos nuestra propia función length (deja cuentas pendientes)

```
let rec length l =
        if l <> [] then
                1 + length (List.tl l)
        else
                0;;
```

- `init n f` hace una función de n elementos en base a la función f
```
# List.init 5 abs;;
- : int list = [0; 1; 2; 3; 4]
```
```
# List.init 5 (function n -> Char. chr(65+n));;
- : char list = ['A'; 'B'; 'C'; 'D'; 'E']
```

- `::` cons (añadir a la lista)
```
# let l = [1; 2; 3; 100];;
val l : int list = [1; 2; 3; 100]
# 0 :: l;;
- : int list = [0; 1; 2; 3; 100]
```


let rec length = function
        [] -> 0 |
        _:: -> 1 + length t;;


let rec init n f = (* n >= 0 *)
        if n = 0 then []
        else f 0 :: init (n-1 -> f (n+1));; 


let head = function h::_ -> h;;

## --------------------- mie 19 oct

### Tipo error de ejecución

- Definimos la función List.hd (no contemplamos todos los casos ya que la lista vacía no tiene head y daría error de compoilación)
```
# let hd (h::_) = h ;;
Warning 8 [partial-match]: this pattern-matching is not exhaustive.
Here is an example of a case that is not matched:
[]
val hd : 'a list -> 'a = <fun>
```

Failure es un constructor que tiene un argumento de tipo string y devuelve de que tipo es el error

```
# Failure;;
Error: The constructor Failure expects 1 argument(s),
       but is applied here to 0 argument(s)
```

hacer List.hd con una lista vacía devolvería un valor de tipo error de ejecución `Failure "hd"`

```
# List.hd [];;
Exception: Failure "hd".

# Failure "hd";;
- : exn = Failure "hd"
```

existen otros valores de error de ejecución como división entre 0

```
# 7/0;;
Exception: Division_by_zero.

# Division_by_zero;;        
- : exn = Division_by_zero
```

### RAISE

Levanta excepciones del tipo que sea
```
# raise;;
- : exn -> 'a = <fun>
```
si le paso la excepción levanta el error (no devuelve nada, se interrumpe la ejecución)
```
# raise Division_by_zero;;
Exception: Division_by_zero.
```

Entonces en el error que nos daba antes en nuestra definición de List.hd (pattern matching exhaustivo)
```
let hd l = match l with
    h::_ -> h |
    [] -> raise (Failure "hd");;
```
o simplificado 

```
# let hd = function
        h::_ -> h |
        [] -> raise (Failure "hd");;

val hd : 'a list -> 'a = <fun>

# hd [];;
Exception: Failure "hd".
```

- EJERCICIO: definir tl con su fallo

```
# let lista = ['a'; 'b'; 'c'];;
val lista : char list = ['a'; 'b'; 'c']

# let tl = function
        h::tl -> tl |
        [] -> raise (Failure "tl");;

        (*NO usar comodín _ como caso genérico*)
```

- definir nth con su fallo

```
let rec nth l n =
        if n < 0 then raise (Invalid_argument "nth")
        else if l = [] then raise (Failure "nth")
        else if n = 0 then List.hd l
        else nth (List.tl l) (n-1);;
```

o, más eficiente

```
# let rec nth l n =
        if l = [] then raise (Failure "nth")
        else if n = 0 then List.hd l
        else nth (List.tl l) (n-1);;

val nth : 'a list -> int -> 'a = <fun>

(* le pongo el mismo, nombre para no poder usar la versión anterior*)
# let nth l n = 
        if n < 0 then raise (Invalid_argument "nth")
        else nth l n;;

val nth : 'a list -> int -> 'a = <fun>

# nth ['a';'b';'c'] 2;;
- : char = 'c'
```

o con pattern matching
```
let rec nth l n = match (l,n) with
        [],_ -> raise (Failure "nth") |
        h::_,0 -> h |
        _::t, _ -> nth t (n-1) ;;
```

## --------------------- vie 21 oct

### List.append o @
```
# List.append;;
- : 'a list -> 'a list -> 'a list = <fun>
```

o

```
# [1;2;3] @ [6;7;8];;
- : int list = [1; 2; 3; 6; 7; 8]
```

- Creamos nuestra profia función append (cuentas pendientes, cons ::)
```
let rec append l1 l2 = match l1 with
        [] -> l2 |
        h::t -> h::append t l2;;
```

### fucnion quo y recursividad terminal

Quo es la íltima operación que se hace? Si es así no deja cuentas pendientes.

Esta operación no es recursiva terminal ya que deja cuentas pendientes (la última operación es la suma, no quo)

```
# let rec quo x y = 
        if x < y then 0
        else 1 + quo (x-y) y;;

val quo : int -> int -> int = <fun>
```

ya que

```
quo 17 3 ->
1+ quo 14 3 ->
1+ (1+ quo 11 3) ->
1+ (1+ (1+ quo 9 3)) -> ...
```

- La función rem 
Es tail recusive ya que no deja cuentas pendientes (la última operación es rem )

```
# let rec rem x y =    
        if x < y then x
        else rem (x-y) y
```

```
rem 17 3->
rem 14 3 -> ...
```

- La función last
Es tail recursive (solo hay la operación recursiva, por lo tanto la última operación va a ser last)

```
# let rec last = function
        h::[] -> h |
        _::t -> last t;;
```

```
last [a, e , i] ->
last [e, i] ->
last [i] ->
i
```

- la función length

No es tail recursive ta que queda la op +1 pendiente

```
# let rec length = function
        [] -> 0 |
        _::t -> 1+ length t;;
```


```
1 + length [1,2,3,4] ->
1 + (1 + length [2,3,4]) ->
1 + (1 + (1 + length [3,4])) ->
1 + (1 + (1 + (1 +length [4]))) ->
1 + (1 + (1 + (1 +length []))) ->
1 + (1 + (1 + (1 + 0))) ->
1 + (1 + (1 + 1)) ->
1 + (1 + 2) ->
1 + 3 ->
4
```

Podemos hacer length de forma tail recursive
```
# let rec aux i = function
        [] -> i |
        _::t -> aux (i+1) t;;

val aux : int -> 'a list -> int = <fun>
```

o, lo mismo

```
# let rec aux i l = match l with
        [] -> i |
        _::t -> aux (i+1) l;;

val aux : int -> 'a list -> int = <fun>

# aux 3 ['a';'b'];;
?????????

# let length l = aux 0 l;;

val length : 'a list -> int = <fun>
```
## --------------------- mie 26 oct

### fact tail recursive

```
let rec aux p i =
        if i = 0 then p
        else aux (p*i) (i-1);;

let fact m = aux 1 n;;
```

otra forma

```
let fact n =
        let rec aux i f =
                if i = n then f
                else aux (i+1) ((i+1)*f);;
        in aux 0 1;
```

## --------------------- vie 28 oct
### Función lmax (no tail recursive)
- En esta definición se repiten funciones innecesarias (se llama a lmax demasiadas veces, recursividad doble)
```
let rec lmax = function
        [] -> raise (Failure "lamax") |
        h::[] -> h |
        h::t -> if h >= lmax t then h
                else lmax t;;
```

Para evitar eso, antes de hacer el if hacemos un `let in` (def local)

```
let rec lmax = function
        [] -> raise (Failure "lamax") |
        h::[] -> h |
        h::t -> let m = lmax t in
                if h >= m then h else m;;
```
No es tail recursive, la última operacióne es el `if`

- Podemos usar max en la definición anterior para que quede más bonito
```
let rec lmax = function
        [] -> raise (Failure "lamax") |
        h::[] -> h |
        h::t -> max h (lmax t);;
```

Sigue sin ser tail recursive ya que la última operación no es `lmax` si no `max`

### Función lmax (tail recursive)

```
let lmax = function
        [] -> raise (Failure "lmax") |
        (*se podría poner function y quitar el argumento r y el match with*)
        h::t -> let rec aux m r = match r with 
                [] -> m |
                h::t2 -> aux (max m h) t2
                in aux h t;;
```
o 

```
let rec last = function
        [] -> raise (Failure "last") |
        h::[] -> h |
        _::t -> last t;;

```

o

```
let rec last = function
        [] -> raise (Failure "last") |
        h::[] -> h |
        h1::h2::t -> ;;

```

### Append y Rev_append

Hay que evitar usar append y length tanto como se pueda (son operaciones costosas sobre todo dentro de operaciones recursivas)!!

- Append no es recursiva terminal
```
let rec append l1 l2 = match l1 with
        [] -  > l2 |
          h::t -> h::append t l2;; 

val append : 'a list -> 'a list -> 'a list = <fun>
```

o de forma recursiva terminal (ya que rev append es recursiva terminal) pero cuesta el doble que hacer un append

```
let append' l1 l2 = rev_append (List.rev l1) l2;;
```

- Rev append es recursiva terminal (las listas se comportan como pilas)

```
let rec rev_append l1 l2 = match l1 with
        [] -> l2 |
        h::t -> rev_append t (h::l2);;
```

o

```
let rev l = rev_append l [];;

# rev [1;2;3];;
- : int list = [3; 2; 1]
```

o

- En este caso se hace append tantas veces como tenga la lista, si se hace en una lista de 10000, hay que hacer append con muchcísimo coste.

```
let rec rev = function
        [] -> [] |
        h::t -> append (rev t) [h];;
```

## --------------------- mie 2 nov
### Función ordenación por iserción

La entrada debe ser una lista ordenada
```
let rec insert x = function
        [] -> [x] |
        h::t -> if x <= h then x::h::t
        else h:: insert x t;;
```

```
let rec isort = function
        [] -> [] |
        h::t -> insert h (isort t);;
```

```
let crono f x = 
        let t = Sys.time () in
        f x;
        Sys.time () -. t;;
```

Genera una lista de 10.000 elementos positivos `abs`.
```
let l1 = List.init 10_000 abs;;
```

`Random.int n` -> es una falsa función que devuelve valores aleatorios entre el 0 y el doble del número indicado.

```
let rlist n = List.init n (fun _ -> Random.int (2*n));;

val rlist : int -> int list = <fun>

# rlist 10;;
- : int list = [4; 5; 2; 1; 19; 0; 4; 0; 1; 10]
```

Ejemplo
```
# let l10 = rlist 10;;
val l10 : int list = [10; 6; 7; 3; 1; 4; 1; 8; 19; 3]
# isort l10;;
- : int list = [1; 1; 3; 3; 4; 6; 7; 8; 10; 19]
```

### Añadir el orden como argumento
```
let rec insert_f ord x = function
        [] -> [x] |
        h::t -> if ord x h then x::h::t
        else h:: insert_f ord x t;;
```

```
let rec isort_f ord = function
        [] -> [] |
        h::t -> insert_f ord h (isort_f ord t);;
```

probamos

```
# isort_f (<=) [0;3;9;7;18];;
- : int list = [0; 3; 7; 9; 18]
```


## --------------------- vie 4 nov
### Versión recursiva terminal de ordenación por inserción

Usaremos una función auxiliar que trabaja con dos pilas que busca hasta que encuentre uno mayor que el que tiene que insertar.

`List.rev` le da la vuelta a una lista, es recursiva terminal.

```
# let insert' x l =
        let rec aux p1 p2 = match p1 with
                [] -> List.rev (x::p2) |
                h::t -> if x <= h then List.rev_append p2 (x::p1)
                        else aux t (h::p2)
        in aux l [];;
```

Ahora el sort, se parte de la lista vacía y se van insertando los elementos ya ordenados. Ahora el caso más rápido es la lista invertida:
```
# let isort' l =
        let rec aux p1 p2 = match p1 with
                [] -> p2 |
                h::t -> aux t (insert' h p2)
        in aux l [];;
```

### Ordenación por fusión
la forma más sencilla de partir una lista a la mitad es ir metiendo los elementos de una lista cada vez en otra diferente (uno pa ti uno pa mi), no es recursiva terminal, en cada paso se procesan dos elementos.

```
# let rec divide  = function
        h1::h2::t -> let t1, t2 = divide t in
                        h1::t1, h2::t2
        | l -> l, [];;

# divide [1;3;5;1;2];;
- : int list * int list = ([1; 5; 2], [3; 1])
```
`EJERCICIO: HACERLA RECURSIVA TERMINAL`

Fusiona dos listas ordenadas (no es recursiva terminal, aún queda hacer los cons)
```
# let rec merge = function 
        ([], l) | (l, []) -> l |
        (h1::t1, h2::t2) -> if h1 <= h2 then h1::merge (t1, h2::t2)
                                        else h2::merge (h1::t1, t2);;

# merge ([1;3;10], [2;4;6]);;
- : int list = [1; 2; 3; 4; 6; 10]
```
`EJERCICIO: HACERLA RECURSIVA TERMINAL`

Es innecesario hacer msort terminal (se hace el msort de la mitad de la lista, primero uno y después otro (log_2 de la longitud de la lista), el max int son 62 pasos, por lo que el stack no se agota).
```
# let rec msort l = match l with
        [] | _::[] -> l |
        _ -> let l1, l2 = divide l in
        merge (msort l1, msort l2);;
```

## --------------------- mie 9 nov

### Valores de tipo alpha option

Los valores de tipo int option tienen los valores de int y un valor más
- `'a option = None`

```
# Some 8;;
- : int option = Some 8
```

Si no quiero que se devuelva un error en vez de devolver un int devuelvo un int option

```
# 5/0;;
Exception: Division_by_zero.

# let quo x y =
          if y = 0 then
                  None
          else 
                  Some (x/y);;
val quo : int -> int -> int option = <fun>

# quo 5 0;;
- : int option = None
```

### Problema de las 8 reinas
funciones

```
let come (i1,j1) (i2,j2) =
        i1 = 12 || j1 = j2 || abs (i2 - i1) = abs (j2 - j1);;

(* let rec compatible p l = match l with 
        [] -> true |
        h::t -> not (come p h) && compatible p t;; *)

let rec compatible p l = not (List.exists (come p) l);;
```

- Solución al problema:

```
let queens n = 
        let rec completa path (i,j) =
                if i > n then 
                        Some path
                else if j > n then
                        None
                else if compatible (i,j) path then
                        match completa ((i,j)::path) (i+1,1) with
                                None -> completa path (i,j+1) |
                                Some sol -> Some sol
                else
                        completa path (i,j+1)
        in completa [] (1,1);;
```

```
# queens 1;;
- : (int * int) list option = Some [(1, 1)] (* solución *)

# queens 2;;
- : (int * int) list option = None (* no tiene solución *)

# queens 4;;
- : (int * int) list option = Some [(4, 3); (3, 1); (2, 4); (1, 2)]
```

- Para que devuelva todas las soluciones viables (sin errores)

```
# let old_queens n = 
        let rec completa path (i,j) =
                if i > n then 
                        [path]
                else if j > n then
                        []
                else if compatible (i,j) path then
                        completa ((i,j)::path) (i+1,1) @ completa path (i,j+1)
                else
                        completa path (i,j+1)
        in completa [] (1,1);;

val old_queens : int -> (int * int) list list = <fun>

# queens 4;; (* 2 sols *)
- : (int * int) list list =
[[(4, 3); (3, 1); (2, 4); (1, 2)]; 
 [(4, 2); (3, 4); (2, 1); (1, 3)]]
```


## --------------------- vie 11 nov

### Función List.find

```
# List.find (fun i -> i > 100) [1;2;3];;
Exception: Not_found. (* es de tipo exn *)

```

### Try <e> with

Evita la interrupción del programa. Coge el tipo del error `<e>` y lo comparo con las opciones que tiene, ejecutando esa condición (sustituye el valor que no fué capaz de dar e)

```
try <e> with
        <p1> -> <e1> |
        |
        | <pn> -> <en>
```

por ejemplo, redefiniendo el find para que no rompa la ejecución

```
# let find_opt p l = try Some (List.find p l) with
        Not_found -> None;;

val find_opt : ('a -> bool) -> 'a list -> 'a option = <fun>

# find_opt (fun i -> i > 100) [1;2;3];;

- : int option = None

# find_opt (fun i -> i > 100) [1;2;3;1000;134802];;

- : int option = Some 1000
```

### Ejercicio queens implementando errores

```
# let queens n = 
        let rec completa path (i,j) =
                if i > n then 
                        path 
                else if j > n then
                        raise Not_found
                else if compatible (i,j) path then
                        try completa ((i,j)::path) (i+1,1) with
                                Not_found -> completa path (i,j+1)
                                (* si no falla ya devuelve la sol *)
                else
                        completa path (i,j+1)
        in completa [] (1,1);;

val queens : int -> (int * int) list = <fun>

# queens 3;;
Exception: Not_found.
```

- Printeando la solución en otro formato (old_queens) sin errores

```
(* se podría definir con function y quitarle el parámetro *)

let rec print_sol l = match l with
        [] -> print_newline () |
        (_,j)::[] -> print_int j; print_newline(); |
        (* el ; evalúa primero la derecha y luego la izquierda *)
        (_,j)::t -> print_int j; print_char ' '; print_sol t;;

val print_sol : ('a * int) list -> unit = <fun>

# let old_queens n = 
        let rec completa path (i,j) =
                if i > n then 
                        print_sol path
                else if j > n then
                        ()
                else if compatible (i,j) path then
                        (completa ((i,j)::path) (i+1,1) ; completa path (i,j+1))
                else
                        completa path (i,j+1)
        in completa [] (1,1);;

val old_queens : int -> unit = <fun>

old_queens 7;;

6 4 2 7 5 3 1
5 2 6 3 7 4 1 ...
```

## --------------------- mie 16 nov

### DEFINICIÓN DE TIPOS DE DATOS
- Las reglas de `construcción` de los tipos de dato se escriben con letra mayúscula, y son las maneras de construír los tipos de datos. Si aparece solo, es un constructor constante.

```
(* tipo parecido a int option *)
# type maybeAnInt =
          NotAnInt |
          AnInt of int;;

type maybeAnInt = NotAnInt | AnInt of int

# NotAnInt;;

- : maybeAnInt = NotAnInt

# AnInt 4;;

- : maybeAnInt = AnInt 4
```

Uso

```
# let quo x y = match x, y with
          _, AnInt 0 -> NotAnInt |
          AnInt m, AnInt n -> AnInt (m/n) |
          _ -> NotAnInt;;
val quo : maybeAnInt -> maybeAnInt -> maybeAnInt = <fun>
```
- tipo parecido a bool
```
type boolean = T | F;;
```

```
# let conj a b = match a, b with
          F,_ -> F |
          _,F -> F |
          _ -> T;;

val conj : boolean -> boolean -> boolean = <fun>

# conj T F;;
- : boolean = F
```

- redefinir alpha option
```
type 'a option =
        None |
        Somw of 'a;;
```

- ERROR: poner a la izquierda de la flecha no indica que haya un nombre definido antes, ese nombre es el argumento, no a otra cosa que se llamase falso, por lo que representa cualquier valor. En este caso sabe el tipo por la última regla, pero todos los valores entrarían por la primera regla por que entran pares de cualquier cosa en cualquier cosa.

```
let verdadero = T;;
let falso = F;;

# let conj a b = match a, b with
          falso,_ -> falso |
          _,falso -> falso |
          _ -> verdadero;;

Warning 11 [redundant-case]: this match case is unused.
Warning 11 [redundant-case]: this match case is unused.
val conj : boolean -> boolean -> boolean = <fun>
```

- Ejemplo con solo constructores variables

```
(* tipo que da dos valores a cada int *)
#type dos = L of int | R of int;;

# L 3;;
- : dos = L 3

# R 3;;
- : dos = R 3
```

### Crear tu propio operador infijo

```
# let (&&&) = conj;;
val ( &&& ) : boolean -> boolean -> boolean = <fun>

# T &&& F &&& T;;
- : boolean = F
```
(* las definiciones en ocaml pueden utilizar el tipo de dato que se está definiendo, son recursivas por defecto *)



## --------------------- vie 18 nov
### Definiciones de tipos de datos 
Las definiciones de tipos en ocaml son recursivas por defecto, es decir que se puede usar un tipo en su propia declaración

- Declaración de tipo número natural (Z es cero y S es siguiente, infinitamente)

```
# type nat = 
        Z | S of nat;;
type nat = Z | S of nat

# Z;;
- : nat = Z

# S Z;;
- : nat = S Z

# S (S Z);;
- : nat = S (S Z)
```

- Suma de dos numeros de tipo Z (naturales)
```
# let rec sum n1 = function
        Z -> n1 |
        S n2 -> sum (S n1) n2;;
```

(match with)
```
# let rec sum n1 n2 = match n1 with
        Z -> n1 |
        S n2 -> sum (S n1) n2;;

val sum : nat -> nat -> nat = <fun>

# sum (S Z) (S Z);;
- : nat = S (S Z)
(* siguiente de zero + siguiente de zero = siguiente siguiente de zero = 2 *)
```

- Funcion que pasa de un int a su nat correspondiente
```
# let rec nat_of_int n = match n with
        0 -> Z |
        n -> S (nat_of_int (n-1));;
(* la definicion termina por qu el numero de ejecuciones es n, sin numeros negativos *)

# nat_of_int 6;;
- : nat = S (S (S (S (S (S Z)))))
```

otra forma
```
let rec nat_of_int n = 
        if n >= 0 then nat_of_int n
        else raise (Invalid_argument "nat_of_int");;
(* no es recursiva terminal, ejercicio no es dificil hacerla recursiva terminal *)
```

- Función que pasa de un nat a su int correspondiente (*HACER*)


### Árboles (btree y st_tree)

- E seria un árbol vacío, en otro caso primero va el valor de la raíz, y las otras dos las dos ramas izq y dcha
```
# type 'a btree =
    E |
    N of 'a * 'a btree * 'a btree;;
type 'a btree = Empty | Node of 'a * 'a bin_tree * 'a bin_tree

# N (1,E,E);;
- : int btree = N (1, E, E)
# let h x = N (x,E,E);;
val h : 'a -> 'a btree = <fun>
# h 10;;
-: int btree = N (10, E, E)
# N(1,N(2,E,E),h 10);;
- : int btree = N (1, N (2, E, E), N (10, E, E))
```

- Representación de un arbol binario
```
let t6 = N (6, h 5, h 11);;
let t9 = N (9, h 4, E);;
let t7 = N (7, h 2, t6);;
let t5 = N (5, E, t9);;
let t = N (2, t7, t5);;
```

- Devuelve el numero de nodos de un arbol
```
# let rec nnodos = function
    E -> 0 |
    N (_, i, d) -> 1 + nnodos i + nnodos d;;
```
- Devuelve la altura del arbol
```
let rec altura = function
    E -> 0 |
    N (_, i, d) -> 1 + max (altura i) (altura d);;
```

- Devuelve los valores de todos los nodos del arbol (preorden)
```
# let rec preorden = function
    E -> [] |
    N (r, i, d) -> r :: preorden i @ preorden d;;

# preorden t;;
- : int list = [2; 7; 2; 6; 5; 11; 5; 9; 4]
```

- Función que devuelve la lista de las etiquetas de las hojas del arbol
```
# let rec hojas = function
    E -> [] |
    N (r, E, E) -> [r] |
    N (r, i, d) -> hojas i @ hojas d;;
val hojas : 'a btree -> 'a list = <fun>
```

- Definición tipo lista (*es un árbol sin raíz*)
```
# type 'a lista =
    [] |
    (::) of 'a * 'a lista;;
```
Solo se pueden representar árboles con un número impar de nodos

- Definición de tipo árbol sin árbol vacío
```
# type 'a st_tree =
    Leaf of 'a
  | Node of 'a * 'a st_tree * 'a st_tree;;

(* Problema, sólo se pueden representar árboles estrictamente binarios (todos los nodos tienen dos hijos o ninguno) *)

Node (6, Leaf 5, Node(4, Leaf 3, Leaf 2));;
```
- Función mirror
```
# let rec mirror = function
    Leaf v -> Leaf v |
    Node (v,l,r) -> Node (v,r,l);;
```


## --------------------- mie 23 nov

Solo se pueden representar árboles con un número impar de nodos

### Árboles (g_tree)
- Definición del un arbol g
```
type 'a gtree =
        GT of 'a * 'a gtree list;;
```

El caso base es la lista vacía

```
# let root x = GT (x,[]);;

# root 6;;
- : int gtree = GT (6, [])

# let t9 = GT (9,[root 4]);;
# let t5 = GT (5,[GT (9, [GT (4, [])])]);;
# let t6 = GT (6, [root 5; root 11]);;
# let t7 = GT (7, [root 2; root 10; t6]);;
# let t = GT (2, [t7;t5]);;
```
- Calcular altura
```
# let rec height = function
    GT(_,[]) -> 1 |
    GT(r,h::t) -> max (height h + 1) (height (GT(r,t)));;

val height : 'a gtree -> int = <fun>
```

- Funcion que devuelve el numero de nodos
```
let rec nngt = function 
        GT (_,[]) -> 1 |
        GT (v,h::t) -> nngt h + nngt (GT(v,t));;
        (* no se puede pasar el tail a nngt por que no es un arbol, es una lista de árboles*)
```

- Funcion que deuvelve una lista con el valor de las hojas
```
let rec leaves = function
        GT(r,[]) -> [r] |
        GT (r,h::[]) -> leaves h |
        GT (r,h::t) -> leaves h @ leaves (GT(r,t));;
```

## --------------------- vie 25 nov
### Entrada/Salida secuencial

Output chanel es el 'dispositivo' en el que queremos implimir, por ejemplo `stdout`

```
# stdout;;
- : out_channel = <abstr>

# output_char;;
- : out_channel -> char -> unit = <fun>

# output_char stdout 'X';;
X - : unit = ()
```

- Funcion que imprima un char
```
let print_char c =
        output_char stdout c;;
```

- Función de un char seguido de 'Y' (con definición local)

```
let _ = print_char 'X' in print_char 'Y';;
```

- Función que imprima un string

```
(* Imprime la segunda letra del String Hola *)
# String.get "Hola" 2;;
- : char = 'l'
```

```
let optput_string c s =
        let n = String.length s in
        let rec loop i = 
                if i >= n then 
                        () (* devuelve unit *)
                else
                        (output_char c s.[i]; loop (i+1))
        in loop 0;;

val optput_string : out_channel -> string -> unit = <fun>

(* Importante meter el ; entre paréntesis!!!, es + débil que el if*)

# output_string stdout "Hola";;
Hola- : unit = ()
```

### Función ignore
Evita el warning de elemento no utilizado

```
# "a" ^ "b" ; 2 * 3;; 
Warning 10 [non-unit-statement]: this expression should have type unit.
- : int = 6

# let ignore _ = ();;
val ignore : 'a -> unit = <fun>

# ignore ("a" ^ "b") ; 2 * 3;;    
- : int = 6
```

### Especificar canal de salida

```
# open_out;;
- : string -> out_channel = <fun>
```

- salida a un archivo

```
# let salida = open_out "nombredelasalida";;
```

vamos a intentar escribir algo en ese archivo

```
# output_string salida "ahoahhapiapvahp";;
- : unit = ()
```

Por el buffer no le llega la info al archivo (hay que obligarle, por ejemplo cerrándolo) con `close_out`

```
# close_out salida;;
- : unit = ()
```

ahora si se han escrito.

- Sin cerrar el dispositivo, podemos volcar el buffer usando `flush`

```
# let salida = open_out "nombredelasalida";;
(* cuando se vuelve a abrir el contenido del documento se vacía!!*)

# output_string salida "nuevas cositaas";;

# flush salida;;

# output_string salida "buenos días";;

# flush salida;;

# close_out;;

```

### Especificar canal de entrada

```
# input_char;;
- : in_channel -> char = <fun>
```

- Tiene que existir el archivo que quiero leer
```
# let entrada = open_in "noexiste";;

exception
```

- Con uno que si funciona (va leyendo por cada input chR)

```
# let entrada = open_in "nombredelasalida";;
val entrada : in_channel = <abstr>

# input_char entrada;;
- : char = 'a'

# input_char entrada;;
- : char = 'h'

...
```

- Leer de la entrada estándar (teclado)

```
# stdin;;             
- : in_channel = <abstr>

# input_char stdin;;
holabuenaaass fjaooiap
- : char = 'h'

(* puedo borrar mientras no pulse enter *)

0;;   
Error: Unbound value olabuenaaass

(* es peligroso ya que la entrada estándar se comparte con el compilador*)
```

Por estos motivos, se lee de línea en línea con `input_line` o `read_line ()`

```
# input_line;;
- : in_channel -> string = <fun>

# input_line stdin;;
hola gud mornin 
- : string = "hola gud mornin"
```

Definiéndolo nosotros:

```
let read_line () = flush stdout;
                input_line stdin;;

(* vacia antes la salida estandar para que lo que haya escrito antes se haya visto ya*)
```

## --------------------- mie 30 nov

- Imprime una lista en donde le indiques

```
let rec output_string_list out = function
        []->() |
        h::t -> output_string (h^"\n"); 
                output_string_list out t;;

otput_string_list stdout ["1";"2";"3"];;

close_out salida;;
```

- Lo mismo con List.iter
```
let rec output_string_list out l = 
        List.iter (fun s -> output_string_list (s^"\n")) l;;
```

- usando `input_line` (canal de entrada)

```
let rec input_string_list input =
        try
                (*definiéndolo de esta manera te aseguras de que s se ejecuta antes que input_line input, si no no avanza nunca el puntero*)
                let s = input_string_list input in
                input_line input :: s
        with End_of_file -> [];;

let entrada = open in "nobredelarchivo";;

input_string_list entrada;;
```

- `pos_in` permite saber dónde está el puntero de lectura de un archivo

```
pos_in entrada;;
```

- `seek_in` indicando un canal de entrada me reposiciona el puntero al byte que le digas

### Programación imperativa

'a ref es un constructor de tipos

```
# ref;;
- : 'a -> 'a ref = <fun> (*crea una variable para guardar cualquier valor*)

# ref 0;;
- : int ref = {contents = 0}

# let i = ref 0;;
val i : int ref = {contents = 0}
```

- Declarar una variable e inicializarla

```
# let i = ref 0;;
val i : int ref = {contents = 0}
```

- No puedo sumar cajas con ints (hay que acceder al valor de la variable)

```
# i+1;;
Error: This expression has type int ref
       but an expression was expected of type int

```

para eso se usa el operador `(!)`

```
# !i+1;;  
- : int = 1
```

- Para modificar el valor de una variable `:=` (asignacion)

```
# i := 100;;
- : unit = ()

# i;;
- : int ref = {contents = 100}

# !i;;
- : int = 100
```

- Factorial imperativo

```
let fact n = 
        let p = ref 1 in
        for i = 1 to n do
                p := !p * i
        done;
        !p;; (*se evalua antes la variable y el bucle y luego imprime p*)

for i = 0 to 20 do
        print_int (fact i);
        print_newline()
done;;
```

## --------------------- vie 2 dic

### Bucles y variables

- For down to (valores menores o iguales que n hasta meyores o iguales que m)

```
(* el bucle for devuelve unit *)
for i = 0 downto 20 do
        print_int (fact i);
        print_newline()
done;;
```

la i del for no es una variable por lo que no se puede modificar su valor, ni tiene sentido fuera del for.

- While (expresión booleana mutable variable) do

```
(* el bucle while devuelve unit *)
let fact n =
        let f = ref 1 in
        let i = ref 1 in
                while !i <= n do
                        f:= !f * !i; i:= !i+1
                done;
                !f;; 
```

- Función que cada vez que se llama se suma un valor
```
let n = ref 0;; (* VARIABLE GLOBAL *)

let turno () =
        n:= !n +1; !n;;

# turno ();;
- : int = 1

# turno ();;
- : int = 2
```

No se puede escribir de forma funcional ya que no hay variables de esa forma.

- Función que define una variable antes de su ejecución
```
(* VARIABLES ESTÁTICAS, persisten de una llamada a otra *)

let turno =
        let n = ref 0 in
                function () -> n:= !n+1 ; !n;;

# turno();;
- : int = 1

# turno();;
- : int = 2
```

- Valiables locales con la ventaja de poder ser compartidas

```
(* let reset () =
        n:= 0;; *)

(* la izquierda de la coma se asigna a turno y la derecha a reset*)

let turno, reset =
        let n = ref 0 in
        (fun () -> n := !n +1; n),
        (fun () -> n := 0);;

(* hay formas más habituales de hacerlo*)
```

## --------------------- mie 6 dic

### Módulos

- Los nombres de módulos siempre empiezan con letra mayúscula.
- Cargar un módulo: `#load "Nombre_modulo.cmo"` u `open Nombre_modulo`.
    * `open` puede dar problemas cuando tenemos más de un módulo con el mismo nombre.
- `sig` (signatura) es la interfaz.
- `struct` (estructura) es la implementación.
- Hacer alias de módulo `module M = Modulo`.
- Llamar a las funciones sin poner `Modulo.funcion`: `open Modulo`

- `functor` función entre módulos, que se aplica a 0 o más módulos y devuelve un módulo.
    * Con `module M1 = Module (argumentos)` definimos un módulo.

Implementación de contador con Módulos

```
module Counter: sig (* como el archivo mli*)
    val turno: unit -> int
    val reset: unit -> unit
end = struct  (* como el archivo ml *)
    let n = ref 0

    let turno () =
        n := !n + 1;
        !n

    let reset () =
        n:= 0
end;;
```

Implementación de contador con `functor` de Módulos

```
module Counter (): sig (* como el archivo mli*)
    val turno: unit -> int
    val reset: unit -> unit
end = struct  (* como el archivo ml *)
    let n = ref 0

    let turno () =
        n := !n + 1;
        !n

    let reset () =
        n:= 0
end;;
```

## --------------------- vie 8 dic

- Módulo set

Modulo para comparar pares de enteros

```
module IntPair =
struct
    type t = int * int
    let compare = Stdlib.compare
end;;

```

Generamos un módulo mediante el functor de set

```
module IPSet = Set.Make (IntPair);;

```


> Funcion of_list del modulo Set

```
(* Manual *)
let trees_S = List.fold_left
              (fun s p -> IPSet.add p s) IPSet.empty trees;;

(* Usando función del módulo let *)
let trees_S

```

Comprobar rendimiento:

```
(*Generamos una lista de 50000 elementos*)
let trees = List.init 50_000 (fun _ -> 1 + Random.int 500,
                                       1 + Random.int 500);;

(*Generamos una lista de 5000 elementos*)
let to_find = List.init 5_000 (fun _ -> 1 + Random.int 500,
                                      1 + Random.int 500);;

(* Buscar que elementos hay de to_find en trees mediante el módulo list *)
let r1 = List.filter (fun p -> List.mem p trees to_find);;


(* Generamos árbol binario de búsqueda*)
let trees_S = List.fold_left (fun s p -> IPSet.add p s) IPSet.empty trees;

(* Buscar que elementos hay de to_find en trees mediante el módulo set *)
let r2 = List.filter (fun p -> IPSet.mem p trees_S) to_find;;

```


### Vectores

Un vector se define de la siguiente manera: `[|<valores>|]`:
    * int array: `[|1;2;3;4|]`
    * string array: `[|"hola mundo"|]`

Para obtener un valor de un array haremos `vector.(<valor>)`
Para cambiar el valor del contenido de una casilla haremos `vector.(<valor>) <- <nuevo_valor>` o `Array.set vector <pos> <valor>`

Generar array de n elementos: `Array.init n <fun>`

## --------------------- mie 14 dic

### Funciones con vectores

- Producto escalar
```
# let sprod v1 v2 =
        if Array.length v1 <> Array.length v2
                then (raise (Invalid_argument "sprod"))
        else (
        let p = ref 0.0 in
        for i = 0 to Array.length v1 - 1 do
                p := !p +. v1.(i) *. v2.(i)
        done;
        !p );;

# sprod [|1.;2.;3.|] [|0.;1.5;2.|];;
- : float = 9.

#sprod [|1.;2.;3.|] [|0.;1.5;2.;5.|];;                  )
Exception: Invalid_argument "sprod".
```

```
#let sprod v1 v2 =
        try
                Array.fold_left (+.) 0.0 (Array.map2 ( *. ) v1 v2)
        with Invalid_argument _ -> raise (Invalid_argument "sprod");;
        ;;

#sprod [|1.;2.;3.|] [|0.;1.5;2.;5.|];; 
Exception: Invalid_argument "sprod".
```

### Structs (registros)

```
# type persona = {nombre: string; edad: int};;

# {nombre = "Pepe"; edad = 57};;
- : persona = {nombre = "Pepe"; edad = 57}

# let pepe = {nombre = "Pepe"; edad = 57};;
val pepe : persona = {nombre = "Pepe"; edad = 57}

# pepe.edad;;
- : int = 57
```

No podemos cambiar el contenido de un campo de un registro

* No usar nombres de campos repetidos en los diferntes registros (el copilador no sabría de que tipo es)

```
# let cumpleanos p =
        {nombre = p.nombre; edad = p.edad + 1};;

# cumpleanos pepe;;
- : persona = {nombre = "Pepe"; edad = 58}

# let old_pepe = cumpleanos pepe;;
val old_pepe : persona = {nombre = "Pepe"; edad = 58}
```

- Registros mutables
```
# type persona_mutable = {nombre: string; mutable edad: int};;

# let pepe_mutado = {nombre = "Pepe"; edad = 57};;
val pepe_mutado : persona_mutable = {nombre = "Pepe"; edad = 57}

# pepe_mutado.edad <- 60;;

# pepe_mutado.edad;;
- : int = 60

# let envejece p =
        p.edad <- p.edad + 1;;

# envejece pepe_mutado;;

# pepe_mutado.edad;;
- : int = 61
```

- Así funcionan las variables

```
# let p = ref 0.0;;
val p : float ref = {contents = 0.}

# p.contents;;
- : float = 0.

# p.contents <- 100.0;;

# !p;;
- : float = 100.
```

- !

```
type 'a ref = {mutable contents 'a};;

let (!) v = v.contents;;

let (:=)  v x = v.contents <-;;
```

- Implementar cosas de algoritmos con ocaml (ejercicio)

## --------------------- vie 16 dic

### Objetos (inmediatos, sin clase)

- el tipo de los objetos dependen de los métodos y del tipo de estos
- los métodos de un objeto tienen que tener tipo en OCaml

```
# let c1 = object
        val mutable n = 0
        method turno =
                n <- n + 1;
                n
        method reset =
                n <- 0
end;;

val c1 : < reset : unit; turno : int > = <obj>
```

- Llamando a los métodos del objeto

```
# c1#turno;;
- : int = 1

# c1#turno + c1#turno;;
(* 2 + 3 *)
- : int = 5

(* se evalúan los paréntesis y dentro de los paréntesis el reset*)
# c1#turno + (c1#reset; c1#turno);;
(* 2 + 1 *)
- : int = 3
```
- Polimorfismo

Este método se aplicaría a cualquier ojeto que tenga, por lo menos, el método turno:

```
# let doble c1 =
        2 * c1#turno;;

val doble : < turno : int; .. > -> int = <fun>
```

- objeto de tipo objeto que tenga método turno y reset (se afectan)

```
# let doble_obj c1 = object
        method turno = 2 * c1#turno
        method reset = c1#reset
end;;
val doble_obj : < reset : 'a; turno : 'b; .. > -> < reset : 'a; turno : 'b > = <fun>

# let c2 = doble_obj c1;;
val c2 : < reset : unit; turno : int > = <obj>

# c1#turno;;
- : int = 6
# c2#turno;;
- : int = 14
(* 7 * 2 *)
```

### Clases

- Una clase es una receta para crear objetos

```
# class counter = object
        val mutable n = 0
        method turno =
                n <- n + 1;
                n
        method reset =
                n <- 0
end;;

class counter :
  object val mutable n : int method reset : unit method turno : int end
```

- Generamos un un objeto (automáticamente el compilador asigna el tipo counter con los métodos: `type counter = <turno: int, reset: unit>`)

```
let c3 = new counter;;
val c3 : counter = <obj>

(* es igual a *)

# c1;;
- : < reset : unit; turno : int > = <obj>
```

- Para comprobar que son iguales los meto en una lista (en una lista solo pueden ir elementos del mismo tipo)

```
# [c1;c3];;      
- : counter list = [<obj>; <obj>]
```

- Herencia (hereda los atributos y los métodos del objeto counter)

```
# class counter_w_set = object
        inherit counter
        method set i = 
                n <- i
end;;

(* terminal *)
class counter_w_set :
  object
    val mutable n : int
    method reset : unit
    method set : int -> unit
    method turno : int
end

# let cc = new counter_w_set;;
val cc : counter_w_set = <obj>

# cc#turno;;
- : int = 1
# cc#turno;;
- : int = 2
# cc#set 100;;
- : unit = ()
# cc#turno;;  
- : int = 101
```

- Superobject (as super) este objeto (this). Aquí no tiene mucho sentido (solo explicación)

```
# class counter_wSet = object (this)
    inherit counter as super
    method set i = 
    n <- i
end;;
```
## --------------------- mie 21 dic

### Método inicializador (initializer)

- Poner el contador al número deseado automáticamente siempre que se crea la clase 

```
class counter_wInit n0 = object (self)
    (* heredo características de counter_wSet*)
    inherit counter_wSet 
    initializer self#set n0
end;;

# let c = new conunter_wInit 27
```

- le ponemos número máximo al contador

```
class counter_wMax n0 max = oject (this)
    inherit counter_wInit n0+ as super
    method reset = this#set n0 (* override *)
    metod next = 
    (* se cambia el método next de la clase anterior*)
    let nx = super#next in
    if nx <= max then nx
        else (this#reset; super#next)
end;;
```

### Colas

```
class [a'] queue = object
    val mutable front = []
    val mutable back = []
    method enqueue (e:: a') =
        back <- e::back
    method dequeue = match front, back with
        h::t, _ -> front <- t; Some h 
        [],[] -> None
        [], _ -> front <- List.rev back;
            back <- [];
            self#dequeue
end;;

(* (e:: a') indica el tipo del valor e*)
```

```
let addlist l q =
    List.iter (fun e -> q#enqueue e) l ;;

let drain q =
    match 1#dequeue with
    None -> () |
    _ -> 
```

- Examen 2020
- árboles
```
type 'a tree = Tree of 'a * 'a tree list;;

let rec nodes = function
    Tree (_,[]) -> 1 |
    Tree (x,h::t) -> nodes h + nodes (Tree (x,t));;

let rec weight (Tree (x,l)) =
List.fold_left (fun n t -> n + weight t) x l;;

let mirror = function
    Tree (x,h::l) -> (Tree (h, (List.rev t)::x));;

# let leaf x = Tree (x, []);;

let leaves_weight tree =
        let rec aux acc = function
                Tree (x,[]) -> acc + x |
                Tree (x,h::t) -> aux acc (Tree(x,t))
        in aux 0 tree;;
```
- Recursividad terminal
```
# let rec g = function
      [] -> []|
      h::t -> h :: List.filter ((<) h) (g t);;

# let g l =
        let rec aux res = function
                [] -> res |
                h::t -> aux(h::(List.filter(fun x -> x > h)res)) t
        in aux [] (List.rev l);;
```

- árboles
```
type 'a tree = Tree of 'a * 'a tree list;;

let rec nodes = function
    Tree (_,[]) -> 1 |
    Tree (x,h::t) -> nodes h + nodes (Tree (x,t));;

let rec weight (Tree (x,l)) =
List.fold_left (fun n t -> n + weight t) x l;;

let mirror = function
    Tree (x,h::l) -> (Tree (h, (List.rev t)::x));;

let leaf x = Tree (x,[]);;

let t2 = Tree (1,[leaf 2; leaf 3]);;

let leaf_weight = function
        Tree(r,[]) -> r |
        Tree(r,h::[]) -> leaf_weight h |
        Tree(r,h::t) -> leaf_weight h + leaf_weight (Tree(r,t));;

```









