let  a_salto d (i1,j1) (i2,j2) =
  (i1 = i2 && abs (j2-j1) <= d) ||
  (j1 = j2 && abs (i2-i1) <= d);;

let saltos d path trees = match path with
  [] -> raise (Failure "No path") |
  h::t -> List.filter (fun coord -> a_salto d h coord && not (List.mem coord path)) trees
;;

let rec completeTour m n tree visited s path = function
[] -> path |
  h::t -> completeTour m n tree visited s (
  if h = (m,n)
    then List.rev (h::visited)::path
    else path @ (completeTour m n tree (h::visited) s path (saltos s (h::visited) tree))
  ) t;;

let shortest l = match l with
  [] -> raise Not_found |
  h::[] -> h |
  h::t ->	let rec aux l a =
    match (l) with
      [] -> a |
      h::t ->	match List.compare_lengths h a with
        1 | 0 -> aux t a |
        _ -> aux t h
in aux t h;;

let shortest_tour m n tree s =
  if (not (List.mem (1,1) tree ))
    then raise Not_found
    else  
      if (1,1) = (m,n)
        then [(1,1)]
        else shortest (completeTour m n tree [(1,1)] s [] 
          (saltos s [(1,1)] tree));;

(*
let trees = [(1,1); (1,4); (1,3); (3,3); (3,4); (4,4)];;
*)