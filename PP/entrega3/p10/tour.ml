let  a_salto d (i1,j1) (i2,j2) =
     (i1 = i2 && abs (j2-j1) <= d) ||
     (j1 = j2 && abs (i2-i1) <= d);;

let saltos d path trees = match path with
    [] -> raise (Failure "No path") |
    h::t -> List.filter (fun coord -> a_salto d h coord && not (List.mem coord path)) trees
;; (* devuelve la lista de los posibles saltos a los que ir desde el sitio en el que estamos (primer elemento del path) *)

let tour m n trees d =
    let rec completa path salto =
    if List.hd path = (m,n) then List.rev path else
    match salto with
        [] -> raise Not_found |
        h::t -> try completa (h::path) (saltos d (h::path) trees) with
                Not_found -> completa path t
in completa [(1,1)] (saltos d [(1,1)] trees) (* calc siguiente salto *)
;;