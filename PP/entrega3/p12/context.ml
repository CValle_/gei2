
type 'a context =
  (string * 'a) list;; (*lista de pares string, valor*)

exception No_binding of string;;

let empty_context = [];;

(* List.assoc busca el primer valor con ese nombre que encuentre y devuelve el segundo valor del par *)

let get_binding ctx name =
  try List.assoc name ctx with
  Not_found -> raise (No_binding name);;

  
(* añado los valores al principio de la lista para que el nuevo sea el primero que encuentre *)

let rec add_binding ctx name v =
  (name, v)::ctx;;

(*
con un fold left se pueden hacer varios bindings empezando con la lista contexto vacío y una lista de pares
let ctx = add_binding empty_context "uno" 1;;
let ctx = add_binding ctx "dos" 2;;
let ctx = add_binding ctx "tres" 3;;
*)

