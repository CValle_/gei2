
open Context;;
open Lib;;

type arith_oper =
    Opp;; (* opuesto *)

type arith_bi_oper =
    Sum | Sub | Prod | Div | Mod | Pow;;

type arith =
    Float of float
    | Var of string
    | Arith_op of arith_oper * arith
    | Arith_bi_op of arith_bi_oper * arith * arith
    | Fun_call of string * arith;;

let bi_oper = function 
    Sum -> (+.) |
    Sub -> (-.) |
    Prod -> ( *. ) |
    Div -> (/.) |
    Mod -> (Float.rem) |
    Pow -> ( ** )
;;

let oper = function 
    Opp -> ( *. ) (-1.)
;;


(* el valor del valor es independiente del contexto *)
(* si te pasan la variable tienes que buscar el valor *)

let rec eval ctx = function
    Float f -> f |
    Var name -> get_binding ctx name |
    Arith_op (op, v1) -> (oper op) (eval ctx v1) |
    Arith_bi_op (bop, v1, v2) -> (bi_oper bop) (eval ctx v1) (eval ctx v2) |
    Fun_call (s, v1) -> get_function s (eval ctx v1) (*check*)
;;

