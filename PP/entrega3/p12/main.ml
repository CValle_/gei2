
open Parsing;;
open Lexing;;

open Context;;
open Lib;;
open Arith;;
open Command;;
open Parser;;
open Lexer;;

exception Syntax_error of string;;

let rec loop ctx =
  print_string ">> ";

  try loop(
    try let cmd = s token (from_string (read_line ())) in 
      let newCtx = run ctx cmd in print_newline(); newCtx
    with 
      No_binding name -> print_endline ("Variable " ^ name ^ " not defined"); ctx |
      Function_not_defined name -> print_endline ("Function " ^ name ^ " not defined "); ctx |
      Lexical_error -> print_endline ("Lexical error"); ctx |
      Parse_error -> print_endline ("Syntax error"); ctx
  ) with 
    End_of_program -> ()

    
;;

let _ = print_endline "Floating point calculator..." in
let _ = loop empty_context in
print_endline "... bye!!!";;

(* cuando se asigna un valor a algo que se printee *)