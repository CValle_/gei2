open Context;;

exception Function_not_defined of string;;

let functions = [("sqrt", sqrt);("exp", exp);("ln", log);("round", Float.round)];; 
(* contexto de las funciones, resultado de hacer add binding en el empty context de "nombre de funcion" + funcion *)

let functions = List.fold_left (fun ctx (name, v) -> add_binding ctx name v) empty_context functions;;

(* let functions = add_binding empty_context "exp" exp;;*)

let get_function s = 
  try get_binding functions s with
  No_binding s -> raise (Function_not_defined s)
;;