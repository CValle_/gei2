
open G_tree;;

let rec breadth_first = function
    Gt (x, []) -> [x]
  | Gt (x, (Gt (y, t2))::t1) -> x :: breadth_first (Gt (y, t1@t2));;

let rec breadth_first_t t =
  let rec aux acc = function
      Gt (x, []) -> List.rev (x::acc) |
      Gt (x, (Gt (y, t2))::t1) -> aux (x::acc) (Gt (y, List.rev_append (List.rev t1) t2))
  in aux [] t;;

let rec deeptree n =
  if n = 1 then
    Gt(1,[])
  else 
    Gt (n , [deeptree (n-1)]);;

let t2 = deeptree 100000;;

