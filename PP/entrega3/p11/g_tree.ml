(*

let root x = Gt (x,[]);;

let t9 = Gt (9,[root 4]);;
let t5 = Gt (5,[Gt (9, [Gt (4, [])])]);;
let t6 = Gt (6, [root 5; root 11]);;
let t7 = Gt (7, [root 2; root 10; t6]);;
let t = Gt (2, [t7;t5]);;

*)

type 'a g_tree =
  Gt of 'a * 'a g_tree list
;;

let rec size = function 
  Gt (_, []) -> 1 |
  Gt (r, h::t) -> size h + size (Gt (r, t))
;;

let rec height = function
  Gt (_, []) -> 1 |
  Gt (r, h::t) -> max (height h+1) (height (Gt (r,t)));;

let rec leaves = function 
  Gt (r,[]) -> [r] | 
  Gt (r,l) -> List.flatten (List.map leaves l);;

let rec mirror = function 
  Gt (r,l) -> Gt(r, List.map mirror (List.rev l));;

let rec preorder = function 
    Gt (r,[]) -> [r] |
    Gt (r,l) -> r::List.flatten (List.map preorder l);;

let rec postorder = function 
    Gt (r,[]) -> [r] |
    Gt (r,l) -> List.flatten (List.map postorder l) @ [r];;