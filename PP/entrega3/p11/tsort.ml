open Bin_tree;;

let rec insert_tree ord x = function
  Empty -> Node (x, Empty, Empty) |
  Node (y,l,r) when ord x y -> Node(y, insert_tree ord x l, r) |
  Node (y,l,r) -> Node (y, l, insert_tree ord x r)
;;

let tsort ord l =
  inorder (List.fold_left (fun a x -> insert_tree ord x a) Empty l)
;;

