let rec divide l = match l with
    h1::h2::t -> let t1, t2 = divide t in (h1::t1, h2::t2)
  | _ -> l, [];;

let rec merge = function
    [], l | 
    l, [] -> l |
    h1::t1, h2::t2 -> 
        if h1 <= h2 then h1::merge (t1, h2::t2) else h2::merge (h1::t1, t2);;

(* La no terminalidad de divide y merge provoca Stack overflow con números muy grandes*)

let rec msort1 l = match l with
    [] | _::[] -> l |
    _ -> let l1, l2 = divide l in
        merge (msort1 l1, msort1 l2);;

let l2  = List.init 1000000 (function _ -> Random.int 10000);;

let divide' l =
  let rec aux l1 l2 = function
    [] -> (List.rev l1, List.rev l2) |
    h::[] -> (List.rev (h::l1), List.rev l2) |
    h1::h2::t -> aux (h1::l1) (h2::l2) t
  in aux [] [] l;;

let merge' ord (l1, l2) =
  let rec aux (a1, a2) m = match a1, a2 with
    [], l |
    l, [] -> List.rev_append m l |
    h1::t1, h2::t2 -> if ord h1 h2 then aux (t1, h2::t2) (h1::m)
                    else aux (h1::t1, t2) (h2::m)
in aux (l1, l2) [];;

let rec msort2 ord l = match l with
    [] |
    _::[] -> l |
    _ -> let l1, l2 = divide' l
        in merge' ord (msort2 ord l1, msort2 ord l2);;

(* los algoritmos tienen resultados muy similares entre ellos, es difícil decir cuál es mejor debido a lo variables que son los tiempos que se miden *)