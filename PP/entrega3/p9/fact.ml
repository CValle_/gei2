let rec fact = function
    0 -> 1 |
    n -> n * fact (n - 1);;
try
    Sys.argv.(1) |> int_of_string |> fact |> string_of_int |> print_endline
with
    Failure _  |
    Stack_overflow |
    Invalid_argument _ -> print_endline "argumento invalido";;