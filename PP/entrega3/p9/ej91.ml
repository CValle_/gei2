let to0from n = 
    let rec aux l n = match n with
        -1 -> List.rev l |
        n -> aux (n::l) (n-1)
    in aux [] n;;

let fromto m n =
    let rec aux m l =
        if m > n then
            List.rev l
        else
            aux (m+1) (m::l)
    in aux m [];;

let incseg l = 
    let rec aux l res l2 = match l with
    [] -> [] |
    [h] -> List.rev ((h + res)::l2) |
    h::t -> aux t (h + res) ((h + res)::l2)
    in aux l 0 [];;

let remove x l =
    let rec aux res l = match l with
        [] -> l |
        h::t -> if x <> h then
                    aux (h::res) t
                else
                    List.rev_append res t 
    in aux [] l ;;

let compress l =
    let rec aux res l = match l with
        []-> List.rev res |
        [h] -> aux (h::res) [] | 
        h1::h2::t -> if h1=h2 then aux res (h2::t)
                    else aux (h1::res) (h2::t)
    in (aux [] l);;


