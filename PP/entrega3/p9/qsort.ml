let rec qsort1 ord = function
    [] -> [] |
    h::t -> let after, before = List.partition (ord h) t in 
        qsort1 ord before @ h :: qsort1 ord after;;

(* cuando el conjunto de datos de la entrada está desbalanceado esta implementación será peor *)

let rec qsort2 ord =
    let append' l1 l2 = List.rev_append (List.rev l1) l2 in
        function
        []->[] |
        h::t -> let after, before = List.partition (ord h) t in
            append' (qsort2 ord before) (h::qsort2 ord after);;

(* una ventaja de qsort2 ordena listas de mayor tamaño que qsort1 *)

let l1  = List.init 1000000 (function _ -> Random.int 10000);;

(* una desventaja de qsort2 es que qsort1 es más rápido cuando la lista se ordena de forma aleatoria y descendente, en concreto, qsort2 fue un 98% más lento ya que tiene que hacer el rev y rev_append cada vez*)