package es.udc.sistemasinteligentes;

import java.util.ArrayList;
import java.util.Stack;

public class EstrategiaBusquedaGrafo implements EstrategiaBusqueda{

    @Override
    public Nodo[] soluciona(ProblemaBusqueda p) throws Exception{
//        Queue<Nodo> frontera = new LinkedList<>(); //anchura
        Stack<Nodo> frontera = new Stack<>(); //profundidad
        Nodo nodoActual = new Nodo(p.getEstadoInicial(), null, null);
        frontera.add(nodoActual);
        ArrayList<Estado> explorados = new ArrayList<>();

        int i = 1;
        int nCreados = 1;
        System.out.println((i++) + " - Empezando búsqueda en " + nodoActual.estado);

        while(true) {
            if (frontera.isEmpty())
                throw new Exception("No se ha podido encontrar una solución");
//            nodoActual = frontera.remove(); //anchura
            nodoActual = frontera.pop(); //profundidad
            System.out.println((i++) + " ! Estado actual cambiado a " + nodoActual.estado);
            if (p.esMeta(nodoActual.estado)) break;
            else {
                System.out.println((i++) + " - " + nodoActual.estado + " no es meta");
                explorados.add(nodoActual.estado);
                Accion[] accionesDisponibles = p.acciones(nodoActual.estado);
                for (Accion acc : accionesDisponibles) {
                    Estado sc = p.result(nodoActual.estado, acc);
                    Nodo nodo = new Nodo(sc, nodoActual, acc); nCreados++;
                    System.out.println((i++) + " - RESULT(" + nodoActual.estado + ","+ acc + ")=" +sc);
                    if (!frontera.contains(nodo) && !explorados.contains(sc)) {
                        frontera.add(nodo);
                        System.out.println((i++) + " - " + sc + " NO explorado");
                        System.out.println((i++) + " - Nodo anadido a frontera" + nodo);
                    }
                    else
                        System.out.println((i++) + " - " + sc + " ya explorado");
                }
            }
        }
        System.out.println((i) + " FIN ---" + nodoActual);
        return reconstruyeSol(nodoActual);
    }

    @Override
    public Nodo[] reconstruyeSol(Nodo nodo) throws Exception {
        System.out.println("Reconstruyendo sol:");

        ArrayList<Nodo> solucion = new ArrayList<>();
        Nodo a = nodo;
        while (a != null){
            solucion.add(a);
            a = a.getPadre();
        }

        return solucion.toArray(new Nodo[solucion.size()]);
    }
}
