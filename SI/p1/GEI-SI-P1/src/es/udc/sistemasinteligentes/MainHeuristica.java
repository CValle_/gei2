package es.udc.sistemasinteligentes;

import es.udc.sistemasinteligentes.cuadradoMagico.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MainHeuristica {

    public static void main(String[] args) throws Exception {
        int n = 4;

        int[][] matriz = {
                {2,8,15,9},
                {14,12,5,0},
                {0,0,0,0},
                {0,0,0,0},
        };

        EstadoCuadrado estadoInicial = new EstadoCuadrado(n,matriz);
        ProblemaBusqueda cuadrado = new ProblemaCuadradoMagico(estadoInicial);
        Heuristica heuristica = new HeuristicaCuadrado();

        EstrategiaBusquedaInformada buscador = new EstrategiaBusquedaA();

        NodoHeuristico[] solucion = buscador.soluciona(cuadrado, heuristica);
        ArrayList<NodoHeuristico> listaNodos = new ArrayList<>(Arrays.asList(solucion));
        Collections.reverse(listaNodos);

        for(NodoHeuristico i : listaNodos)
            System.out.println(i.toString());
    }
}