package es.udc.sistemasinteligentes;

import java.util.ArrayList;
import java.util.List;

public class Nodo {
    Estado estado;
    Nodo padre;
    Accion accion;

    public Nodo(Estado estado, Nodo padre, Accion accion) {
        this.estado = estado;
        this.padre = padre;
        this.accion = accion;
    }

    public static Nodo creaNodo(Estado estado, Nodo padre, Accion accion) {
        Nodo nodo = new Nodo(estado, padre, accion);
        nodo.estado = estado;
        nodo.padre = padre;
        nodo.accion = accion;
        return nodo;
    }

    public Estado getEstado() {
        return estado;
    }

    public Nodo getPadre() {
        return padre;
    }

    @Override
    public String toString() {
        return "Nodo{" +
                "estado=" + estado +
                ", accion=" + accion +
                '}';
    }
}
