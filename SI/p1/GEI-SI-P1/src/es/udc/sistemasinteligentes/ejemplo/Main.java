package es.udc.sistemasinteligentes.ejemplo;

import es.udc.sistemasinteligentes.*;

import java.util.Collections;

public class Main {

    public static void printArray(Nodo[] arrL){
        for (Nodo nodo: arrL){
            System.out.println(nodo.toString());
        }
    }

    public static void main(String[] args) throws Exception {
        ProblemaAspiradora.EstadoAspiradora estadoInicial = new ProblemaAspiradora.EstadoAspiradora(ProblemaAspiradora.EstadoAspiradora.PosicionRobot.IZQ,
                                                                                                    ProblemaAspiradora.EstadoAspiradora.PosicionBasura.AMBAS);
        ProblemaBusqueda aspiradora = new ProblemaAspiradora(estadoInicial);

        //EstrategiaBusqueda buscador = new Estrategia4();
        EstrategiaBusqueda buscador = new EstrategiaBusquedaGrafo();

        Nodo[] nodosL = buscador.soluciona(aspiradora);

        System.out.println("\nSoluciona: ");
        //System.out.println(buscador.soluciona(aspiradora));
        printArray(nodosL);

        System.out.println("\nReconstruye sol: ");
        Nodo[] solucion = buscador.reconstruyeSol(nodosL[(nodosL.length)-1]);
        printArray(solucion);
    }
}
