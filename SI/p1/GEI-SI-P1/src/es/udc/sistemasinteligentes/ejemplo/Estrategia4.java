package es.udc.sistemasinteligentes.ejemplo;

import es.udc.sistemasinteligentes.*;
import java.util.ArrayList;

public class Estrategia4 implements EstrategiaBusqueda {

    public Estrategia4() {
    }

    @Override
    public Nodo[] soluciona(ProblemaBusqueda p) throws Exception{
        ArrayList<Estado> explorados = new ArrayList<Estado>();
        ArrayList<Nodo> sucesores = new ArrayList<>();

        Estado estadoActual = p.getEstadoInicial();
        Nodo nodoAct = Nodo.creaNodo(estadoActual, null, null);

        sucesores.add(nodoAct);
        explorados.add(estadoActual);

        int i = 1;

        System.out.println((i++) + " - Empezando búsqueda en " + estadoActual);

        while (!p.esMeta(estadoActual)){
            System.out.println((i++) + " - " + estadoActual + " no es meta");
            Accion[] accionesDisponibles = p.acciones(estadoActual);
            boolean modificado = false;

            for (Accion acc: accionesDisponibles) {
                Estado sc = p.result(estadoActual, acc);
                System.out.println((i++) + " - RESULT(" + estadoActual + ","+ acc + ")=" + sc);

                //Mantener el array estados
                if (!explorados.contains(sc)) {

                    estadoActual = sc;
                    nodoAct = Nodo.creaNodo(sc, nodoAct, acc);

                    System.out.println((i++) + " - " + sc + " NO explorado");

                    sucesores.add(nodoAct);
                    explorados.add(estadoActual);
                    modificado = true;
                    System.out.println((i++) + " - Estado actual cambiado a " + estadoActual);
                    break;
                }
                else
                    System.out.println((i++) + " - " + sc + " ya explorado");
            }
            if (!modificado) throw new Exception("No se ha podido encontrar una solución");
        }
        System.out.println((i++) + " - FIN - " + estadoActual);

        return sucesores.toArray(new Nodo[sucesores.size()]);
    }

    public Nodo[] reconstruyeSol(Nodo nodo) throws Exception {
        ArrayList<Nodo> solucion = new ArrayList<>();
        Nodo a = nodo;
        while (a != null){
            solucion.add(a);
            a = a.getPadre();
        }

        return solucion.toArray(new Nodo[solucion.size()]);
    }
}
