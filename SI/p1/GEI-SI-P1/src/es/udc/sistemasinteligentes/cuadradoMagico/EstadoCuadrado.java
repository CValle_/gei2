package es.udc.sistemasinteligentes.cuadradoMagico;

import es.udc.sistemasinteligentes.Estado;

import java.util.Arrays;
import java.util.Objects;

public class EstadoCuadrado extends Estado {
    private final int n;
    private final int[][] cuadrado;

    public EstadoCuadrado(int n, int[][] cuadrado) {
        this.n = n;
        this.cuadrado = cuadrado;
    }

    /**
     * @return número de filas y columnas
     */
    public int getN() {
        return n;
    }

    /**
     * @return matriz con los valores de cada casilla
     */
    public int[][] getCuadrado() {
        return cuadrado;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("{");
        for(int i=0;i<n;i++) {
            str.append("{");
            for (int j = 0; j < n; j++) {
                str.append(cuadrado[i][j]).append(",");
            }
            str.append("}");
        }
        str.append("}");
        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EstadoCuadrado that)) return false;
        return n == that.n && Arrays.deepEquals(cuadrado, that.cuadrado);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(n);
        result = 31 * result + Arrays.deepHashCode(cuadrado);
        return result;
    }
}
