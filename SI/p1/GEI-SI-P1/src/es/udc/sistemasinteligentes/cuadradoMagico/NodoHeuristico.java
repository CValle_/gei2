package es.udc.sistemasinteligentes.cuadradoMagico;

import es.udc.sistemasinteligentes.Accion;
import es.udc.sistemasinteligentes.Estado;
import es.udc.sistemasinteligentes.Heuristica;
import es.udc.sistemasinteligentes.Nodo;

public class NodoHeuristico implements Comparable<NodoHeuristico>{
    NodoHeuristico padre;
    Estado estado;
    Accion accion;
    float coste;
    float f;

    public NodoHeuristico(NodoHeuristico padre, Estado estado, Accion accion, Heuristica heuristica) {
        this.padre = padre;
        this.estado = estado;
        this.accion = accion;
        if(padre != null){
            this.coste = padre.coste + accion.getCoste();
            if(heuristica != null)
                this.f = this.coste + heuristica.evalua(estado);
        }
    }


    public Estado getEstado() {
        return estado;
    }

    @Override
    public String toString() {
        return "Nodo{" +
                "estado=" + estado +
                ", accion=" + accion +
                '}';
    }

    @Override
    public int compareTo(NodoHeuristico nodo) {
        // el orden se basa en la funcion f
        return nodo.f < this.f ? 1 : -1;
    }
}
