package es.udc.sistemasinteligentes.cuadradoMagico;
import es.udc.sistemasinteligentes.*;

import java.util.*;

public class EstrategiaBusquedaA implements EstrategiaBusquedaInformada {

    public EstrategiaBusquedaA() {
    }

    @Override
    public NodoHeuristico[] soluciona(ProblemaBusqueda p, Heuristica h) throws Exception {
        Queue<NodoHeuristico> frontera = new PriorityQueue<>();
        NodoHeuristico nodoActual = new NodoHeuristico(null, p.getEstadoInicial(), null, h);
        frontera.add(nodoActual);
        ArrayList<Estado> explorados = new ArrayList<>();

        int i = 1;
        System.out.println((i++) + " - Empezando búsqueda en " + nodoActual.estado);

        NodoHeuristico nodo;
        while (true) {
            if (frontera.isEmpty())
                throw new Exception("No se ha podido encontrar una solución");
            nodoActual = frontera.remove();
            System.out.println((i++) + " ! Estado actual cambiado a " + nodoActual.estado);
            if (p.esMeta(nodoActual.estado)) break;
            else {
                System.out.println((i++) + " - " + nodoActual.estado + " no es meta");
                explorados.add(nodoActual.estado);
                Accion[] accionesDisponibles = p.acciones(nodoActual.estado);
                for (Accion acc : accionesDisponibles) {
                    Estado sc = p.result(nodoActual.estado, acc);
                    nodo = new NodoHeuristico(nodoActual, sc, acc, h);
                    System.out.println((i++) + " - RESULT(" + nodoActual.estado + "," + acc + ")=" + sc);
                    if (!explorados.contains(sc)) {
                        if (!frontera.contains(nodo) && !explorados.contains(sc)) {
                            frontera.add(nodo);
                            System.out.println((i++) + " - " + sc + " NO explorado");
                            System.out.println((i++) + " - Nodo anadido a frontera" + nodo);
                        } else
                            System.out.println((i++) + " - " + sc + " ya explorado");
                    }
                }
            }
        }
        System.out.println((i) + " FIN ---" + nodoActual);
        return reconstruyeSol(nodoActual);
    }

    /**
     * Calcula el camino para llegar a la solucion dado el nodo final
     */
    private NodoHeuristico[] reconstruyeSol(NodoHeuristico nodo) {
        System.out.println("Reconstruyendo sol:");
        ArrayList<NodoHeuristico> solucion = new ArrayList<NodoHeuristico>();
        NodoHeuristico actual = nodo;
        while(actual != null){
            solucion.add(actual);
            actual = actual.padre;
        }
        return solucion.toArray(new NodoHeuristico[0]);
    }
}
