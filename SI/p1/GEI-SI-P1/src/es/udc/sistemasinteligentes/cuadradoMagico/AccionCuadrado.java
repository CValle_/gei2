package es.udc.sistemasinteligentes.cuadradoMagico;

import es.udc.sistemasinteligentes.Accion;
import es.udc.sistemasinteligentes.Estado;

public class AccionCuadrado extends Accion {
    int x;
    int y;
    int num;

    public AccionCuadrado(int x, int y, int num) {
        this.x = x;
        this.y = y;
        this.num = num;
    }

    @Override
    public String toString() {
        return "(" + "(" + x + "," + y + ")" + num + ")";
    }

    /**
     * Miramos si la acción realizada es aplicable
     */
    @Override
    public boolean esAplicable(Estado es) {
        EstadoCuadrado esC = (EstadoCuadrado) aplicaA(es);

        //posición ya está ocupada (no es aplicable)
        if (((EstadoCuadrado)es).getCuadrado()[x][y] != 0) return false;

        int num = esC.getN();
        //número resultado
        int maxN = (num*((num*num)+1))/2;

        //Diagoanles
        int sumd1 = 0,sumd2=0;
        //Filas y columnas
        int rowSum = 0, colSum = 0;

        //Sumamos tödo a la vez de forma que si te pasas de maxN ya no es aplicable
        for (int i = 0; i < esC.getN(); i++) {
            if (maxN < (sumd1 += esC.getCuadrado()[i][i])) return false;
            if (maxN < (sumd2 += esC.getCuadrado()[i][esC.getN()-1-i])) return false;
            if (maxN < (rowSum += esC.getCuadrado()[x][i])) return false;
            if (maxN < (colSum += esC.getCuadrado()[i][y])) return false;
        }
        //Entiendo que no se compueba por de bajo ya que siempre te pasas en añgún punto al no repetirse números
        return true;
    }

    /**
     * Realizar la acción
     */
    @Override
    public Estado aplicaA(Estado es) {
        EstadoCuadrado esC = ((EstadoCuadrado) es);
        //Copiamos la matriz del estado anterior
        int[][] matriz = new int[esC.getN()][esC.getN()];

        for(int i = 0 ; i < esC.getN() ; i++)
            System.arraycopy(esC.getCuadrado()[i], 0, matriz[i], 0, esC.getN());

        //ponemos en la posición inidicada el número indicado
        matriz[x][y] = num;

        return new EstadoCuadrado(esC.getN(), matriz);
    }
}
