package es.udc.sistemasinteligentes.cuadradoMagico;

import es.udc.sistemasinteligentes.Accion;
import es.udc.sistemasinteligentes.Estado;
import es.udc.sistemasinteligentes.ProblemaBusqueda;

import java.util.ArrayList;

public class ProblemaCuadradoMagico extends ProblemaBusqueda {

    public ProblemaCuadradoMagico(Estado estadoInicial) {
        super(estadoInicial);
    }

    @Override
    public boolean esMeta(Estado es) {
        EstadoCuadrado esC = (EstadoCuadrado) es;

        int sumd1 = 0,sumd2=0;
        for (int i = 0; i < esC.getN(); i++) {
            sumd1 += esC.getCuadrado()[i][i];
            sumd2 += esC.getCuadrado()[i][esC.getN()-1-i];
        }
        //compara que las diagonales sumen lo mismo
        if(sumd1!=sumd2)
            return false;

        for (int i = 0; i < esC.getN(); i++) {
            int rowSum = 0, colSum = 0;
            for (int j = 0; j < esC.getN(); j++) {
                rowSum += esC.getCuadrado()[i][j];
                colSum += esC.getCuadrado()[j][i];
            }
            //compara que las filas y columnas sumen lo mismo
            if (rowSum != colSum || colSum != sumd1)
                return false;
        }
        return true;
    }

    @Override
    public Accion[] acciones(Estado es){
        EstadoCuadrado esC = (EstadoCuadrado) es;
        ArrayList<Accion> listaAcciones = new ArrayList<>();
        ArrayList<Integer> valoresIncluidos = new ArrayList<>();
        ArrayList<Integer> valoresExcluidos = new ArrayList<>();

        // mete todos los valores de la matriz en una lista
        for (int i = 0; i < esC.getN(); i++) {
            for (int j = 0; j < esC.getN(); j++) {
                valoresIncluidos.add(esC.getCuadrado()[i][j]);
            }
        }
        // hace una lista con los valores que no están en la matriz
        for(int i=1;i<= (esC.getN() * esC.getN());i++){
            if(!valoresIncluidos.contains(i))
                valoresExcluidos.add(i);
        }

        // crea las posibles acciones para cada casilla vacía
        for (int i = 0; i < esC.getN(); i++) {
            for (int j = 0; j < esC.getN(); j++) {
                if(esC.getCuadrado()[i][j] == 0){
                    for(Integer item : valoresExcluidos){
                        Accion a = new AccionCuadrado(i,j,item);
                        if (a.esAplicable(esC))
                            //lista de acciones se llena con las acciones aplicables
                            listaAcciones.add(a);
                    }
                }
            }
        }

        return listaAcciones.toArray(new Accion[0]);
    }
}
