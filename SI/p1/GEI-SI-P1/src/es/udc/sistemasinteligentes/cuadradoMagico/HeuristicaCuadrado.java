package es.udc.sistemasinteligentes.cuadradoMagico;

import es.udc.sistemasinteligentes.Estado;
import es.udc.sistemasinteligentes.Heuristica;

public class HeuristicaCuadrado extends Heuristica {

    /**
     * Calcula la puntuacion de una fila,columna o diagonal
     *  sum suma de la fila,columna o diagonal
     *  maxN numero maximo que puede tener
     *  complete si esta completa o no
     */
    private int score(int sum, int maxN, boolean complete){
        //terminado
        if(sum == maxN && complete) return 0;
        //incompleto
        else if(sum < maxN && !complete) return 1;
        //cualquier otra opción el coste es muy alto
        else return 1000;
    }

    /**
     * Calcula la heuristica de un estado
     */
    @Override
    public float evalua(Estado e) {
        EstadoCuadrado esC = (EstadoCuadrado) e;

        int result = 0;
        int n = esC.getN();
        int[][] cuadrado = esC.getCuadrado();
        int maxN = (n*((n*n)+1))/2;

        int sumd1 = 0, sumd2=0;
        boolean sized1 = true, sized2 = true;
        // recorre las diagonales, seran falso si no están llenas
        for (int i = 0; i < n; i++) {
            sumd1 += cuadrado[i][i];
            if (cuadrado[i][i] == 0) sized1 = false;
            sumd2 += cuadrado[i][n-1-i];
            if (cuadrado[i][n-1-i] == 0) sized2 = false;
        }

        //añadiendo al coste del camino
        result += score(sumd1, maxN, sized1);
        result += score(sumd2, maxN, sized2);

        // recorre las filas y columnas
        for (int i = 0; i < n; i++) {
            int rowSum = 0, colSum = 0;
            boolean fullRow = true, fullCol = true;
            for (int j = 0; j < n; j++) {
                rowSum += cuadrado[i][j];
                if (cuadrado[i][j] == 0) fullRow = false;
                colSum += cuadrado[j][i];
                if (cuadrado[j][i] == 0) fullCol = false;
            }
            result += score(rowSum, maxN, fullRow);
            result += score(colSum, maxN, fullCol);
        }

        return result;
    }
}
