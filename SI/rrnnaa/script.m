precisionEntrenamiento = [];
precisionValidacion = [];
precisionTest = [];
for i=1:50,
    rna = patternnet([18]);
    rna.trainParam.showWindow = false;
    [rna, tr] = train(rna, entradas, salidasDeseadas);
    flagsOutputs = sim(rna, entradas);
    precisionEntrenamiento(end+1) = 1-confusion(salidasDeseadas(:,tr.trainInd), flagsOutputs(:,tr.trainInd));   
    precisionValidacion(end+1) = 1-confusion(salidasDeseadas(:,tr.valInd),flagsOutputs(:,tr.valInd));
    precisionTest(end+1) = 1-confusion(salidasDeseadas(:,tr.testInd), flagsOutputs(:,tr.testInd));
end;

mediaPrecisionTest = mean(precisionTest);
desviacionTipicaPrecisionTest = std(precisionTest);
