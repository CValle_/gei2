#include "process_list.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <pwd.h>

void priority(int substring_size, char *substring[substring_size]);
void cmd_showvar(int substring_size, char* substring[substring_size],char *arg3[]);
void cmd_changevar(int substring_size, char *substring[substring_size], char *arg3[]);
void cmd_fork ();
void cmd_showenv(int substring_size, char *substring[substring_size],char *arg3[]);
void executeCmd(int substring_size, char *substring[substring_size]);
void job(int substring_size, char *substring[substring_size], tProcessList* processList);
void listjobs(int substring_size, char *substring[substring_size], tProcessList* processList);
void deljobs(int substring_size, char *substring[substring_size], tProcessList* processList);
void execute(int substring_size, char *substring[substring_size], tProcessList* processList);
