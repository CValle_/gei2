#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>


#define COMMAND_LENGTH_LIMIT 1024
#define LNULL NULL

typedef enum {FINISHED, STOPPED, SIGNALED, ACTIVE} tProcessStatus;


typedef struct ProcessNodeL* tProcessPosL;

typedef struct tProcessDataL {
    pid_t pid;
    tProcessStatus status;
    int signal;
    struct tm time;
    int priority;
    char commandLine[COMMAND_LENGTH_LIMIT];

} tProcessDataL;

struct ProcessNodeL {
    tProcessDataL data;
    tProcessPosL next;
};

typedef tProcessPosL tProcessList;



void createEmptyProcessList(tProcessList *L);

bool insertProcess(tProcessDataL d, tProcessPosL p, tProcessList *L);

void deleteProcess(tProcessPosL p, tProcessList *L);

void updateProcess(tProcessDataL d, tProcessPosL p, tProcessList *L);

bool isEmptyProcessList(tProcessList L);

tProcessPosL firstProcess(tProcessList L);

tProcessPosL lastProcess(tProcessList L);

tProcessPosL nextProcess(tProcessPosL p, tProcessList L);

tProcessPosL previousProcess(tProcessPosL p, tProcessList L);

tProcessDataL getProcess(tProcessPosL p, tProcessList L);

tProcessPosL findProcess(pid_t pid, tProcessList L);

void deleteProcessList(tProcessList* L);

void deleteProcStatus(tProcessList* L, tProcessStatus status);
