#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

#define LNULL NULL
#define NAME_LENGTH_LIMIT 256

typedef struct tNode *tPosL;
typedef char tItemL[NAME_LENGTH_LIMIT];
struct tNode{
    tItemL data;
    tPosL next;
};
typedef tPosL tList;

void createEmptyList (tList *L);

tPosL next(tPosL p,tList L);

bool insertItem(char* data, tPosL p, tList *L);

tPosL last(tList L);

bool isEmptyList(tList L);

void deleteList(tList *L);

char* getItem(tPosL p, tList L);

tPosL first(tList L);
