#include "processes.h"
#define MAXNAME 1024
#define MAXVAR 1024

extern char **environ;

static const char *STATUS[] = {
    "FINISHED", "STOPPED", "SIGNALED", "ACTIVE",
};

char * Ejecutable (char *s){
	char path[MAXNAME];
	static char aux2[MAXNAME];
	struct stat st;
	char *p;
	if (s==NULL || (p=getenv("PATH"))==NULL)
		return s;
	if (s[0]=='/' || !strncmp (s,"./",2) || !strncmp (s,"../",3))
        return s;       /*is an absolute pathname*/
	strncpy (path, p, MAXNAME);
	for (p=strtok(path,":"); p!=NULL; p=strtok(NULL,":")){
       sprintf (aux2,"%s/%s",p,s);
	   if (lstat(aux2,&st)!=-1)
		return aux2;
	}
	return s;
}

int OurExecvpe(char *file, char *const argv[], char *const envp[]){
   return (execve(Ejecutable(file),argv, envp));
}


int BuscarVariable (char * var, char *e[])  /*busca una variable en el entorno que se le pasa como parámetro*/
{
  int pos=0;
  char aux[MAXVAR];

  strcpy (aux,var);
  strcat (aux,"=");

  while (e[pos]!=NULL)
    if (!strncmp(e[pos],aux,strlen(aux)))
      return (pos);
    else
      pos++;
  errno=ENOENT;   /*no hay tal variable*/
  return(-1);
}


int CambiarVariable(char * var, char * valor, char *e[]) /*cambia una variable en el entorno que se le pasa como parámetro*/
{                                                        /*lo hace directamente, no usa putenv*/
  int pos;
  char *aux;

  if ((pos=BuscarVariable(var,e))==-1)
    return(-1);

  if ((aux=(char *)malloc(strlen(var)+strlen(valor)+2))==NULL)
	return -1;
  strcpy(aux,var);
  strcat(aux,"=");
  strcat(aux,valor);
  e[pos]=aux;
  return (pos);
}


/*las siguientes funciones nos permiten obtener el nombre de una senal a partir
del número y viceversa */
struct SEN{
  char *nombre;
  int senal;
};
static struct SEN sigstrnum[]={
	{"HUP", SIGHUP},
	{"INT", SIGINT},
	{"QUIT", SIGQUIT},
	{"ILL", SIGILL},
	{"TRAP", SIGTRAP},
	{"ABRT", SIGABRT},
	{"IOT", SIGIOT},
	{"BUS", SIGBUS},
	{"FPE", SIGFPE},
	{"KILL", SIGKILL},
	{"USR1", SIGUSR1},
	{"SEGV", SIGSEGV},
	{"USR2", SIGUSR2},
	{"PIPE", SIGPIPE},
	{"ALRM", SIGALRM},
	{"TERM", SIGTERM},
	{"CHLD", SIGCHLD},
	{"CONT", SIGCONT},
	{"STOP", SIGSTOP},
	{"TSTP", SIGTSTP},
	{"TTIN", SIGTTIN},
	{"TTOU", SIGTTOU},
	{"URG", SIGURG},
	{"XCPU", SIGXCPU},
	{"XFSZ", SIGXFSZ},
	{"VTALRM", SIGVTALRM},
	{"PROF", SIGPROF},
	{"WINCH", SIGWINCH},
	{"IO", SIGIO},
	{"SYS", SIGSYS},
/*senales que no hay en todas partes*/
#ifdef SIGPOLL
	{"POLL", SIGPOLL},
#endif
#ifdef SIGPWR
	{"PWR", SIGPWR},
#endif
#ifdef SIGEMT
	{"EMT", SIGEMT},
#endif
#ifdef SIGINFO
	{"INFO", SIGINFO},
#endif
#ifdef SIGSTKFLT
	{"STKFLT", SIGSTKFLT},
#endif
#ifdef SIGCLD
	{"CLD", SIGCLD},
#endif
#ifdef SIGLOST
	{"LOST", SIGLOST},
#endif
#ifdef SIGCANCEL
	{"CANCEL", SIGCANCEL},
#endif
#ifdef SIGTHAW
	{"THAW", SIGTHAW},
#endif
#ifdef SIGFREEZE
	{"FREEZE", SIGFREEZE},
#endif
#ifdef SIGLWP
	{"LWP", SIGLWP},
#endif
#ifdef SIGWAITING
	{"WAITING", SIGWAITING},
#endif
 	{NULL,-1},
	};    /*fin array sigstrnum */


int valorSenal(char * sen)  /*devuelve el numero de senial a partir del nombre*/
{
  int i;
  for (i=0; sigstrnum[i].nombre!=NULL; i++)
  	if (!strcmp(sen, sigstrnum[i].nombre))
		return sigstrnum[i].senal;
  return -1;
}


char *nombreSenal(int sen)  /*devuelve el nombre senal a partir de la senal*/
{			/* para sitios donde no hay sig2str*/
 int i;
  for (i=0; sigstrnum[i].nombre!=NULL; i++)
  	if (sen==sigstrnum[i].senal)
		return sigstrnum[i].nombre;
 return ("SIGUNKNOWN");
}



void cmd_fork (tProcessList* processList){
	pid_t pid;

	if ((pid=fork())==0){
/*		VaciarListaProcesos(&LP); Depende de la implementación de cada uno*/
        deleteProcessList(processList);
		printf ("ejecutando proceso %d\n", getpid());
	}
	else if (pid!=-1)
		waitpid (pid,NULL,0);
}

void showenv (char **env, char *env_name){
	int i;
	for(i = 0; env[i] != NULL; i++){
		printf ("%p->%s[%d]=(%p) %s \n", &env[i], env_name, i, env[i], env[i]);
	}
}

void cmd_showvar(int substring_size, char *substring[substring_size],char *arg3[]){
	int pos;
	if(substring[1] == NULL){
		showenv(arg3,"arg3");
	} else if((pos = BuscarVariable(substring[1],environ)) != -1){
        if(arg3[pos] != NULL){
            printf("Con arg3 main %s(%p) @%p\n",arg3[pos],arg3, &arg3);
        }
        if(environ[pos] != NULL){
            printf("  Con environ %s(%p) @%p\n",environ[pos],environ, environ);
        }
        if(getenv(substring[1]) != NULL){
            printf("   Con getenv %s(%p)\n",getenv(substring[1]), getenv(substring[1]));
        }
	} else {
		printf("Imposible encontrar variable %s: %s\n",substring[1], strerror(errno));
	}
}

void cmd_changevar(int substring_size, char *substring[substring_size], char *arg3[]){
    char *aux;
    if(substring[1] != NULL && substring[2] != NULL && substring[3] != NULL){
		if(strcmp(substring[1],"-a") == 0){
			CambiarVariable(substring[2], substring[3], arg3);
		} else if(strcmp(substring[1],"-e") == 0){
			CambiarVariable(substring[2], substring[3], environ);
		} else if(strcmp(substring[1],"-p") == 0){
            aux = malloc(MAXVAR);
			strcpy(aux,substring[2]);strcat(aux,"=");strcat(aux,substring[3]);
			if (putenv(aux) != 0){
				printf("putenv error: %s\n",strerror(errno));
			}
		} else {
			printf("Uso: cambiarvar [-a|-e|-p] var valor\n");
		}
	} else {
		printf("Uso: cambiarvar [-a|-e|-p] var valor\n");
	}
}

void cmd_showenv(int substring_size, char *substring[substring_size],char *arg3[]){
	if(substring[1] == NULL){
		showenv(arg3,"main arg3");
	} else if(strcmp(substring[1], "-environ") == 0){
		showenv(environ,"environ");
	} else if(strcmp(substring[1], "-addr") == 0){
		printf("environ:   %p (almacenado en %p)\n",&environ[0],&environ);
		printf("main arg3: %p (almacenado en %p)\n",&environ[0],&arg3);
	}
}



void priority(int substring_size, char *substring[substring_size]){
	pid_t pid;
	int priority;

	if(substring[1] == NULL){
		pid = getpid();
		printf("Prioridad del proceso %d es %d\n",pid, getpriority(PRIO_PROCESS, pid));
	} else if(substring[2] == NULL){
		if((pid = atoi(substring[1])) != 0 && (priority = getpriority(PRIO_PROCESS, pid)) != -1){
			printf("Prioridad del proceso %d es %d\n",pid, priority);
		} else {
			printf("Imposible obtener la prioridad del proceso %d: %s\n",pid,strerror(errno));
		}
	} else {
		if((pid = atoi(substring[1])) == 0 || (priority = atoi(substring[2])) == 0 || (priority = setpriority(PRIO_PROCESS, pid, priority)) == -1){
			printf("Imposible cambiar la prioridad del proceso %d: %s\n",pid,strerror(errno));
		}
	}
}



void executeCmd(int substring_size, char *substring[substring_size]){
    char* argv[substring_size];
    char* env[MAXVAR];
    char aux[substring_size];
    bool more_env = true;
    bool more_arg = true;
    int i,j,z,pos;
    int priority = 0;

    memcpy(env,environ,MAXVAR);

    for(i = 1,j = 0,z = 0; substring[i] != NULL; i++){
        if(substring[i][0] == '@'){
            memmove(aux, substring[i]+1, strlen(substring[i]));
            priority = atoi(aux);
            more_arg = false;
            more_env = false;
        } else if(more_env && (pos = BuscarVariable(substring[i],environ)) != -1){
            env[j] = environ[pos];
            j++;
        } else if(more_arg){
            argv[z] = substring[i];
            z++;
            more_env = false;
        }
    }
    if(j != 0){
        env[j] = NULL;
    }
    argv[z] = NULL;
    if(priority != 0 && setpriority(PRIO_PROCESS, getpid(), priority) == -1){
		printf("Imposible cambiar prioridad: %s\n",strerror(errno));
        return ;
    }
    if(OurExecvpe(substring[j+1], argv, env) == -1){
        printf("Execve error: %s\n",strerror(errno));
        exit(-1);
    }
}


/*
* WNOHANG: return immediately if no child has exited
* WUNTRACED: also return if a child has stopped
* WCONTINUED: also return if a stopped child has been resumed by delivery of SIGCONT
*/
int updateProcessStatus(struct tProcessDataL* p){
	int status;
	if(waitpid(p->pid,&status, WNOHANG | WUNTRACED | WCONTINUED) == p->pid){ //Si no han cambiado de estado devuelve 0
		if(WIFEXITED(status)){
			p->status = FINISHED;
			p->signal = WEXITSTATUS(status);
            p->priority = -1;
		} else if(WIFSIGNALED(status)){
			p->status = SIGNALED;
			p->signal = WTERMSIG(status);
            p->priority = -1;
		} else if(WIFSTOPPED(status)){
			p->status = STOPPED;
			p->signal = WSTOPSIG(status);
		} else if(WIFCONTINUED(status)){
			p->status = ACTIVE;
			p->signal = 0; //Si está activo no hay señal
		}
		return 0; //Hay cambios
	}
    if(p->priority != getpriority(PRIO_PROCESS, p->pid)){
        p->priority = getpriority(PRIO_PROCESS, p->pid);
        return 0;
    }
	return 1; //No hay cambios
}

void printjob(tProcessList* processList,tProcessPosL p, struct passwd *aux){
    tProcessDataL q;
    q = getProcess(p,*processList);
    if(updateProcessStatus(&q) == 0){
        updateProcess(q,p,processList);
    }
    printf("  %d        %s p=%d %d/%d/%d %02d:%02d:%02d %s", q.pid, aux->pw_name,
    q.priority, 1900+q.time.tm_year, q.time.tm_mon+1, q.time.tm_mday,
    q.time.tm_hour,  q.time.tm_min, q.time.tm_sec, STATUS[q.status]);

    if(q.status == SIGNALED){
        printf(" (%s) ",nombreSenal(q.signal));
    } else {
        printf(" (%03d) ",q.signal);
    }
    printf("%s\n",q.commandLine);
}

void printjobs(tProcessList* processList){
    tProcessPosL p;
    struct passwd *aux;
    if ((aux = getpwuid(getuid())) == NULL){
        perror("getpwuid() error");
    }
    for(p=firstProcess(*processList);p != NULL; p = nextProcess(p,*processList)){
        printjob(processList,p,aux);
    }
}

void listjobs(int substring_size, char *substring[substring_size], tProcessList* processList){
	printjobs(processList);
}

void deljobs(int substring_size, char *substring[substring_size], tProcessList* processList){
	if(substring[1] != NULL){
        if(strcmp(substring[1],"-term") == 0){
        deleteProcStatus(processList,FINISHED);
        } else if(strcmp(substring[1],"-sig") == 0){
        deleteProcStatus(processList,SIGNALED);
        }
    }
    printjobs(processList);
}

void job(int substring_size, char *substring[substring_size], tProcessList* processList){
    struct passwd *aux;
    pid_t pid;
    tProcessPosL p;
    int status;

    if(substring[1] != NULL){
        if(strcmp(substring[1],"-fg") == 0 && substring[2] != NULL && (pid = atoi(substring[2])) != 0 ){
            if((p =findProcess(pid,*processList)) == NULL){
                printf("Proceso %d pid ya esta finalizado\n",pid);
            } else if(waitpid(pid,&status,0)!=-1){
                printf("Proceso %d terminado normalmente. Valor devuelto %d\n",pid, status);
                deleteProcess(p,processList);
            } else {
                printf("waitpid error: %s\n",strerror(errno));
            }
        } else {
        	if ((aux = getpwuid(getuid())) == NULL){
            	perror("getpwuid() error");
        	}
            if((pid = atoi(substring[1])) != 0 && (p = findProcess(pid,*processList)) != NULL){
                printjob(processList, p, aux);
            } else {
                printjobs(processList);
            }
        }
    }
}




void execute(int substring_size, char *substring[substring_size], tProcessList* processList){
    char* argv[substring_size];
	char* env[MAXVAR];
	char aux[substring_size];
	char commandLine[COMMAND_LENGTH_LIMIT] = "";
    pid_t child_pid;
    bool background = false;
    bool more_env = true;
	bool more_arg = true;
    int i,j,z,pos;
	int priority = 0;
	tProcessDataL q;
	time_t rawtime;

    memcpy(env,environ,MAXVAR);

    for(i = 0,j = 0,z = 0; substring[i] != NULL; i++){
		if(substring[i][0] == '@'){
			memmove(aux, substring[i]+1, strlen(substring[i]));
			priority = atoi(aux);
			more_arg = false;
			more_env = false;
		} else if(strcmp(substring[i],"&") == 0){
            background = true;
            break;
        } else if(more_env && (pos = BuscarVariable(substring[i],environ)) != -1){
            env[j] = environ[pos];
            j++;
        } else if(more_arg){
			argv[z] = substring[i];
			z++;
			more_env = false;
		}
		strcat(commandLine,substring[i]);
		strcat(commandLine," ");
    }
	if(j != 0){
		env[j] = NULL;
	}
    argv[z] = NULL;

    child_pid = fork();
	if(priority !=0 && setpriority(PRIO_PROCESS, child_pid, priority) == -1){
		printf("Imposible cambiar prioridad: %s\n",strerror(errno));
	} else if(child_pid == 0){
		if(OurExecvpe(substring[j], argv, env) == -1){
			printf("Execve error: %s\n",strerror(errno));
			exit(-1);
		}
    } else if(child_pid != -1) {
		if(!background){
			waitpid(child_pid,NULL,0);
		} else {
			time (&rawtime);
			q.pid = child_pid;
			q.time = *localtime(&rawtime);
			strcpy(q.commandLine,commandLine);
			q.status = ACTIVE;
			q.signal = 0;
            q.priority = priority;
			insertProcess(q,NULL,processList);
		}
	}
}
