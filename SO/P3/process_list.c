#include "process_list.h"

bool static createNode(tProcessPosL* p){
    *p = malloc(sizeof(struct ProcessNodeL));
    return *p!=LNULL;
}


void createEmptyProcessList(tProcessList *L){
    *L = LNULL;
}


bool insertProcess(tProcessDataL d, tProcessPosL p, tProcessList *L){
    tProcessPosL q;
    if(!createNode(&q)){
        return false;
    }else{
        q->data = d;
        q->next = LNULL;

        if(isEmptyProcessList(*L)){
            *L = q;
        }else if(p == LNULL){
            lastProcess(*L)->next = q;
        }else if(p == *L){
            q -> next = p;
            *L = q;
        }else {
            q -> next = p -> next;
            p -> next = q;
            q->data = p->data;
            p->data = d;
        }
        return true;
    }
}

void deleteProcess(tProcessPosL p, tProcessList *L){
    tProcessPosL q;
    if(p == *L){
        *L = p->next;
    } else if(p->next == LNULL){
        for(q=*L;q->next != p; q=q->next);
        q->next = LNULL;
    } else {
        q = p->next;
        p->data = q->data;
        p->next = q->next;
        p=q;
    }
    free(p);
}

void updateProcess(tProcessDataL d, tProcessPosL p, tProcessList *L){
    p->data = d;
}

bool isEmptyProcessList(tProcessList L){
    return L == LNULL;
}


tProcessPosL firstProcess(tProcessList L){
    return L;
}

tProcessPosL lastProcess(tProcessList L){
    tProcessPosL p;
    for(p = L; p -> next != LNULL; p = p -> next);
    return p;
}

tProcessPosL nextProcess(tProcessPosL p, tProcessList L){
    return p->next;
}


tProcessDataL getProcess(tProcessPosL p, tProcessList L){
    return (p -> data);
}

tProcessPosL findProcess(pid_t pid, tProcessList L){
    tProcessPosL p;
    if(isEmptyProcessList(L)) p = LNULL;
    else{
        for(p = L;p != LNULL && ( pid != p->data.pid);p = p->next);
    }
    return p;
}

void deleteProcessList(tProcessList* L){
    tProcessPosL p;
    while(*L != LNULL){
        p = *L;
        *L = (*L) -> next;
        free(p);
    }
}

void deleteProcStatus(tProcessList* L, tProcessStatus status){
    tProcessPosL p;
    bool delete = true;
    while(delete){
        for(p = *L;p != LNULL ;p = p->next){
            if(p->data.status == status){
                deleteProcess(p,L);
                break;
            }
        }
        if(p == LNULL){
            delete = false;
        }
    }
}
