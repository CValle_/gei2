#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

int main() {
    
    char BUF [6]= {'x', 'x', 'x', 'x', 'x'};
    int fd = open ("file.txt", O_RDONLY);
    int fd2 = dup (fd);
    lseek (fd2 , 3, SEEK_SET) ;
    printf ("%s\n", BUF);
    pread (fd, BUF+1, 2, 0);
    lseek (fd, 1, SEEK_CUR);
    printf ("%s\n", BUF);
    pread (fd, BUF, 2, 3);
    printf ("%s\n", BUF);
    read (fd, BUF+2, 1);
    printf ("%s\n", BUF);
    read (fd, BUF+3, 2);
    printf ("%s\n", BUF);
    // read (fd, BUF, 1);
    // printf ("%s", BUF);
    close (fd);
}