/*
 * TITLE: p0
 * AUTHOR 1: Raúl Mouzo Quiza LOGIN 1: raul.mouzo@udc.es
 * AUTHOR 2: Clara Valle Gómez LOGIN 2: clara.valle@udc.es
 * GROUP: 2.2
 * DATE: 23/09/2022
 */

#include "dynamic_list.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h> //librería para procesos del sistema
#include <errno.h> //libreria para errores
#include <time.h> //librería fecha y hora
#include <sys/utsname.h> //librería infosys

int stringSplit (char *string, char *substring[]){
    int i=1;
    if((substring[0]=strtok(string, " \n\t")) == NULL){
        return 0;
    }
    while((substring[i]=strtok(NULL, " \n\t")) != NULL){
        i++;
    }
    return i;
}

void get_input(char *str,int size){
    fgets(str,size,stdin);
    strtok(str, "\n");
}

void authors(int substring_size,char *substring[substring_size]) {
    if (substring[1] == NULL) {
        printf("Clara Valle Gómez: clara.valle@udc.es\n"
               "Raúl Mouzo Quiza: raul.mouzo@udc.es\n");
    } else if(strcmp(substring[1], "-l") == 0){
        printf("clara.valle@udc.es\n"
               "raul.mouzo@udc.es\n");
    } else if(strcmp(substring[1], "-n") == 0) {
        printf("Clara Valle Gómez\n"
               "Raúl Mouzo Quiza\n");
    }
}

void pid(int substring_size,char *substring[substring_size]) {
    if (substring[1] != NULL && strcmp(substring[1], "-p") == 0 ) {
        printf("Pid del padre del shell:%d\n", getppid());
    } else {
        printf("Pid de la shell %d\n", getpid());
    }
}

void directory(int substring_size,char *substring[substring_size]){
    const int MAX_BUF = 256;
    char path[MAX_BUF];

    if(substring[1] == NULL){
        if(getcwd(path, MAX_BUF)!=NULL){
            printf("%s\n",path);
        } else {
            printf("Error: %s\n", strerror(errno));
        }
    } else {
        if(chdir(substring[1]) == -1){
            printf("Imposible cambiar directorio: %s\n", strerror(errno));
        }
    }
}
void datetime(int substring_size,char *substring[substring_size]){
    time_t t = time(NULL); // Se crea un struct de tipo tm con la fecha y la hora
    struct tm tm = *localtime(&t);

    int h, min, sec, day, month, year;
    h = tm.tm_hour;
    min = tm.tm_min;
    sec = tm.tm_sec;
    day = tm.tm_mday;
    month = tm.tm_mon + 1;
    year = tm.tm_year + 1900;

    if (substring[1] == NULL) {
        printf("%02d:%02d:%02d\n%d/%02d/%02d\n", h, min, sec, day, month, year);
    } else if(substring[1][2] == '\0' || substring[1][2] == ' '){
        if(strcmp(substring[1], "-d") == 0){
            printf("%d/%02d/%02d\n", day, month, year);
        }
        if(strcmp(substring[1], "-h") == 0) {
            printf("%02d:%02d:%02d\n", h, min, sec);
        }
    }
}
void infosys(){
    struct utsname buffer;
    uname(&buffer);

    printf("%s (%s), OS: %s-%s-%s\n", buffer.nodename, buffer.machine, buffer.sysname, buffer.release, buffer.version);
}

void help(int substring_size, char *substring[substring_size]){
    if (substring[1] == NULL) {
        printf("\'ayuda cmd\' donde cmd es uno de los siguientes comandos: \nfin salir bye fecha pid autores hist comando carpeta infosis ayuda\n");
    } else if (strcmp(substring[1], "fin") == 0 || strcmp(substring[1], "salir") == 0 || strcmp(substring[1], "bye") == 0) {
        printf("%s\tTermina la ejecucion del shell\n", substring[1]);
    } else if (strcmp(substring[1], "fecha") == 0) {
        printf("%s [-d|.h\tMuestra la fecha y o la hora actual\n", substring[1]);
    } else if (strcmp(substring[1], "pid") == 0) {
        printf("%s [-p]\tMuestra el pid del shell o de su proceso padre\n", substring[1]);
    } else if (strcmp(substring[1], "autores") == 0) {
        printf("%s [-n|-l]\tMuestra los nombres y logins de los autores\n", substring[1]);
    } else if (strcmp(substring[1], "hist") == 0) {
        printf("%s [-c|-N]\tMuestra el historico de comandos, con -c lo borra\n", substring[1]);
    } else if (strcmp(substring[1], "comando") == 0) {
        printf("%s [-N]\tRepite el comando N (del historico)\n", substring[1]);
    } else if (strcmp(substring[1], "carpeta") == 0) {
        printf("%s [dir]\tCambia (o muestra) el directorio actual del shell\n", substring[1]);
    } else if (strcmp(substring[1], "infosis") == 0) {
        printf("%s \tMuestra informacion de la maquina donde corre el shell\n", substring[1]);
    } else {
        printf("%s no encontrado\n", substring[1]);
    }
}

void hist(tList* list, int substring_size, char *substring[substring_size], int* commandCounter){
    tPosL itemPos = first(*list);
    int i = 0;
    int numParam;
    if (substring[1] == NULL) {
        do {
            char* item = getItem(itemPos, *list);
            printf("%d -> %s\n", i, item);
            itemPos = next(itemPos, *list);
            i++;
        } while (itemPos != LNULL);

    } else if (strcmp(substring[1], "-c") == 0) {
        deleteList(list);
        *commandCounter = 0;

    } else if (substring[1][0] == '-' ) {
        memmove(substring[1],substring[1]+1, strlen(substring[1]));

        numParam = atoi(substring[1]);
        if (numParam != 0) { // Comprueba que sea un int
            do {
                char* item = getItem(itemPos, *list);
                printf("%d -> %s\n", i, item);
                itemPos = next(itemPos, *list);
                i++;
            } while (itemPos != LNULL && i < numParam);
        }
    }
}

char* commandN(tList* list, int substring_size, char *substring[substring_size]){
    tPosL itemPos = first(*list);
    int i = 0;

    while (itemPos != LNULL && i < atoi(substring[1])){
        itemPos = next(itemPos, *list);
        i++;
    }

    return getItem(itemPos, *list);
}

void whichCommand(bool* exit, tList* list, char* inputAux, char* substring[], int SUBSTRING_SIZE, int* commandCounter, int* recursiveCounter) {
    char inputAux2[256];

    if (strcmp(substring[0], "comando") == 0 ) {
        if (substring[1] != NULL && atoi(substring[1]) >= 0 && atoi(substring[1]) < *commandCounter) {

            inputAux = commandN(list, SUBSTRING_SIZE, substring);

            printf("Ejecutando hist (%s): %s\n", substring[1], inputAux);

            strcpy(inputAux2, inputAux);
            stringSplit(inputAux2, substring);

            if (*recursiveCounter < 10){
                *recursiveCounter = *recursiveCounter + 1;
                whichCommand(exit, list, inputAux, substring, SUBSTRING_SIZE, commandCounter, recursiveCounter);
            } else {
                printf("Demasiada recursion en hist\n");
                *recursiveCounter = 0;
            }

        } else if(substring[1]!=NULL){
            printf("No hay elemento %s en el historico\n", substring[1]);
        }
    } else if (strcmp(substring[0], "fin") == 0 || strcmp(substring[0], "salir") == 0 || strcmp(substring[0], "bye") == 0) {
        *exit = true;
    } else if (strcmp(substring[0], "autores") == 0) {
        authors(SUBSTRING_SIZE, substring);
    } else if (strcmp(substring[0], "pid") == 0) {
        pid(SUBSTRING_SIZE, substring);
    } else if (strcmp(substring[0], "carpeta") == 0) {
        directory(SUBSTRING_SIZE, substring);
    } else if (strcmp(substring[0], "fecha") == 0) {
        datetime(SUBSTRING_SIZE, substring);
    } else if (strcmp(substring[0], "infosis") == 0) {
        infosys();
    } else if (strcmp(substring[0], "ayuda") == 0){
        help(SUBSTRING_SIZE, substring);
    } else if (strcmp(substring[0], "hist") == 0) {
        hist(list, SUBSTRING_SIZE, substring, commandCounter);
    } else {
        printf("%s: no es un comando del shell\n", substring[0]);
    }

}

int main(){
    const int STRING_SIZE = 256;
    const int SUBSTRING_SIZE = 3;

    char input[STRING_SIZE];
    char *substring[SUBSTRING_SIZE];
    char inputAux[STRING_SIZE];
    bool exit=false;
    int commandCounter = 0;
    int recursiveCounter = 0;

    //Definición e inicialización de la lista del histórico
    tList list;
    createEmptyList(&list);


    while(!exit) {
        printf("> ");
        get_input(input, STRING_SIZE);
        strcpy(inputAux, input);
        stringSplit(input, substring);

        if (substring[0] != NULL) {
            insertItem(inputAux, LNULL, &list);
            commandCounter++;
            
            whichCommand(&exit, &list, inputAux, substring, SUBSTRING_SIZE, &commandCounter, &recursiveCounter);
        }
    }

    deleteList(&list);
    return 0;
}

