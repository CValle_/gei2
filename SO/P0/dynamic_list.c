#include "dynamic_list.h"
#include <stdio.h>

void createEmptyList (tList* list) {
    *list = LNULL;
}

tPosL next (tPosL p, tList L){
    if (last(L) == p){
        return LNULL;
    } else {
        return p -> next;
    }
}

bool createNode(tPosL *p){
    *p=malloc(sizeof(struct tNodo));
    return *p!=LNULL;
}

bool insertItem(char* data, tPosL p, tList* L){
    tPosL q, r;
    if(!createNode(&q)){
        return false;
    }else{
        q->data = malloc(strlen(data)+1);
        strcpy(q->data, data);
        q -> next = LNULL;

        if(isEmptyList(*L)){
            *L = q;
        }else if(p == LNULL){
            for(r = *L; r -> next != LNULL; r = r -> next);
            r -> next = q;
        }else if(p == *L){
            q -> next = p;
            *L = q;
        }else {
            q -> next = p -> next;
            p -> next = q;
            q -> data = p -> data;
            p -> data = data;
        }
        return true;
    }
}

tPosL last(tList L) {
    tPosL p;
    for(p=L;p->next!=LNULL;p = p->next);
    return p;
}

tPosL previous(tPosL p, tList L){
    tPosL q;
    if(p==L){
        return LNULL;
    }else {
        for (q = L; q->next != p; q = q->next);
        return q;
    }
}

bool isEmptyList(tList L){
    return L==LNULL;
}

void deleteList(tList* L){
    tPosL p;
    while(*L != LNULL){
        p=*L;
        *L = (*L)->next;
        free(p);
    }
}

char* getItem(tPosL p, tList L){
    return (p->data);
}

tPosL first(tList L){
    return (L);
}
