#define DYNAMIC_LIST_H

#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#define LNULL NULL

typedef struct tNodo *tPosL;

struct tNodo{
    char* data;
    tPosL next;
};

typedef tPosL tList;

void createEmptyList (tList*);

tPosL next(tPosL, tList);

bool insertItem(char*, tPosL, tList*);

tPosL last(tList);

tPosL previous(tPosL, tList);

bool isEmptyList(tList);

void deleteList(tList*);

char* getItem(tPosL p, tList L);

tPosL first(tList);


