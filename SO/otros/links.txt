C library function - gmtime()  -->  https://www.tutorialspoint.com/c_standard_library/c_function_gmtime.htm

getpwuid  -->  https://pubs.opengroup.org/onlinepubs/009604499/functions/getpwuid.html

stat()  -->  https://www.ibm.com/docs/en/i/7.3?topic=ssw_ibm_i_73/apis/stat.htm

readlink(2) --> https://man7.org/linux/man-pages/man2/readlink.2.html
