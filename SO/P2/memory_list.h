#include <stdbool.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>


#define NAME_LENGTH_LIMIT 256
#define LNULL NULL


typedef void* tAddress;

typedef size_t tSize;

typedef char tName[NAME_LENGTH_LIMIT];

typedef enum {malloc_memory, shared_memory, mapped_file} tAllocationType;

typedef int tDescriptor;

typedef struct NodeL* tMemPosL;

typedef struct tMemItemL {
    tAddress address;
    tSize size;
    struct tm time;
    tName name;
    tAllocationType allocationType;
    key_t key;
    tDescriptor descriptor;

} tMemItemL;

struct NodeL {
    tMemItemL data;
    tMemPosL next;
};

typedef tMemPosL tMemList;



void createEmptyMemoryList(tMemList *L);

bool insertMemory(tMemItemL d, tMemPosL p, tMemList *L);

void deleteMemory(tMemPosL p, tMemList *L);

bool isEmptyMemoryList(tMemList L);

tMemPosL firstMemory(tMemList L);

tMemPosL lastMemory(tMemList L);

tMemPosL nextMemory(tMemPosL p, tMemList L);

tMemPosL previousMemory(tMemPosL p, tMemList L);

tMemItemL getMemory(tMemPosL p, tMemList L);

tMemPosL findMemory(tMemItemL d, tMemList L);
