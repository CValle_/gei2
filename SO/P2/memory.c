#include "memory.h"

#define TAMANO 2048
#define MAX_ARGS 32

int glob1, glob2, glob3;


void enum_to_string(tAllocationType type, char* string){
    switch(type){
        case shared_memory:
            strcpy(string," shared");
            break;
        case malloc_memory:
            strcpy(string," malloc");
            break;
        case mapped_file:
            strcpy(string," mmap");
            break;
        default:
            strcpy(string,"");
    }
}


void* cadtop(char* str){
    unsigned long long ul;

    sscanf(str, "%llx", &ul);
    return (void*) (uintptr_t) ul;
}

void imprimirListaType(tMemList memList,tAllocationType enumType){
    tMemPosL p = memList;
    tMemItemL q;
    char month[10];
    char stringType[10];
    enum_to_string(enumType, stringType);

    printf("******Lista de bloques asignados%s para el proceso %d\n", stringType, getpid());
    while(p != NULL){
        q = getMemory(p,memList);
        enum_to_string(q.allocationType, stringType);

        if(enumType == -1 || q.allocationType == enumType){
            strftime(month,sizeof(month),"%b",&q.time);
            printf("      %p%15ld %s %02d %02d:%02d %s", q.address, q.size, month, q.time.tm_mday,q.time.tm_hour, q.time.tm_min, q.allocationType!=mapped_file? stringType:"");

            if(q.allocationType == shared_memory){
                printf(" (key %d)", q.key);
            } else if(q.allocationType == mapped_file){
                printf(" %s (descriptor %d)",q.name, q.descriptor);
            }
            printf("\n");
        }
        p = nextMemory(p, memList);
    }
}

ssize_t LeerFichero (char *f, void *p, size_t cont)
{
   struct stat s;
   ssize_t  n;
   int df,aux;

   if (stat (f,&s)==-1 || (df=open(f,O_RDONLY))==-1)
	return -1;
   if (cont==-1)   /* si pasamos -1 como bytes a leer lo leemos entero*/
	cont=s.st_size;
   if ((n=read(df,p,cont))==-1){
	aux=errno;
	close(df);
	errno=aux;
	return -1;
   }
   close (df);
   return n;
}

void do_I_O_read (char *ar[]){
   void *p;
   size_t cont=-1;
   ssize_t n;
   if (ar[0]==NULL || ar[1]==NULL){
	printf ("faltan parametros\n");
	return;
   }
   p=cadtop(ar[1]);  /*convertimos de cadena a puntero*/
   if (ar[2]!=NULL)
	cont=(size_t) atoll(ar[2]);
   if ((n=LeerFichero(ar[0],p,cont))==-1)
	perror ("Imposible leer fichero");
   else
	printf ("leidos %lld bytes de %s en %p\n",(long long) n,ar[0],p);
}

ssize_t EscribirFichero (char *f, void *p, size_t cont,int overwrite)
{
   ssize_t  n;
   int df,aux, flags=O_CREAT | O_EXCL | O_WRONLY;

   if (overwrite)
	flags=O_CREAT | O_WRONLY | O_TRUNC;

   if ((df=open(f,flags,0777))==-1)
	return -1;

   if ((n=write(df,p,cont))==-1){
	aux=errno;
	close(df);
	errno=aux;
	return -1;
   }
   close (df);
   return n;
}


void do_I_O_write (char *ar[]){
   void *p;
   size_t cont=-1;
   ssize_t n;
   int overwrite = 0;

   if (ar[1]==NULL || ar[2]==NULL || ar[3] == NULL){
	printf ("faltan parametros\n");
	return;
   }
   if(ar[0] != NULL){
       overwrite = 1;
   }
   p=cadtop(ar[2]);  /*convertimos de cadena a puntero*/
   cont=(size_t) atoll(ar[3]);
   if ((n=EscribirFichero(ar[1], p, cont, overwrite))==-1)
	printf("Imposible escribir fichero: %s\n",strerror(errno));
   else
	printf ("escritos %lld bytes en %s desde %p\n",(long long) n,ar[1],p);
}


void Do_pmap (void) /*sin argumentos*/
 { pid_t pid;       /*hace el pmap (o equivalente) del proceso actual*/
   char elpid[32];
   char *argv[4]={"pmap",elpid,NULL};

   sprintf (elpid,"%d", (int) getpid());
   if ((pid=fork())==-1){
      perror ("Imposible crear proceso");
      return;
      }
   if (pid==0){
      if (execvp(argv[0],argv)==-1)
         perror("cannot execute pmap (linux, solaris)");

      argv[0]="procstat"; argv[1]="vm"; argv[2]=elpid; argv[3]=NULL;
      if (execvp(argv[0],argv)==-1)/*No hay pmap, probamos procstat FreeBSD */
         perror("cannot execute procstat (FreeBSD)");

      argv[0]="procmap",argv[1]=elpid;argv[2]=NULL;
            if (execvp(argv[0],argv)==-1)  /*probamos procmap OpenBSD*/
         perror("cannot execute procmap (OpenBSD)");

      argv[0]="vmmap"; argv[1]="-interleave"; argv[2]=elpid;argv[3]=NULL;
      if (execvp(argv[0],argv)==-1) /*probamos vmmap Mac-OS*/
         perror("cannot execute vmmap (Mac-OS)");
      exit(1);
  }
  waitpid (pid,NULL,0);
}


void insertarNodoShared(tMemList* memList, void* p, size_t size, key_t key){
    tMemItemL q;
    time_t rawtime;
    time (&rawtime);
    q.allocationType = shared_memory;
    q.address = p;
    q.size = size;
    q.key = key;
    q.time = *localtime(&rawtime);
    insertMemory(q, LNULL, memList);
}

void insertarNodoMmap(tMemList* memList, void* p, size_t size, tDescriptor descriptor, tName filename){
    tMemItemL q;
    time_t rawtime;
    time (&rawtime);
    q.allocationType = mapped_file;
    q.address = p;
    q.size = size;
    q.descriptor = descriptor;
    q.time = *localtime(&rawtime);
    strcpy(q.name, filename);
    insertMemory(q, LNULL, memList);
}

void * obtenerMemoriaShmget (key_t clave, size_t tam, tMemList* memList){
    void * p;
    int aux,id,flags=0777;
    struct shmid_ds s;

    if (tam)     /*tam distito de 0 indica crear */
        flags = flags | IPC_CREAT | IPC_EXCL;
    if (clave == IPC_PRIVATE)  /*no nos vale*/
        {errno = EINVAL; return NULL;}
    if ((id = shmget(clave, tam, flags))==-1)
        return (NULL);
    if ((p = shmat(id,NULL,0))==(void*) -1){
        aux=errno;
        if (tam)
             shmctl(id,IPC_RMID,NULL);
        errno = aux;
        return (NULL);
    }
    shmctl (id,IPC_STAT,&s);
    insertarNodoShared (memList, p, s.shm_segsz, clave);

    return (p);
}

void * mapearFichero (char * fichero, int protection, tMemList* memList){
    int df, map = MAP_PRIVATE, modo = O_RDONLY;
    struct stat s;
    void *p;

    if (protection &PROT_WRITE)
        modo = O_RDWR;
    if (stat(fichero,&s) == -1 || (df = open(fichero, modo)) == -1)
        return NULL;
    if ((p = mmap (NULL,s.st_size, protection,map,df,0)) == MAP_FAILED)
        return NULL;
    insertarNodoMmap (memList, p, s.st_size, df, fichero);
    return p;
}


void do_AllocateMalloc (tSize size, tMemList* memList){
    tMemItemL q;
    tAddress address;
    time_t rawtime;

    if(size != 0){
        address = malloc(size);
        if(address == NULL){
            printf("Imposible obtener memoria con malloc: %s\n",strerror(errno));
        } else {
            time (&rawtime);
            q.address = address;
            q.size = size;
            q.allocationType = malloc_memory;
            q.time = *localtime (&rawtime);
            insertMemory(q, LNULL, memList);
            printf ("Asignados %lu bytes en %p\n",(unsigned long) size, address);
        }
    } else {
        printf("No se asignan bloques de 0 bytes\n");
    }
}


void do_AllocateCreateshared (char *tr[], tMemList* memList){
    key_t cl;
    size_t tam;
    void *p;

    if (tr[0]==NULL || tr[1]==NULL) {
        imprimirListaType(*memList,shared_memory);
        return;
    }

    cl = (key_t)  strtoul(tr[0],NULL,10);
    tam = (size_t) strtoul(tr[1],NULL,10);
    if (tam == 0) {
        printf ("No se asignan bloques de 0 bytes\n");
        return;
    }
    if ((p=obtenerMemoriaShmget(cl,tam, memList))!=NULL) {
        printf ("Asignados %lu bytes en %p\n",(unsigned long) tam, p);
    } else {
        printf ("Imposible asignar memoria compartida clave %lu:%s\n",(unsigned long) cl,strerror(errno));
    }
}


void do_AllocateMmap(char *arg[], tMemList* memList) {
    char *perm;
    void *p;
    int protection = 0;

    if (arg[0] == NULL){
        imprimirListaType(*memList,mapped_file);
        return;
    }
    if ((perm=arg[1])!=NULL && strlen(perm)<4) {
        if (strchr(perm,'r') != NULL) protection |= PROT_READ;
        if (strchr(perm,'w') != NULL) protection |= PROT_WRITE;
        if (strchr(perm,'x') != NULL) protection |= PROT_EXEC;
    }
    if ((p=mapearFichero(arg[0],protection, memList))==NULL) {
            perror ("Imposible mapear fichero");
    } else {
            printf ("fichero %s mapeado en %p\n", arg[0], p);
    }
}

void do_AllocateShared(char *tr[], tMemList* memList) {
    void * p;
    key_t cl;

    if (tr[0]==NULL) {
        imprimirListaType(*memList,shared_memory);
        return;
    }
    cl = (key_t)  strtoul(tr[0],NULL,10);
    if ((p = obtenerMemoriaShmget(cl, 0, memList)) != NULL) {
        printf ("Memoria compartida de clave %d en %p\n",cl, p);
    } else {
        printf ("Imposible asignar memoria compartida clave %lu:%s\n",(unsigned long) cl,strerror(errno));
    }
}

void do_DeallocateDelkey (char *args[]){
   key_t clave;
   int id;
   char *key=args[0];

   if (key==NULL || (clave=(key_t) strtoul(key,NULL,10))==IPC_PRIVATE){
        printf ("      delkey necesita clave_valida\n");
        return;
   }
   if ((id=shmget(clave,0,0666))==-1){
        perror ("shmget: imposible obtener memoria compartida");
        return;
   }
   if (shmctl(id,IPC_RMID,NULL)==-1)
        perror ("shmctl: imposible eliminar memoria compartida\n");
}

void do_DeallocateMalloc (char *args[], tMemList* memList){
    tMemPosL p;
    tMemItemL q;
    tSize size;
    if (args[0] == NULL) {
        imprimirListaType(*memList, malloc_memory);
    } else if((size = atoi(args[0])) == 0){
        printf("No se asignan bloques de 0 bytes\n");
    } else{
        for(p = firstMemory(*memList);p != NULL ; p = nextMemory(p,*memList)){
            q = getMemory(p,*memList);
            if (q.allocationType == malloc_memory &&q.size == size)
                break;
        }
        if(p == NULL){
            printf("No hay bloque de ese tamano asignado con malloc\n");
        } else {
            free(q.address);
            deleteMemory(p,memList);
        }
    }
}

void do_DeallocateShared (char *args[], tMemList* memList){
    tMemPosL p;
    tMemItemL q;
    key_t key;

    if (args[0] == NULL) {
        imprimirListaType(*memList, shared_memory);
    } else if((key = atoi(args[0])) == 0){
        printf("No hay bloque de esa clave mapeado en el proceso\n");
    } else{
        for(p = firstMemory(*memList);p != NULL ; p = nextMemory(p,*memList)){
            q = getMemory(p,*memList);
            if (q.allocationType == shared_memory && q.key == key)
                break;
        }
        if(p == NULL){
            printf("No hay bloque de esa clave mapeado en el proceso\n");
        } else {
            shmdt(q.address);
            deleteMemory(p,memList);
        }
    }
}



void do_DeallocateMmap (char *args[], tMemList* memList){
    tMemPosL p;
    tMemItemL q;
    if (args[0] == NULL) {
        imprimirListaType(*memList, mapped_file);
    }else{
        for(p = firstMemory(*memList);p != NULL ; p = nextMemory(p,*memList)){
            q = getMemory(p,*memList);
            if (q.allocationType == mapped_file && strcmp(q.name,args[0]) == 0)
                break;
        }
        if(p == NULL){
            printf("Fichero %s no mapeado\n",args[0]);
        } else {
            munmap(q.address,q.size);
            deleteMemory(p,memList);
        }
    }
}




void allocate(tMemList* memList, int substring_size, char *substring[substring_size]){
    tSize size = 0;
    char *tr[MAX_ARGS];

    if (substring[1] == NULL) {
        imprimirListaType(*memList, -1);
    } else if(strcmp(substring[1],"-malloc") == 0 ){
        if (substring[2] != NULL){
            size = atoi(substring[2]);
            do_AllocateMalloc(size, memList);
        } else {
            imprimirListaType(*memList, malloc_memory);
        }
    } else if(strcmp(substring[1],"-shared") == 0){
        tr[0]=substring[2];
        do_AllocateShared(tr,memList);
    } else if(strcmp(substring[1],"-createshared") == 0){
        tr[0]=substring[2];
        tr[1]=substring[3];
        do_AllocateCreateshared(tr, memList);
    } else if(strcmp(substring[1],"-mmap") == 0){
        tr[0]=substring[2];
        tr[1]=substring[3];
        do_AllocateMmap(tr, memList);
    } else {
        printf("uso: allocate [-malloc|-shared|-createshared|-mmap] ....\n");
    }
}

void deallocate(tMemList* memList, int substring_size, char *substring[substring_size]){
    tMemPosL p;
    tMemItemL q;
    char *args[MAX_ARGS];
    void *address;

    if (substring[1]==NULL) {
        imprimirListaType(*memList,-1);
    } else if(strcmp(substring[1],"-malloc") == 0 ){
        args[0]=substring[2];
        do_DeallocateMalloc(args, memList);
    } else if(strcmp(substring[1],"-shared") == 0){
        args[0]=substring[2];
        do_DeallocateShared(args, memList);
    } else if(strcmp(substring[1],"-delkey") == 0){
        args[0]=substring[2];
        do_DeallocateDelkey(args);
    } else if(strcmp(substring[1],"-mmap") == 0){
        args[0]=substring[2];
        do_DeallocateMmap(args, memList);
    } else {
        address = cadtop(substring[1]);
        for(p = firstMemory(*memList);p != NULL ; p = nextMemory(p,*memList)){
            q = getMemory(p,*memList);
            if (q.address == address)
                break;
        }
        if(p == NULL){
            printf("Direccion %p no asignada con malloc, shared o mmap\n",address);
        } else {
            switch(q.allocationType){
                case malloc_memory:
                    free(q.address);
                    break;
                case shared_memory:
                    shmdt(q.address);
                    break;
                case mapped_file:
                    munmap(q.address,q.size);
                    break;
            }
            deleteMemory(p,memList);
        }
    }
}

void i_o(int substring_size, char *substring[substring_size]){
    char *args[MAX_ARGS];
    if(strcmp(substring[1],"read") == 0){
        args[0] = substring[2];
        args[1] = substring[3];
        args[2] = substring[4];
        do_I_O_read(args);

    } else if(strcmp(substring[1],"write") == 0){
        if(strcmp(substring[2],"-o") == 0){
            args[0] = substring[2];
            args[1] = substring[3];
            args[2] = substring[4];
            args[3] = substring[5];
        } else {
            args[0] = NULL;
            args[1] = substring[2];
            args[2] = substring[3];
            args[3] = substring[4];
        }
        do_I_O_write(args);
    } else {
        printf("uso: e-s [read|write] ......\n");
    }
}


void memdump(int substring_size, char *substring[substring_size]){
    if(substring[1] != NULL){
        void* p = cadtop(substring[1]);
        size_t cont = 25;
        unsigned char *arr=(unsigned char *) p;
        size_t i = 0;
        int j;

        if(substring[2] != NULL)
            cont =(size_t) atoi(substring[2]);

        printf("Volcando %zu bytes desde la direccion %p\n", cont, p);

        while(i<cont){
            for(j= 0;j<25 && i<cont;j++,i++){
                if(arr[i] == '\n'){
                    printf("\\n");
                } else{
                    printf("%2c ",arr[i]);
                }
            }
            printf("\n");
            i=i-j;
            for (int k=0;k<j;k++,i++){
                printf("%02x ",arr[i]);
            }
            printf("\n");
        }
    }
}

void memfill (int substring_size, char *substring[substring_size]){
    if(substring[1] != NULL){
        void *p = cadtop(substring[1]);
        size_t cont = 128;
        unsigned char byte = 'A';
        unsigned char *arr=(unsigned char *) p;
        size_t i;
        if(substring[2] != NULL){
            cont =(size_t) atoi(substring[2]);
            if(substring[3] != NULL)
                byte = substring[3][1];
        }

        printf("Llenando %zu bytes de memoria con el byte %c(%2x) a partir de la direccion %p\n",cont, byte, byte, p);
        for (i=0; i<cont;i++){
    	   arr[i]=byte;
       }
   }
}


void memory(int substring_size, char *substring[substring_size],tMemList memList){
    int a,b,c;
    static int d, e, f;
    if(substring[1] == NULL || strcmp(substring[1],"-all") == 0){
        printf("Variables locales   %20p,%20p,%20p\n",&a,&b,&c);
        printf("Variables globales  %20p,%20p,%20p\n",&glob1, &glob2, &glob3);
        printf("Variables estaticas %20p,%20p,%20p\n",&d,&e,&f);
        printf("Funciones programa  %20p,%20p,%20p\n",&memdump, &memfill, &recurse);
        printf("Funciones libreria  %20p,%20p,%20p\n",&printf, &malloc, &time);
        imprimirListaType(memList,-1);
    } else if(strcmp(substring[1],"-blocks") == 0){
        imprimirListaType(memList,-1);
    } else if(strcmp(substring[1],"-funcs") == 0){
        printf("Funciones programa  %20p,%20p,%20p\n",&memdump, &memfill, &recurse);
        printf("Funciones libreria  %20p,%20p,%20p\n",&printf, &malloc, &time);
    } else if(strcmp(substring[1],"-vars") == 0){
        printf("Variables locales   %20p,%20p,%20p\n",&a,&b,&c);
        printf("Variables globales  %20p,%20p,%20p\n",&glob1, &glob2, &glob3);
    } else if(strcmp(substring[1],"-pmap") == 0){
        Do_pmap();
    } else {
        printf("Opcion %s no contemplada\n",substring[1]);
    }
}

void recurse (int n){
    char automatico[TAMANO];
    static char estatico[TAMANO];

    printf ("parametro:%3d(%p) array %p, arr estatico %p\n",n,&n,automatico, estatico);

    if (n>0)
    recurse(n-1);
}
