#include "memory_list.h"


void createEmptyMemoryList(tMemList *L){
    *L = LNULL;
}

bool static createNode(tMemPosL* p){
    *p = malloc(sizeof(struct NodeL));
    return *p!=LNULL;
}


bool insertMemory(tMemItemL d, tMemPosL p, tMemList* L){
    tMemPosL q;
    if(!createNode(&q)){
        return false;
    }else{
        q->data = d;
        q->next = LNULL;

        if(isEmptyMemoryList(*L)){
            *L = q;
        }else if(p == LNULL){
            lastMemory(*L)->next = q;
        }else if(p == *L){
            q -> next = p;
            *L = q;
        }else {
            q -> next = p -> next;
            p -> next = q;
            q->data = p->data;
            p->data = d;
        }
        return true;
    }
}


void deleteMemory(tMemPosL p, tMemList *L){
    tMemPosL q;
    if(p == *L){
        *L = p->next;
    } else if(p->next == LNULL){
        for(q=*L;q->next != p; q=q->next);
        q->next = LNULL;
    } else {
        q = p->next;
        p->data = q->data;
        p->next = q->next;
        p=q;
    }
    free(p);
}

bool isEmptyMemoryList(tMemList L){
    return L == LNULL;
}

tMemPosL firstMemory(tMemList L){
    return L;
}

tMemPosL lastMemory(tMemList L) {
    tMemPosL p;
    for(p = L; p -> next != LNULL; p = p -> next);
    return p;
}

tMemPosL nextMemory(tMemPosL p, tMemList L){
    return p->next;
}

//tMemPosL previous(tMemPosL p,tMemList L);

tMemItemL getMemory(tMemPosL p, tMemList L){
    return (p -> data);
}

tMemPosL findMemory(tMemItemL d, tMemList L){
    tMemPosL p;
    if(isEmptyMemoryList(L)) p = LNULL;
    else{
        for(p = L;p != LNULL && (d.address != L->data.address || d.size != L->data.size);p = p->next);
    }
    return p;
}
