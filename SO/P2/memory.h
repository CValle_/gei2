#include "memory_list.h"
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <sys/wait.h>




void allocate(tMemList* memList, int substring_size, char *substring[substring_size]);
void deallocate(tMemList* memList, int substring_size, char *substring[substring_size]);
void i_o(int substring_size, char *substring[substring_size]);
void memdump(int substring_size, char *substring[substring_size]);
void memfill(int substring_size, char *substring[substring_size]);
void memory(int substring_size, char *substring[substring_size], tMemList memList);
void recurse(int n);
