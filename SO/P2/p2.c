/*
 * TITLE: p2
 * AUTHOR 1: Raúl Mouzo Quiza LOGIN 1: raul.mouzo@udc.es
 * AUTHOR 2: Clara Valle Gómez LOGIN 2: clara.valle@udc.es
 * GROUP: 2.2
 * DATE: 18/11/2022
 */

#include "dynamic_list.h"
#include "memory.h"

#include <stdio.h>
#include <unistd.h> //librería para procesos del sistema
#include <errno.h> //libreria para errores
#include <time.h> //librería fecha y hora
#include <sys/utsname.h> //librería infosys
#include <sys/stat.h>
#include <fcntl.h> //librería creación de ficheros
#include <pwd.h> //librería para obtener nombre usuario con uid
#include <grp.h> //librería para obtener nombre grupo con gid
#include <dirent.h> //libreriía opendir,readdir...

char LetraTF (mode_t m){
    switch (m&S_IFMT) { /*and bit a bit con los bits de formato,0170000 */
        case S_IFSOCK: return 's'; /*socket */
        case S_IFLNK: return 'l'; /*symbolic link*/
        case S_IFREG: return '-'; /* fichero normal*/
        case S_IFBLK: return 'b'; /*block device*/
        case S_IFDIR: return 'd'; /*directorio */
        case S_IFCHR: return 'c'; /*char device*/
        case S_IFIFO: return 'p'; /*pipe*/
        default: return '?'; /*desconocido, no deberia aparecer*/
    }
}

char * convierteModo (mode_t m){
    static char permisos[12];
    strcpy (permisos,"---------- ");

    permisos[0]=LetraTF(m);
    if (m&S_IRUSR) permisos[1]='r';    /*propietario*/
    if (m&S_IWUSR) permisos[2]='w';
    if (m&S_IXUSR) permisos[3]='x';
    if (m&S_IRGRP) permisos[4]='r';    /*grupo*/
    if (m&S_IWGRP) permisos[5]='w';
    if (m&S_IXGRP) permisos[6]='x';
    if (m&S_IROTH) permisos[7]='r';    /*resto*/
    if (m&S_IWOTH) permisos[8]='w';
    if (m&S_IXOTH) permisos[9]='x';
    if (m&S_ISUID) permisos[3]='s';    /*setuid, setgid y stickybit*/
    if (m&S_ISGID) permisos[6]='s';
    if (m&S_ISVTX) permisos[9]='t';

    return permisos;
}

int stringSplit (char *string, char *substring[]){
    int i=1;
    if((substring[0]=strtok(string, " \n\t")) == NULL){
        return 0;
    }
    while((substring[i]=strtok(NULL, " \n\t")) != NULL){
        i++;
    }
    return i;
}

void get_input(char *str,int size){
    fgets(str,size,stdin);
    strtok(str, "\n");
}

void authors(int substring_size,char *substring[substring_size]) {
    if (substring[1] == NULL) {
        printf("Clara Valle Gómez: clara.valle@udc.es\n"
               "Raúl Mouzo Quiza: raul.mouzo@udc.es\n");
    } else if(strcmp(substring[1], "-l") == 0){
        printf("clara.valle@udc.es\n"
               "raul.mouzo@udc.es\n");
    } else if(strcmp(substring[1], "-n") == 0) {
        printf("Clara Valle Gómez\n"
               "Raúl Mouzo Quiza\n");
    }
}

void pid(int substring_size,char *substring[substring_size]) {
    if (substring[1] != NULL && strcmp(substring[1], "-p") == 0 ) {
        printf("Pid del padre del shell:%d\n", getppid());
    } else {
        printf("Pid de la shell %d\n", getpid());
    }
}


void directory(int substring_size,char *substring[substring_size]){
    const int MAX_BUF = 256;
    char path[MAX_BUF];

    if(substring[1] == NULL){
        if(getcwd(path, MAX_BUF)!=NULL){
            printf("%s\n",path);
        } else {
            printf("Error: %s\n", strerror(errno));
        }
    } else {
        if(chdir(substring[1]) == -1){
            printf("Imposible cambiar directorio: %s\n", strerror(errno));
        }
    }
}

void datetime(int substring_size,char *substring[substring_size]){
    time_t t = time(NULL); // Se crea un struct de tipo tm con la fecha y la hora
    struct tm tm = *localtime(&t);

    int h, min, sec, day, month, year;
    h = tm.tm_hour;
    min = tm.tm_min;
    sec = tm.tm_sec;
    day = tm.tm_mday;
    month = tm.tm_mon + 1;
    year = tm.tm_year + 1900;

    if (substring[1] == NULL) {
        printf("%02d:%02d:%02d\n%d/%02d/%02d\n", h, min, sec, day, month, year);
    } else if(strcmp(substring[1], "-d") == 0){
        printf("%d/%02d/%02d\n", day, month, year);
    } else if(strcmp(substring[1], "-h") == 0) {
        printf("%02d:%02d:%02d\n", h, min, sec);
    }
}
void infosys(){
    struct utsname buffer;
    uname(&buffer);

    printf("%s (%s), OS: %s-%s-%s\n", buffer.nodename, buffer.machine, buffer.sysname, buffer.release, buffer.version);
}

void help(int substring_size, char *substring[substring_size]){
    if (substring[1] == NULL) {
        printf("\'ayuda cmd\' donde cmd es uno de los siguientes comandos: "
               "\nfin salir bye fecha pid autores hist comando carpeta infosis ayuda create stat list delete deltree allocate deallocate i-o memdump memfill memory recurse\n");
    } else if (strcmp(substring[1], "fin") == 0 || strcmp(substring[1], "salir") == 0 || strcmp(substring[1], "bye") == 0) {
        printf("%s\tTermina la ejecucion del shell\n", substring[1]);
    } else if (strcmp(substring[1], "fecha") == 0) {
        printf("%s [-d|.h\tMuestra la fecha y o la hora actual\n", substring[1]);
    } else if (strcmp(substring[1], "pid") == 0) {
        printf("%s [-p]\tMuestra el pid del shell o de su proceso padre\n", substring[1]);
    } else if (strcmp(substring[1], "autores") == 0) {
        printf("%s [-n|-l]\tMuestra los nombres y logins de los autores\n", substring[1]);
    } else if (strcmp(substring[1], "hist") == 0) {
        printf("%s [-c|-N]\tMuestra el historico de comandos, con -c lo borra\n", substring[1]);
    } else if (strcmp(substring[1], "comando") == 0) {
        printf("%s [-N]\tRepite el comando N (del historico)\n", substring[1]);
    } else if (strcmp(substring[1], "carpeta") == 0) {
        printf("%s [dir]\tCambia (o muestra) el directorio actual del shell\n", substring[1]);
    } else if (strcmp(substring[1], "infosis") == 0) {
        printf("%s \tMuestra informacion de la maquina donde corre el shell\n", substring[1]);
    } else if (strcmp(substring[1], "create") == 0) {
        printf("%s [-f] [name]\t Crea un directorio o un fichero (-f)\n", substring[1]);
    } else if (strcmp(substring[1], "stat") == 0) {
        printf("%s [-long][-link][-acc] name1 name2 ..\t lista ficheros\n\t\t-long: listado largo\n\t\t-acc: acesstime\n\t\t-link: si es enlace simbolico, el path contenido\n", substring[1]);
    } else if (strcmp(substring[1], "list") == 0) {
        printf("%s list [-reca] [-recb] [-hid][-long][-link][-acc] name1 name2 ..\tlista contenidos de directorios\n\t\t-hid: incluye los ficheros ocultos\n\t\t-reca: recursivo (antes)\n\t\t-recb: recursivo (despues)\n\t\tresto parametros como stat\n" , substring[1]);
    } else if (strcmp(substring[1], "delete") == 0) {
        printf("%s [name1 name2 ..]\tBorra ficheros o directorios vacios\n", substring[1]);
    } else if (strcmp(substring[1], "deltree") == 0) {
        printf("%s [name1 name2 ..]\tBorra ficheros o directorios no vacios recursivamente\n", substring[1]);
    } else if (strcmp(substring[1], "allocate") == 0) {
        printf("%s [-malloc|-shared|-createshared|-mmap]...\tAsigna un bloque de memoria\n\t-malloc tam: asigna un bloque malloc de tamano tam\n\t-createshared cl tam: asigna (creando) el bloque de memoria compartida de clave cl y tamano tam \n\t-shared cl: asigna el bloque de memoria compartida (ya existente) de clave cl\n\t-mmap fich perm: mapea el fichero fich, perm son los permisos\n", substring[1]);
    } else if (strcmp(substring[1], "deallocate") == 0) {
        printf("%s [-malloc|-shared|-delkey|-mmap|addr]...\tDesasigna un bloque de memoria\n\t-malloc tam: desasigna el bloque malloc de tamano tam\n\t-shared cl: desasigna (desmapea) el bloque de memoria compartida de clave cl\n\t-delkey cl: elimina del sistema (sin desmapear) la clave de memoria cl\n\t-mmap fich: desmapea el fichero mapeado fich\n\taddr: desasigna el bloque de memoria en la direccion addr\n", substring[1]);
    } else if (strcmp(substring[1], "i-o") == 0) {
        printf("%s [read|write] [-o]\tfiche addr cont\n\tread fich addr cont: Lee cont bytes desde fich a addr\n\twrite [-o] fich addr cont: Escribe cont bytes desde addr a fich. -o para sobreescribir\n\taddr es una direccion de memoria\n", substring[1]);
    } else if (strcmp(substring[1], "memdump") == 0) {
        printf("%s addr cont\tVuelca en pantallas los contenidos (cont bytes) de la posicion de memoria addr\n", substring[1]);
    } else if (strcmp(substring[1], "memfill") == 0) {
        printf("%s addr cont byte\tLlena la memoria a partir de addr con byte\n", substring[1]);
    } else if (strcmp(substring[1], "memory") == 0) {
        printf("%s [-blocks|-funcs|-vars|-all|-pmap]...\tMuestra muestra detalles de la memoria del proceso\n\t-blocks: los bloques de memoria asignados\n\t-funcs: las direcciones de las funciones\n\t-vars: las direcciones de las variables\n\t:-all: todo\n\t-pmap: muestra la salida del comando pmap(o similar)\n", substring[1]);
    } else if (strcmp(substring[1], "recursiva") == 0) {
        printf("%s [n]\tInvoca a la funcion recursiva n veces\n", substring[1]);
    } else {
        printf("%s no encontrado\n", substring[1]);
    }
}

void hist(tList* list, int substring_size, char *substring[substring_size], int* commandCounter){
    tPosL itemPos = first(*list);
    int i = 0;
    int numParam;
    if (substring[1] == NULL) {
        do {
            char* item = getItem(itemPos, *list);
            printf("%d -> %s\n", i, item);
            itemPos = next(itemPos, *list);
            i++;
        } while (itemPos != LNULL);

    } else if (strcmp(substring[1], "-c") == 0) {
        deleteList(list);
        *commandCounter = 0;

    } else if (substring[1][0] == '-' ) {
        memmove(substring[1],substring[1]+1, strlen(substring[1])); //Elimina el "-" para quedarnos con el número

        numParam = atoi(substring[1]);
        if (numParam != 0) { // Comprueba que sea un int
            do {
                char* item = getItem(itemPos, *list);
                printf("%d -> %s\n", i, item);
                itemPos = next(itemPos, *list);
                i++;
            } while (itemPos != LNULL && i < numParam);
        }
    }
}

char* commandN(tList* list, int substring_size, char *substring[substring_size]){
    tPosL itemPos = first(*list);
    int i = 0;

    while (itemPos != LNULL && i < atoi(substring[1])){
        itemPos = next(itemPos, *list);
        i++;
    }

    return getItem(itemPos, *list);
}


void create(int substring_size, char *substring[substring_size]){

    if(strcmp(substring[1],"-f") == 0){
        if(open(substring[2], O_CREAT | O_EXCL, 0755) == -1){ //flag O_CREAT to create new file
            printf("Imposible crear: %s\n", strerror(errno));
        }
    } else if(mkdir(substring[1], 0755) == -1){
        printf("Imposible crear: %s\n", strerror(errno));
    }
}

void printStat(char* directory, char* name, bool large, bool acc, bool link){
    struct stat stats;
    struct tm *dt;
    char buf[256] = "";

    if (lstat(directory, &stats) == -1) {
        printf("****error al acceder a %s: %s\n", directory, strerror(errno));
    } else {
        if (large) {
            if (acc) {
                //Convierte todos los datos de segundos (UTC) a su unidad (GMT+2)
                dt = localtime(&stats.st_atime);
            } else {
                dt = localtime(&stats.st_mtime);
            }
            printf("%04d/%02d/%02d-%02d:%02d ",dt->tm_year + 1900, dt->tm_mon + 1, dt->tm_mday,
                   dt->tm_hour, dt->tm_min);
            printf("%3ld (%8ld)     %s   %s %s %9ld %s", stats.st_nlink, stats.st_ino,getpwuid(stats.st_uid)->pw_name,
                   getgrgid(stats.st_gid)->gr_name, convierteModo(stats.st_mode), stats.st_size, name);
            if(link && readlink(directory, buf, sizeof(buf)) != -1){
                printf(" -> %s",buf);
            }
            printf("\n");
        } else {
            printf("%8ld  %s\n", stats.st_size, name);
        }
    }
}

void status(int substring_size, char *substring[substring_size]) {
    int i;
    bool large, link, acc, exception;
    large = acc = link = exception = false;

    for (i = 1; substring[i] != NULL; i++) {
        if (strcmp(substring[i], "-long") == 0 && !exception) { large = true; }
        else if (strcmp(substring[i], "-acc") == 0 && !exception) { acc = true; }
        else if (strcmp(substring[i], "-link") == 0 && !exception) { link = true; }
        else {
            exception = true;
            printStat(substring[i], substring[i], large, acc, link);
        }
    }
    if(!exception) {
        substring[1] = NULL;
        directory(substring_size, substring);
    }
}

void concatenate(const char *a, const char *b, const char *d, char* result){
    strcpy(result, a);
    strcat(result, b);
    strcat(result, d);
}

int isDir(const char* fileName){
    struct stat stats;
    if(lstat(fileName, &stats) == -1){
        return false;
    }
    return (LetraTF(stats.st_mode) == 'd');
}

void printEntries(DIR* dir, char* input, bool large, bool acc, bool link, bool hid){
    struct dirent *direntp;
    const size_t MAX_LENGTH_ENTRY = 256;
    char complete_dir[strlen(input) + MAX_LENGTH_ENTRY + 1];

    printf("************%s\n",input);
    while ((direntp = readdir(dir)) != NULL) {
        if(hid || direntp->d_name[0] != '.'){
            concatenate(input, "/", direntp->d_name, complete_dir);
            printStat(complete_dir, direntp->d_name, large, acc, link);
        }
    }
}

void functReca(char* input, bool large, bool acc, bool link, bool hid){
    DIR *dir;
    DIR *nextDir;
    struct dirent *direntp;
    const size_t MAX_LENGTH_ENTRY = 256;
    char complete_dir[strlen(input) + MAX_LENGTH_ENTRY + 1];
    dir = opendir(input);

    while((direntp = readdir(dir)) != NULL){
        concatenate(input,"/",direntp->d_name, complete_dir);
        if(isDir(complete_dir) && strcmp(direntp->d_name,".") != 0 && strcmp(direntp->d_name,"..") != 0 &&
            (hid || direntp->d_name[0] != '.') && (nextDir = opendir(complete_dir)) != NULL ){
            //Comprueba que sea directorio, que no sea ni . ni .. y que sea una direccion accesible
            printEntries(nextDir, complete_dir, large, acc, link, hid);
            functReca(complete_dir, large, acc, link, hid);
            closedir(nextDir);
        }
    }
    closedir(dir);
}

void functRecb(char* input, bool large, bool acc, bool link, bool hid){
    DIR *dir;
    DIR *nextDir;
    struct dirent *direntp;
    const size_t MAX_LENGTH_ENTRY = 256;
    char complete_dir[strlen(input) + MAX_LENGTH_ENTRY + 1];
    dir = opendir(input);

    while((direntp = readdir(dir)) != NULL){
        concatenate(input,"/",direntp->d_name, complete_dir);
        if(isDir(complete_dir) && strcmp(direntp->d_name,".") != 0 && strcmp(direntp->d_name,"..") != 0 &&
            (hid || direntp->d_name[0] != '.') && (nextDir = opendir(complete_dir)) != NULL ){
            //Comprueba que sea directorio, que no sea ni . ni .. y que sea una direccion accesible
            functRecb(complete_dir, large, acc, link, hid);
            printEntries(nextDir, complete_dir, large, acc, link, hid);
            closedir(nextDir);
        }
    }
    closedir(dir);
}

void lista(int substring_size, char *substring[substring_size]) {
    DIR *dir;
    bool large, link, acc, hid, reca, recb;

    large = acc = link = hid = reca = recb = false;

    for(int i = 1;substring[i] != NULL;i++){
        if (strcmp(substring[i], "-long") == 0 ) { large = true; }
        else if (strcmp(substring[i], "-acc") == 0 ) { acc = true; }
        else if (strcmp(substring[i], "-link") == 0 ) { link = true; }
        else if (strcmp(substring[i], "-hid") == 0 ) { hid = true; }
        else if (strcmp(substring[i], "-reca") == 0 ) {
            reca = true;
            recb = false;} //Si se instroduce reca y recb solo funciona el ultimo
        else if (strcmp(substring[i], "-recb") == 0 ) {
            recb = true;
            reca = false;}
        else {
            dir = opendir(substring[i]);
            if (dir == NULL){
                printf("****error al acceder a %s:%s\n",substring[i],strerror(errno));
            } else {
                if(reca){
                    printEntries(dir, substring[i], large, acc, link, hid);
                    functReca(substring[i], large, acc, link, hid);
                }else if(recb){
                    functRecb(substring[i], large, acc, link, hid);
                    printEntries(dir, substring[i], large, acc, link, hid);
                } else {
                    printEntries(dir, substring[i], large, acc, link, hid);
                }
                closedir(dir);
            }
        }
    }
}

void delete(char* path){
    DIR *dir;
    if (isDir(path)){
        dir = opendir(path);
        if(dir == NULL || rmdir(path) != 0){
            printf("****imposible borrar %s: %s\n",path,strerror(errno));
        }
        if(dir != NULL) {
            closedir(dir);
        }
    } else if (unlink(path) != 0) {
        printf("****imposible borrar %s: %s\n",path,strerror(errno));
    }
}

int deltree(char* input){
    DIR *dir;
    struct dirent *direntp;
    const size_t MAX_LENGTH_ENTRY = 256;
    char complete_dir[strlen(input) + MAX_LENGTH_ENTRY + 1];
    dir = opendir(input);
    if(dir == NULL){
        return 1;
    }

    while((direntp = readdir(dir)) != NULL){
        concatenate(input,"/",direntp->d_name, complete_dir);
        if(strcmp(direntp->d_name,".") != 0 && strcmp(direntp->d_name,"..") != 0){
            //Si es un directorio
            if(isDir(complete_dir)){
                //se sigue haciendo la recursividad
                if (deltree(complete_dir) != 0){
                    closedir(dir);
                    return 1;
                }
            }
            //Si no, se borra
            delete(complete_dir);
        }
    }
    closedir(dir);
    return 0;
}



void whichCommand(bool* exit,tMemList* memList, tList* list, char* inputAux,
        char* substring[], int SUBSTRING_SIZE, int* commandCounter, int* recursiveCounter) {
    char inputAux2[256];

    if (strcmp(substring[0], "comando") == 0 ) {
        if (substring[1] != NULL && atoi(substring[1]) >= 0 && atoi(substring[1]) < *commandCounter) {

            inputAux = commandN(list, SUBSTRING_SIZE, substring);

            printf("Ejecutando hist (%s): %s\n", substring[1], inputAux);

            strcpy(inputAux2, inputAux);
            stringSplit(inputAux2, substring);

            if (*recursiveCounter < 10){
                *recursiveCounter = *recursiveCounter + 1;
                whichCommand(exit, memList, list, inputAux, substring, SUBSTRING_SIZE, commandCounter, recursiveCounter);
            } else {
                printf("Demasiada recursion en hist\n");
                *recursiveCounter = 0;
            }

        } else if(substring[1]!=NULL){
            printf("No hay elemento %s en el historico\n", substring[1]);
        }
    } else if (strcmp(substring[0], "fin") == 0 || strcmp(substring[0], "salir") == 0 || strcmp(substring[0], "bye") == 0) {
        *exit = true;
    } else if (strcmp(substring[0], "autores") == 0) {
        authors(SUBSTRING_SIZE, substring);
    } else if (strcmp(substring[0], "pid") == 0) {
        pid(SUBSTRING_SIZE, substring);
    } else if (strcmp(substring[0], "carpeta") == 0) {
        directory(SUBSTRING_SIZE, substring);
    } else if (strcmp(substring[0], "fecha") == 0) {
        datetime(SUBSTRING_SIZE, substring);
    } else if (strcmp(substring[0], "infosis") == 0) {
        infosys();
    } else if (strcmp(substring[0], "ayuda") == 0){
        help(SUBSTRING_SIZE, substring);
    } else if (strcmp(substring[0], "hist") == 0) {
        hist(list, SUBSTRING_SIZE, substring, commandCounter);
    } else if(strcmp(substring[0], "create") == 0){
        create(SUBSTRING_SIZE, substring);
    } else if(strcmp(substring[0], "stat") == 0){
        status(SUBSTRING_SIZE, substring);
    } else if(strcmp(substring[0], "list") == 0){
        lista(SUBSTRING_SIZE, substring);
    } else if(strcmp(substring[0], "delete") == 0){
        for(int i = 1;substring[i] != NULL;i++){
            delete(substring[i]);
        }
    } else if(strcmp(substring[0], "deltree") == 0){
        for(int i = 1;substring[i] != NULL;i++){
            if(!isDir(substring[i])){
                delete(substring[i]);
            } else if(deltree(substring[i]) == 0){
                delete(substring[i]);
            } else {
                printf("Imposible borrar %s: %s\n", substring[i], strerror(errno));
            }
        }
    } else if(strcmp(substring[0], "allocate") == 0){
        allocate(memList,SUBSTRING_SIZE,substring);
    } else if(strcmp(substring[0], "deallocate") == 0){
        deallocate(memList,SUBSTRING_SIZE,substring);
    } else if(strcmp(substring[0], "memdump") == 0){
        memdump(SUBSTRING_SIZE,substring);
    } else if(strcmp(substring[0], "memfill") == 0){
        memfill(SUBSTRING_SIZE,substring);
    } else if(strcmp(substring[0], "recursiva") == 0){
        if(substring[1] != NULL)
            recurse(atoi(substring[1]));
    } else if(strcmp(substring[0], "memory") == 0){
        memory(SUBSTRING_SIZE,substring, *memList);
    } else if(strcmp(substring[0], "i-o") == 0){
        i_o(SUBSTRING_SIZE,substring);
    } else {
        printf("%s: no es un comando del shell\n", substring[0]);
    }
}

int main(){
    const int STRING_SIZE = 256;
    const int SUBSTRING_SIZE = 100;

    char input[STRING_SIZE];
    char *substring[SUBSTRING_SIZE];
    char inputAux[STRING_SIZE];
    bool exit=false;
    int commandCounter = 0;
    int recursiveCounter = 0;

    //Definición e inicialización de la lista del histórico
    tList list;
    tMemList memList;

    createEmptyList(&list);
    createEmptyMemoryList(&memList);


    while(!exit) {
        printf("> ");
        get_input(input, STRING_SIZE);
        strcpy(inputAux, input);
        stringSplit(input, substring);

        if (substring[0] != NULL) {
            insertItem(inputAux, LNULL, &list);
            commandCounter++;

            whichCommand(&exit,&memList, &list, inputAux, substring, SUBSTRING_SIZE,
                &commandCounter, &recursiveCounter);
        }
    }

    deleteList(&list);
    return 0;
}
