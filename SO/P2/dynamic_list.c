#include "dynamic_list.h"
#include <stdio.h>

void createEmptyList (tList* list) {
    *list = LNULL;
}

tPosL next (tPosL p, tList L){
        return p -> next;
}

bool createNode(tPosL *p){
    *p = malloc(sizeof(struct tNode));
    return *p != LNULL;
}

bool insertItem(char* data, tPosL p, tList* L){
    tPosL q;
    if(!createNode(&q)){
        return false;
    }else{
        strcpy(q->data, data);
        q -> next = LNULL;

        if(isEmptyList(*L)){
            *L = q;
        }else if(p == LNULL){
            last(*L)->next = q;
        }else if(p == *L){
            q -> next = p;
            *L = q;
        }else {
            q -> next = p -> next;
            p -> next = q;
            strcpy(q -> data, p -> data);
            strcpy(p -> data, data);
        }
        return true;
    }
}

tPosL last(tList L) {
    tPosL p;
    for(p = L; p -> next != LNULL; p = p -> next);
    return p;
}


bool isEmptyList(tList L){
    return L == LNULL;
}

void deleteList(tList* L){
    tPosL p;
    while(*L != LNULL){
        p = *L;
        *L = (*L) -> next;
        free(p);
    }
}

char* getItem(tPosL p, tList L){
    return (p -> data);
}

tPosL first(tList L){
    return L;
}
