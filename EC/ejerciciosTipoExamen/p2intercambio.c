#include <stdio.h>

#define N 512
double a[N][N], b[N][N];
int i, j;

int main() {
    for(j=0;j<N;j++){
        for(i=0;i<N;i++){
            a[i][j] = i+j;
        }
    }

    for(j=1;j<(N-1);j++){
        for(i=1; i<(N-1);i++){
            a[i+1][j] = a[i][j+1]+b[i][j];          
        }
    }
    printf("%f \n", a[N-1][N-1]);
}