#include <stdio.h>

#define N 20
int i,j;
float a[N][N],t;

int main() {
    for(j=0;j<N;j++){
        for(i=0;i<N;i++){
            a[i][j] = i+j;
        }
    }

    for(j=0;j<N;j++){
        for(i=0;i<N;i++){
            t += a[i][j];
        }
    }
    printf("%d \n",t);
}