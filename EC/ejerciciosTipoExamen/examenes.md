1 Parte 2: Técnicas de resolución de software

```
#define N 2000
int i,j;
float a[N][N],t;

int main() {
    for(j=0;j<N;j++){
        for(i=0;i<N;i++){
            t += a[i][j];
        }
    }
}
```

- 1 Técnica de intercambio de bucles
```
#define N 2000
int i,j;
float a[N][N],t;

int main() {
    for(i=0;i<N;i++){
        for(j=0;j<N;j++){
            t += a[i][j];
        }
    }
}
```

- Es legal aplicar la técnica del intercambio de bucles? Por que?

Si, por que los datos no dependen unos de otros, lo que no altera el resultado del programa.

- Y beneficioso? Por que?

Es beneficioso por que de esta forma recorres la matriz por filas, que es como lo hace C, lo que produce menos fallos


- Compila con -O2 la versión optimizada del código una vez aolicado el intercambio. Muestra la evolución de la tasa de fallos observada en la caché de nivel 1 para una caché de 8192 Bytes y líneas de 32 bytes, cambiando la asociatividad según la tabla.

```
gcc -O2 p1intercambioOpt.c 

valgrind --tool=cachegrind --D1=8192,1,32 ./a.out

D1  miss rate:      12.5%

valgrind --tool=cachegrind --D1=8192,2,32 ./a.out

D1  miss rate:      12.5% 

...
```

- 2 Dado el siguiente codigo: Es legal aplicar la tecnica del intercambio de bucles? Por que?

```
#define N 512
double a[N][N], b[N][N];
int i, j;

int main() {
    for(j=1;j<(N-1);j++){
        for(i=1; i<(N-1);i++){
            a[i+1][j] = a[i][j+1]+b[i][j];         
        }
    }
}
```

Si que es legal hacer el intercambio ya que no hay conflitos a la hora de modificar el array `a[i+1][j]`con `a[i][j+1]`


3 Parte 2: Tecnicas de optimizacion de software

```
#define N 500
double a[N][N], b[N][N];
int i, j;

int main() {
    for(j=1; j<(N-1);j++){
        for(i=1; i<(N-1);i++){
            a[i][j] += b[i][j+i];
        }
    }

    for(i=1; i<(N-1); i++){
        for(j=1; j<(N-1); j++){
            b[i][j] += a[i-1][j];
        }
    }
}

```

- ¿Que tecnica o tecnicas se pueden aplicar para mejorar el comportamiento en cache del coodigo?

