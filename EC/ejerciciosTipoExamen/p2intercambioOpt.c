#include <stdio.h>

#define N 3
double a[N][N], b[N][N];
int i, j;

int main() {
    //INIT
    for(j=0;j<N;j++){
        for(i=0;i<N;i++){
            a[i][j] = i+j;
            b[i][j] = i+i;
        }
    }

    for(i=1; i<(N-1);i++){
        for(j=1;j<(N-1);j++){
            a[i+1][j] = a[i][j+1]+b[i][j];          
        }
    }

    printf("Sin optimizar\n");
    for (int x = 0; x < N; x++) {
        for (int y = 0; y < N; y++) {
            printf("%f ", a[x][y]);
        }
        printf("\n");
    }


    printf("\n");

    //INIT
    for(j=0;j<N;j++){
        for(i=0;i<N;i++){
            a[i][j] = i+j;
            b[i][j] = i+i;
        }
    }

    for(j=1; j<(N-1);j++){
        for(i=1;i<(N-1);i++){
            a[i+1][j] = a[i][j+1]+b[i][j];          
        }
    }

    printf("Optimizada\n");

    for (int x = 0; x < N; x++) {
        for (int y = 0; y < N; y++) {
            printf("%f ", a[x][y]);
        }
        printf("\n");
    }
}