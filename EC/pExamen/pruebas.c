#include <stdio.h>

#define N 3
double a[N][N], b[N][N];
int i, j;

int main() {
    //INIT
    for(j=0;j<N;j++){
        for(i=0;i<N;i++){
            a[i][j] = i+j;
            b[i][j] = i+i;
        }
    }

    for(j=1;j<(N-1);j++){
        for(i=1;i<(N-1);i++){
            a[i][j] = a[i-1][j]+b[i][j];
        }
    }

    for(j=1;j<(N-1);j++){
        for(i=1;i<(N-1);i++){
            a[i][j] = a[i+1][j]+b[i][j];
        }
    }

    printf("Sin optimizar\n");
    printf("a \n");
    for (int x = 0; x < N; x++) {
        for (int y = 0; y < N; y++) {
            printf("%f ", a[x][y]);
        }
        printf("\n");
    }

    printf("b \n");
    for (int x = 0; x < N; x++) {
        for (int y = 0; y < N; y++) {
            printf("%f ", b[x][y]);
        }
        printf("\n");
    }


    printf("\n");

    //INIT
    for(j=0;j<N;j++){
        for(i=0;i<N;i++){
            a[i][j] = i+j;
            b[i][j] = i+i;
        }
    }

    
    //Se pueden intercambiar? SI
    //Se pueden fusionar? SI
    //Se pueden fusionar arrays? NO
    for(j=1;j<(N-1);j++){
        for(i=1;i<(N-1);i++){
            a[i][j] = a[i-1][j]+b[i][j];
            a[i][j] = a[i+1][j]+b[i][j];
        }
    }

    printf("Optimizada\n");
    printf("a \n");
    for (int x = 0; x < N; x++) {
        for (int y = 0; y < N; y++) {
            printf("%f ", a[x][y]);
        }
        printf("\n");
    }

    printf("b \n");
    for (int x = 0; x < N; x++) {
        for (int y = 0; y < N; y++) {
            printf("%f ", b[x][y]);
        }
        printf("\n");
    }
}

//El que escribe tiene que hacer dos accesos a memoria, es mejor que falle el que lee