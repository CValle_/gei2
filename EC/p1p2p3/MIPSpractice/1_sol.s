## 83 ciclos

.data
n: .word 10
a: .float 1,2,3,4,5,6,7,8,9,10
b: .space 40
.text 
.globl main

main:
la $t0, n
lw $t0,0($t0)
la $t1,a
la $t2,b

loop:
lwc1 $f1,0($t1)
addi $t0, $t0, -1
add.s $f0,$f1,$f0
addi $t1, $t1, 4
swc1 $f0,0($t2)
bne $t0, $0, loop
addi $t2, $t2, 4

end:
addi $v0,$0,10
syscall
