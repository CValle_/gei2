## 161 ciclos

.data
n: .word 9
x: .float 10, 5, 3, 9, 4, 3, 8, 3, 2
y: .float 1, 2, 3, 1, 2, 3, 1, 2, 3
suma: .float 0
.text
.globl main
main:
          la $t0, n
          lw $t0, 0($t0)
          la $t1, x
          la $t2, y
          la $t3, suma
          lwc1 $f0, 0($t3)

loop:
          lwc1 $f1, 0($t1)
          lwc1 $f2, 0($t2)
          add.s $f0, $f0, $f2
          mul.s $f6, $f1, $f2
          swc1 $f6, 0($t2)
          addi $t1, $t1, 4
          addi $t2, $t2, 4
          addi $t0, $t0, -1
          bne $t0, $0, loop
end:
          nop
          swc1 $f0, 0($t3)
          addi $v0, $0, 10
          syscall
