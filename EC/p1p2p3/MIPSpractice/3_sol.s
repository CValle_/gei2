##76 ciclos

.data
n: .word 10
a: .float 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
b: .float 1

.text
.globl main

main:
  la $t0, n
  lw $t0, 0($t0)
  la $t3, b

  add $t1, $0, $t3
    lwc1 $f0, -8($t1)
  lwc1 $f2, 0($t3)
    add $t2, $0, $0
loop:
    addi $t0, $t0, -1
  mul.s $f2, $f2, $f0
  lwc1 $f0, -12($t1)
  addi $t1, $t1, -4
  bne $t0, $0, loop
  swc1 $f2, 0($t1)
end:
  swc1 $f2, 0($t3)
  addi $v0, $0, 10
  syscall
