# P4 Cachegrind I

- Caché de primer nivel: `I1`
- Caché de primer nivel datos: `D1`
- Caché de nivel 2 (Last Level caché): `LL`
- Fallos de instrucciones de LL: `LLi`
- Fallos de datos de LL: `LLd`

- Número de instrucciones ejecutadas: `Ir`
- Fallos de lectura en caché Lvl1: `I1mr`
- Fallos de lectura en caché LL: `ILmr`

- Número de lecturas en memoria: `Dr`
- Fallos de lectura de datos en caché Lvl1: `D1mr`
- Fallos de lectura de datos en caché LL: `DLmr`

- Número de escrituras en memoria: `Dw`
- Fallos de escritura de datos en caché Lvl1: `D1mw`
- Fallos de escritura de datos en caché LL: `DLmw`

### Funcionamiento

- Para ejecutar cachegrind sobre un proceso:

```
valgrind --tool=cachegrind ./ejecutable
```

Puede usarse la opción `-v` para ver más detalles

- Para configurar la caché (de primer nivel en este caso) con los parámetros deseados (en Bytes)

```
--D1=<tamaño de caché>,<asociatividad>,<tamaño de línea>
```

- Para ver más detalles con el comando `cg_annnotate`

```
gcc -g -O3 -o traspuesta traspuesta.c
valgrind --tool=cachegrind ./traspuesta
cg_annotate --auto=yes cachegrind.out.<pid>
```

### P4.1 Ej-1

Ejecutar el programa traspuesta con cachegrind y responder a las siguientes preguntas:

```
gcc -o traspuesta traspuesta.c
valgrind --tool=cachegrind --LL=2048,1,64 ./traspuesta
```

- Números de fallos obtenidos en LL?

    12678

- Tasa de fallos local de la LL?

    0.05%

- Número de accesos total a datos?

    562609

- Tasa de fallos global de caché?

    2.1 * 0.5 = 1.05%

> Sólo se accedería a la caché del siguiente nivel, si se falla en la actual. Por lo tanto, la tasa de fallos global es igual a la multiplicación de todas las tasas de fallos locales.

    

### P.14 Ej-2

Ejecutar el programa traspuesta con cachegrind y responder a las siguientes preguntas:

```
gcc -O2 -o traspuesta traspuesta.c

valgrind --tool=cachegrind --LL=2048,1,64 ./traspuesta
```

- Número de accesos total a datos?

    101,807 

- ¿Cómo cambia el número de accesos total a datos entre la versión optimizada y la versión sin opti-
mizar? ¿Por qué?

En la versión optimizada el número de accesos a datos es menor ya que la caché es accedida más eficientemente

### P4.1 Ej-3

```
gcc -g -O2 -o traspuesta traspuesta.c
valgrind --tool=cachegrind --cachegrind-out-file=output --D1=4096,1,64 ./traspuesta
cg_annotate --auto=yes output
```

- ¿Cuántos fallos produce la instrucción `a[i][j] = (double)(i * j)` en la caché de datos de nivel 1?

D1mr + D1mw = 0 + 5290 = 5290

- ¿Cuántos fallos produce la instrucción `b[j][i] = a[i][j]` en la caché de datos de nivel 1?

D1mr + D1mw = 0 + 40000 = 40000

### P4.2 Ej-1

ejecuta los programas base con el siguiente código

```
gcc -g -O2 -DN=100 -DNUM_REPS=1 ejemplo.c
```

- `gcc -O3 fusion_arrays.c`        
R: 44869888.000000
Tu: 0.029075  Tlib: 0.000395

- fusion_bucles-c -> Tu: 0.562029  Tlib: 0.000227

- intercambio.c -> R:133955584.000000
Tu: 0.140963  Tlib: 0.000019


### P4.2 Ej-2
Optimiza los ejercicios con las técnicas indicadas:

- `gcc -O3 fusion_arrays.c`       
R: 44869888.000000
Tu: 0.028708  Tlib: 0.000188





