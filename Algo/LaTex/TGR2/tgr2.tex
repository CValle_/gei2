\documentclass[a4paper, 12pt, titlepage]{extarticle}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{fancyhdr}

\setlength{\headheight}{15pt}
\setlength{\parindent}{0pt} % no indent paragraphs
\usepackage[margin=1in,bottom=1.2in,top=1.4in]{geometry}

\usepackage{amsmath, mathtools, amssymb, amsfonts, mathrsfs} %math
\usepackage{centernot}
\usepackage{array}
\usepackage{multicol}
\usepackage{spalign} %systems
\usepackage{polynom} %ruffini
\usepackage{dirtree} %trees

\makeatletter
\renewcommand*\env@matrix[1][*\c@MaxMatrixCols c]{%
  \hskip -\arraycolsep
  \let\@ifnextchar\new@ifnextchar
  \array{#1}}
\makeatother

% -- colors
\usepackage{soul}
\usepackage[dvipsnames]{xcolor}

\definecolor{Lilac}{HTML}{c3cde6}

\newcommand{\hllc}[1]{\colorbox[HTML]{c3cde6}{$\displaystyle #1$}} % formato ancho cian
\newcommand{\hllr}[1]{\colorbox[HTML]{ffd7c6}{$\displaystyle #1$}} % formato ancho rosa
\newcommand{\hlly}[1]{\colorbox[HTML]{e7c9ae}{$\displaystyle #1$}} % formato ancho amarillo
\newcommand{\hlfancy}[2]{\sethlcolor{#1}\hl{#2}} % no afecta al ancho del caracter

\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing}

\newcommand{\xmark}{\ding{55}}
\newcommand{\comma}{$,$}
\newcommand{\curlyarrow}{\mathrel{\leadsto}}

%--- MAKETITLE ------------------------------------------------------------------------------

\def\myauthor{Clara Valle Gómez}
\author{\myauthor}

\title{
    \begin{Huge}
        {\textbf{ALGORITMOS}}\\
    \end{Huge}
    \vspace{50px}
    \textbf{TGR2 - Tema 1}\\
    Análisis de algoritmos. Resolución de recurrencias
    \date{12 de mayo de 2022}%\today
}

%--- CABECERA Y PIE DE PÁGINA -------------------------------------------------------------------------
\pagestyle{fancy}
\lhead{TGR2}
\chead{}
\rhead{C. Valle} 
\lfoot{}
\cfoot{}
\rfoot{\thepage}    

%--- DOC ----------------------------------------------------------------------------   

\begin{document}
\maketitle
   
%--- ÍNDICE -------------------------------------------------------------------------
    \newpage
    \tableofcontents

    \newpage
    \section{Relaciones de recurrencia}
    Una sucesión es una aplicación del conjunto números naturales en un conjunto S. Se suele usa $a_n$ para denotar la imagen de $\mathbb(n)$, el término n-ésimo de la sucesión.
   
    \[
        \def\arraystretch{0.9}
        \begin{array}{c}
            \mathbb{N} \rightarrow S\\
            \ n \curlyarrow a_n\\
        \end{array}
    \]

    \[
        \boxed{
            \!\begin{aligned}
                &\text{Se llama relación de recurrencia para una sucesión } \{a_n\} \text{ a toda expresión matemática}\\
                & \text{que relaciona cada término $a_n$, a partir de uno dado, con los anteriores.}
            \end{aligned}
        }
    \]

    \hspace{10px}

    Resolver una relación de recurrencia es encontrar las sucesiones que la satisfacen dando una fórmula explícita para el cálculo de su n-ésimo término.

    \subsection{Ejemplo: Cadenas binarias de long n sin ceros consecutivos}
    \begin{itemize}
        \item Para $n=0$ existe las cadenas $\{\emptyset\}$, por lo tanto: $a_0 = 1$
        \item Para $n=1$ existen las cadenas $\{0\}$ y $\{1\}$ sin ceros consecutivos, por lo tanto: $a_1 = 2$
        \item Para $n=2$ existen las cadenas $\{0,1\}$, $\{1,0\}$ y $\{1,1\}$, por lo tanto: $a_2 = 3$
    \end{itemize}
    \hspace{15px}\dots

    \begin{center}
        \begin{tabular}{|c||c|c|c|c|}
            \hline
            n     & 0 & 1 & 2 & 3 \\ \hline
            $a_n$ & 1 & 2 & 3 & 5 \\ \hline
            \end{tabular}
    \end{center}
    $a_n$: número de cadenas de longitud n sin ceros consecutivos\\

    Buscamos una expresión que relacione un término con los anteriores y resuelva la relación de recurrencia:

    \begin{itemize}
        \item Cogemos las cadenas de $a_n$ que empiezan por 1: $a_{n-1}$ 
    \end{itemize}
    \begin{center}
        \begin{tabular}{c|ccc|}
            \hline
            \multicolumn{1}{|c|}{1} & \multicolumn{1}{c|}{n} & \multicolumn{1}{c|}{n} & ... \\ \hline
                                    & \multicolumn{3}{c|}{$a_{n-1}$}                        \\ \cline{2-4} 
            \end{tabular}
    \end{center}

    \begin{itemize}
        \item Y añadimos las cadenas de $a_n$ que empiezan por 0: $a_{n-2}$
    \end{itemize}

    \begin{center}
        \begin{tabular}{cc|cc|}
            \hline
            \multicolumn{1}{|c|}{0} & 1 & \multicolumn{1}{c|}{n}  & ...  \\ \hline
                                    &   & \multicolumn{2}{c|}{$a_{n-2}$} \\ \cline{3-4} 
            \end{tabular}
    \end{center}

    Por lo tanto la expresión que soluciona el algoritmo es:

    \[a_n = a_{n-1} + a_{n-2}\]
    \hspace{20px}

    \textbf{Deben tenerse en cuenta las condiciones iniciales, ya que con la misma relación de recurrencia $a_n = a_{n-1} + a_{n-2}$ y determinando unas condiciones iniciales $\mathbf{a_0= 0\, \ a_1=1}$, se determina una sucesión diferente, la sucesión de Fibonacci}

    \subsection{Ejemplo resolución relaciones recurrencia: Torres de Hanoi}

    Encontrar la sucesión $\{H_n\}$, donde $H_n$ es el número de movimientos necesarios para resolver las torres de Hanoi:
    \begin{enumerate}
        \item Pasar todos los discos superiores excepto el último: $H_{n-1}$ movimientos
        \item Mover el último disco a la última columna: $+1$ movimiento
        \item Pasar todos los discos de la segunda columna a la tercera: $H_{n-1}$ movimientos
    \end{enumerate}

    \begin{figure}[!htb]
        \minipage{0.32\textwidth}
        \includegraphics[width=130px]{img/fig1.png}
        \caption{Paso 1}
        \endminipage\hfill
        \minipage{0.32\textwidth}
        \includegraphics[width=130px]{img/fig2.png}
        \caption{Paso 2}
        \endminipage\hfill
        \minipage{0.32\textwidth}%
        \includegraphics[width=130px]{img/fig3.png}
        \caption{Paso 3}
        \endminipage
        \end{figure}

        Por lo que la solución de la relación de recurrencia sería $2H_{n-1}+1$.\\

        Se puede simplificar a $2\cdot (2^{n-1} -1) +1$ número de movimientos\footnote{Dentro de esta sucesión también entra la sucesión constante $\{-1,-1,-1,...\}$ , que, evidentemente, no es una solución del problema.}

        \[a_n = 2\cdot (2^{n-1} -1) +1\]

        \section{Relaciones de recurrencia homogéneas}
        Una relación de recurrencia homogénea de coeficientes constantes (RRLHCC), de orden k es una expresióm de la forma:
        \[a_n = c_1a_{n-1}+c_2a_{n-2}+\dots + c_ka_{n-k}\]
        Dónde los coeficientes constantes $\{c_1,c_2, \dots , c_k\}$ son números y $c_k \not = 0$

        \begin{itemize}
            \item \textbf{Orden} $(k)$: Número de términos relacionados en la sucesión
            \item \textbf{Grado}: mayor exponente al que están elevados los elementos de la sucesión
            \item \textbf{Ecuación homogénea}: sucesiones que no tienen términos independientes
            \item \textbf{Coeficientes constantes}: son los términos que no dependen de n
        \end{itemize}

        Algunos ejemplos de relaciones de recurrencia homogéneas son:
        \begin{itemize}
            \item $F_n = F_{n-1} + F_{n-2}$ Es una RRLHCC de orden 2
            \item $a_n = 2a_{n-1} + 3a_{n-2} - 5a_{n-3}$ es una RRLHCC de orden 3
            \item $a_n = na_{n-1}$ es una RRLH de coef. no constantes ($n$)
            \item $a_n = 2a_{n-1} +1$ es una RRLCC de orden 1 pero no homogénea ($+1$)
            \item $a_n = 2a_{a-1}\cdot a_{n-2}$ es una RRHCC de orden 2 pero no lineal ($a\cdot a$)
        \end{itemize}

        \subsection{Resolución de una RRLHCC}
        Para resolver una RRLHCC buscaremos soluciones de tipo $\ a_n = r^n,\ r\not = 0$
        \[a_n = \hllr{c_1}a_{n-1}+\hllr{c_2}a_{n-2}+\dots + \hllr{c_k}\ a_{n-\textcolor{Peach}{k}}\]
        \begin{enumerate}
            \item Sustituyendo, tenemos:
        \end{enumerate}
        \[r^n= c_1r^{n-1}+c_2r^{n-2}+\dots + c_k r^{n-k}\]
        \begin{enumerate}
            \setcounter{enumi}{1}
            \item Dividimos por el término más pequeño ($r^{n-k}$)
        \end{enumerate}
        \[\dfrac{r^n}{r^{n-k}}
        = c_1\cdot\dfrac{r^{n-1}}{r^{n-k}}+c_2\cdot\dfrac{r^{n-2}}{r^{n-k}}+\dots + c_k\cdot\dfrac{\centernot{r^{n-k}}}{\centernot{r^{n-k}}}\]
        \begin{enumerate}
            \setcounter{enumi}{2}
            \item Simplificando y ordenando, obtenemos la ecuación y raíces características $\hllc{r_n}$
        \end{enumerate}
        \[
            \boxed{r\textcolor{Peach}{^k} - \hllr{c_1} r\textcolor{Peach}{^{k-1}} - \hllr{c_2} r\textcolor{Peach}{^{k-2}} - \dots - \hllr{c_k} = 0}
        \]
        \[
            \text{\scriptsize Para ecuaciones de grado mayor a dos deberán resolverse por Ruffini}
        \]

        \subsection{Resolución de RRLHCC con raíces características distintas}
        Sea una RRLHCC tal que sus raíces características $\hllc{\{r_1,\dots, r_k\}}$ son todas \hllc{\text{reales y distintas}}.

        \[a_n = c_1a_{n-1}+c_2a_{n-2}+\dots + c_ka_{n-k}\]
        
        Para cualquier número $\alpha_1, \dots, \alpha_n$, la sucesión siguiente es \textbf{solución} para la recurrencia:

        \[\boxed{a_n = \alpha_1\hllc{r_{1}^n}+\alpha_2\hllc{r_{2}^n}+\dots + \alpha_k\hllc{r_{k}^n}}\]
        \

        Se resuelve con un sistema de ecuaciones, sustituyendo las condiciones iniciales $a_n$.

        \subsubsection{Ejemplo resolución de RRLHCC de raíces distintas}
        
        $\mathbf{\ a_n = \hllr{5}a_{n-1}-\hllr{6}a_{n-2}, \quad a_0=1,\ a_1 = 2}$\\
        \begin{enumerate}
            \item Dividimos entre $r^{n-2}$ $\hllr{\scriptstyle (k=2)}$ para obtener la ecuación característica:
            \[ r^2 -\hllr{5}r + \hllr{6} = 0 \]
            \item Resolvemos con la fórmula cuadrática
            \[ r =\frac{-5 \pm \sqrt{5^2-4\cdot 1\cdot 6}}{2 \cdot 1} = \dfrac{5\pm1}{2} \quad \hllc{r_1 = 2} \text{ y } \hllc{r_2 = 3}\]
            \item Al ser las raíces distintas sustituímos en la ecuación solución:
            \[\boxed{a_n =\alpha_1\hllc{2^n}+\alpha_2\hllc{3^n}}\]
            \item Sustituimos con las condiciones iniciales ($a_n$):
            \[
                \spalignsys{
                    n = 0 \quad\rightarrow\quad \alpha_1 \cdot 2^0 + \alpha_2 \cdot 3^0 = a_0 \quad\rightarrow \quad \alpha_1 + \alpha_2 = 1;
                    n = 1 \quad\rightarrow\quad \alpha_1 \cdot 2^1 + \alpha_2 \cdot 3^1 = a_1 \quad \rightarrow \quad 2\alpha_1 + 3\alpha_2 = 2;
                }
            \]
            $\ $
            \item Hallamos los coeficientes resolviendo el sistema
            \begin{multicols}{3}
                \noindent
                \[
                    \spalignsys{
                        \alpha_1 + \alpha_2 = 1;
                        2\alpha_1 + 3\alpha_2 = 2;
                    }
                    \quad
                    \xrightarrow{x2}
                \]
                \columnbreak
                \[
                    \spalignsys{
                        \centernot{2\alpha_1} + 2\alpha_2 = \not 2;
                        \centernot{2\alpha_1} + 3\alpha_2 = \not 2;
                    }
                    -
                    \quad
                    \rightarrow
                \]
                \columnbreak
                \[
                    \spalignsys{
                        \alpha_1 = 1 \text{ (sustituyendo)}; 
                        \alpha_2 = 0;
                    }
                \]
            \end{multicols}

            \item Sustituyendo los coeficientes en la ecuación queda:
            \[\boxed{a_n = 2^n}\]
        \end{enumerate}

        \subsection{Resolución RRLHCC con raíces características no distintas}
        Sea una RRLHCC tal que sus raíces características $\hllc{\{r_1,\dots, r_k\}}$ son todas reales con \hllc{\text{multiplicidades respectivas}}\footnote{\textbf{Multiplicidad}: Número de veces que se repite una raíz} $\{m_1,\dots,m_k\}$
        \[a_n = c_1a_{n-1}+c_2a_{n-2}+\dots + c_ka_{n-k}\]
        Las soluciones son de la forma:
        \[a_n = (a_{10} + a_{11}\cdot n + \dots + a_{{m1}-1} \cdot n^{{m1}-1})\cdot r_{1}^n +\dots + (a_{s0} + a_{s1}\cdot n + \dots + a_{{sm}-1} \cdot n^{m-1})\cdot r_{s}^n\]
        Simplificando:
        \[a_n = P_{m_1-1}(n)\cdot r_1^n + \dots + P_{m_s-1}(n)\cdot r_s^n\]

        \subsubsection{Ejemplo resolución de RRLHCC de raíces no distintas}
        Sea una RRLHCC de raíces $\{r_1,\dots , r_2\}$ reales y no distintas.
        \[
            tn = 
            \spalignsys{
                n \- \- \- \- \- \- \- \quad n = 0\comma\ 1\comma\ 2;
                \hllr{5} t_{n-1} - \hllr{8} t_{n-2} + \hllr{4} t_{n-3} \quad n \geq 3;
            }
        \]
        \[\scriptstyle  \text{Sistema de ecuaciones de orden 3 (3 incógnitas)}\]

        \begin{enumerate}
            \item Dividimos entre $r^{n-3}$ $\hllr{\scriptstyle (k=3)}$ para obtener la ecuación característica
            \[r^3 - 5r^2 + 8r - 4 = 0\]
            \item Resolvemos la ecuación por ruffini (resuelto en el punto \ref{ruffini}), da como raíces:
            \[\hllc{r_1 = 1}\quad \text{ y }\quad \hllc{r_2 = r_3 = 2} \ \scriptstyle(doble)\]
            \item Sustituyendo en la ecuación solución:
            
            \[t_n = \alpha_1\cdot \hllc{1}^n + \alpha_2\cdot \hllc{2}^n + \alpha_3\cdot \hllc{2}^n \quad\rightarrow\quad \boxed{t_n = \alpha_1\cdot 1^n + (\alpha_2+\alpha_3\cdot n)\cdot 2^n}\]

            \item Sustituimos con las condiciones iniciales ($t_0 = 0,\ t_1 = 1,\ t_2 = 2$)
            \[
                \spalignsys{
                    t_0 = 0 \quad\rightarrow\quad 
                    \alpha_1\cdot 1^0 + \alpha_2\cdot 2^0 + \alpha_3\cdot 2^0 \cdot 0 = t_n \quad\rightarrow \quad 
                    \alpha_1 + \alpha_2 \- \- = 0;
                    t_1 = 1 \quad\rightarrow\quad 
                    \alpha_1\cdot 1^1 + \alpha_2\cdot 2^1 + \alpha_3\cdot 2^1 \cdot 1 = t_n \quad \rightarrow \quad 
                    \alpha_1 + 2\alpha_2 + 2\alpha_3 = 1;
                    t_2 = 2 \quad\rightarrow\quad 
                    \alpha_1\cdot 1^2 + \alpha_2\cdot 2^2 + \alpha_3\cdot 2^2 \cdot 2 = t_n \quad \rightarrow \quad 
                    \alpha_1 + 4\alpha_2 + 8\alpha_3 = 2;
                }
            \]
            $\ $
            \item Resolvemos el sistema de ecuaciones:
            
            \[
                \begin{pmatrix}[ccc|c]
                    1 & 1 & 0 & 0 \\
                    1 & 2 & 2 & 1 \\
                    1 & 4 & 8 & 2 \\
                \end{pmatrix}
                \xrightarrow[F3 = F3 - F1]{F2 = F2 - F1}
                \begin{pmatrix}[ccc|c]
                    1 & 1 & 0 & 0 \\
                    0 & 1 & 2 & 1 \\
                    0 & 3 & 8 & 2 \\
                \end{pmatrix} 
                \xrightarrow{F2 = 4F2 - F3}
                \begin{pmatrix}[ccc|c]
                    1 & 1 & 0 & 0 \\
                    0 & 1 & 0 & 2 \\
                    0 & 3 & 8 & 2 \\
                \end{pmatrix} 
            \]
            \[
                \xrightarrow[F3 = F3 - 3F2]{F1 = F1 - F2}
                \begin{pmatrix}[ccc|c]
                    1 & 0 & 0 & -2 \\
                    0 & 1 & 0 & 2 \\
                    0 & 0 & 8 & -4 \\
                \end{pmatrix} 
                \xrightarrow{F3 = F3 / 8}
                \begin{pmatrix}[ccc|c]
                    1 & 0 & 0 & -2 \\
                    0 & 1 & 0 & 2 \\
                    0 & 0 & 1 & \frac{-1}{2} \\
                \end{pmatrix} 
            \]

            \[\alpha_1 = -2,\ \alpha_2 = 2,\ \alpha_3 = \dfrac{-1}{2}\]
            \item Sustituimos en la ecuación y obtenemos la solución:
            \[t_n = -2\cdot 1^n + (2-\dfrac{1}{2}\cdot n)\cdot 2^n \]
            Simplificando:
            \[
                \def\arraystretch{1.8}
                \begin{array}{c}
                    t_n = -2 + (2^1-2^{-1}\cdot n)\cdot 2^n\\
                    \boxed{t_n = -2 + 2^{n+1} - 2^{n-1}\cdot n}\\
                \end{array}
            \]
        \end{enumerate}

        \section{Relaciones de recurrencia no homogéneas}

        Una relación de recurrencia lineal no homogénea de coeficientes constantes (RRLnHCC) de roden k es una expresión de la forma:
        \[a_n= c_1a_{n-1}+c_2a_{n-2}+\dots + c_ka_{n-k} + L(n)\]
        Dónde los coeficientes constantes $\{c_1,\dots,c_k\},\quad c_k\not=0$, son números reales y $L(n)$ es una función de n no nula.

        \subsection{Solución de una RRLnHCC}
        Siendo una RRLH asociada:
        \[a_n^{(h)} = c_1a_{n-1}^h+c_2a_{n-2}^h+\dots + c_ka_{n-k}^h\]

        Entonces si $a_n^{(p)}$ es una solución particular de la RRLnHCC y $a_n^{(h)}$ es cualquier solución de la RRLH asociada:
        \[a_n = a_n^{(h)} + a_n^{(p)}\]
        \[\text{general} = \text{homogénea} + \text{sol }L(n)\]

        \subsubsection{Ejemplo resolución RRLnHCC: Torres de Hanoi $L(n)=1$}
        \[h_n = \hllc{2h_{n-1}} + \hllr{1_{\ }} \ \text{ con }\ h_0 = 0\]

        Dado $\boxed{h_n =\hllc{a_n^{(h)}} + \hllr{a_n^{(p)}}}$ 
        
        \begin{enumerate}
            \item Sabiendo que $\{-1,-1,...\}$ es una solución particular:
            \[\boxed{h_n = a_n^{(h)}\hllr{-1}}\]
            \item Por otro lado resolvemos la RRLH asociada ($a_n^{(h)}$):
            \[a_n^{(h)} = \hllc{2h_{n-1}}\]
            Cuya solución es $\hllc{r_1 = 2}$
            \item Entonces sustituyendo en la ecuación solución:
            \[a_n^{(h)} = \alpha_1 \hllc{2}^n \qquad \rightarrow \qquad \boxed{h_n = \hllc{\alpha_1 2^n} \hllr{-1}_{\ }^{\ }}\]
            \item Finalmente añadiendo las condiciones inciales ($h_0 = 0$)
            \[\{ h_0 = \alpha_1 2^0 -1 = \alpha_1 -1 \qquad \rightarrow \qquad 0 = \alpha_1 -1 \]
            \[\boxed{h_n = 2 ^n -1}\]

        \end{enumerate}

        \subsection{Soluciones particulares}
        Siendo uns RRLnHCC:
        \[a_n= c_1a_{n-1}+c_2a_{n-2}+\dots + c_ka_{n-k} + L(n)\]
        Dónde $L(n) = (p_0+p_1n+\dots+p_t n^t)\hlly{s^n}, \quad$ podemos encontrarnos dos tipos:
        \begin{itemize} 
            \item[Caso 1.] Si $s$ no es una de las raíces de la relación homogénea asociada, etonces:
            \[a_n^{(p)} = (\beta_0+\beta_1n+\dots+\beta_t n^t)\hlly{s^n}\]
            \item[Caso 2.] Si $s$ es una de las raíces de la relación homogénea asociada, con multiplicidad $m$
            \[a_n^{(p)} = (\beta_0+\beta_1n+\dots+\beta_t n^t)\hlly{n^m s^n}\]
        \end{itemize}

        \subsubsection{Ejemlos de ecuaciones no homogéneas}

        \[a_n = 7a_{n-1} - 16a_{n-2} + 12a_{n-3} + L(n) \qquad r_1=r_2=2, r_3=3 \scriptstyle\text{ (ruffini)}\]
        \[\scriptstyle \text{en este apatado se irá cambiando } L(n) \text{ para ver los dos casos anteriores}\]

        \begin{itemize}
            \item $L(n) = (n+1)\hlly{5}^n$
        \end{itemize}
        \begin{itemize}
            \item [Caso 1.] $\hlly{s = 5} \not = 2,3$
            \[a_n^{(p)}=(\beta_0+\beta_1n)\hlly{5^n}\]
        \end{itemize}

        \begin{itemize}
            \item $L(n) = (n+1)\hlly{3}^n$
        \end{itemize}
        \begin{itemize}
            \item [Caso 2.] $\hlly{s = 3}= r_3$ con multiplicidad $\hlly{m=1}$
            \[a_n^{(p)}=(\beta_0+\beta_1n)\hlly{n^1 3^n}\]
        \end{itemize}

        % \begin{itemize}
        %     \item $L(n) = (n+1)\hlly{2}^n$
        % \end{itemize}
        % \begin{itemize}
        %     \item [Caso 2.] $\hlly{s = 2}= r_2 = r_1$ con multiplicidad $\hlly{m=2}$
        %     \[a_n^{(p)}=(\beta_0+\beta_1n)\hlly{n^2 2^n}\]
        % \end{itemize}

        \begin{itemize}
            \item $L(n) = 3 = 3\hlly{\color{gray}1^n}$
        \end{itemize}
        \begin{itemize}
            \item [Caso 1.] $\hlly{s = 1} \not = 2,3$
            \[a_n^{(p)}=\beta_0\hlly{1^n} = \beta_0\]
        \end{itemize}

        \subsubsection{Ejemplo de resolución de ecuaciones no homogéneas}
        \[a_n = 3a_{n-1} + \hlly{2^n} \quad \text{para}\ \hllc{n\geq 1} \quad \quad \text{con} \ a_0 = 0 \scriptstyle\text{ (cond. ini.)}\]

        \begin{enumerate}
            \item Solucionamos la ecuación característica $\scriptstyle\ r^k-c_1r^{k-1}-\dots-c_k = 0$
            \[ r^1 - 3 = 0\ \rightarrow\ \hllr{r = 3}\]
            \[\scriptstyle\text{Sabiendo la raíz, es un caso 1 de RRLnH ya que } r = 3 \not = \hlly{\scriptstyle 2}\]
            \item Componemos la ecuación homogénea asoc. (p) y obligamos a que $\hlly{\text{verifique la ec}}$.
            \[\boxed{a_n^{(p)} = \hlly{\beta2^n}} \ \xrightarrow{verifica}\ \hlly{a_n} = 3 \cdot\hlly{a_{n-1}} +2^n\quad \scriptstyle \text{manteniendo el orden de n, n-1, n-2...} \]
            \[\hlly{\beta2^n} = 3\cdot \hlly{\beta2^{n(-1)}}+2^n\ \xrightarrow{\hllc{\scriptstyle\text{asumo }n=1}}\ \beta2^1 = 3\cdot\beta2^0+2^1\ \rightarrow\ 2\beta = 3\beta + 2\ \rightarrow\ \hllc{\beta = -2}\]
            \[\boxed{a_n^{(p)}= \hllc{-2^{\color{gray}{1}}}\cdot 2^n = -2^{n+1}}\]
            \item Componemos la ecuación solución asociada (h) $\scriptstyle a_n = \alpha_1 r_1^n + \alpha_2 r_2^n + \dots + \alpha_kr_k^n$ 
            \[\boxed{a_n^{(h)} = \alpha_1 \hllr{3}^n}\]
            \item Componemos la RRLnHCC general $\scriptstyle a_n = a_n^{(h)}+a_n^{(p)}$ y sustituímos con las cond. ini.
            \[a_n = \boxed{\alpha_1 3^n} - \boxed{2^{n+1}} \ \xrightarrow{a_0 = 0}\ 0 = \alpha_1 3^0 - 2^1 \rightarrow \alpha_1 = 2\]
            \item Encontramos la ecuación solución sustituyendo
            \[a_n = 2\cdot 3^n - 2^{n+1}\]
        \end{enumerate}












    \newpage
    \section{Resolución de ecuaciones por Ruffini} \label{ruffini}
    Dada la ecuación:
    \[r^3 - 5r^2 + 8r - 4 = 0\]
    \begin{enumerate}
        \item Colocamos los coeficientes (constantes) en una línea y buscamos un número $(x)$ que sea divisor del término independiente, que pondremos a la izquierda.
        \item El primer coeficiente pasa a la parte inferior de la línea sin realizar ninguna operación.
        \item A continuación multiplicamos el número que hemos bajado por el $x$ de la izquierda y colocamos el resultado debajo del siguiente coeficiente
        \item Sumamos el número que acabamos de colocar el coeficiente que le corresponde (arriba) y colocamos el resultado debajo de la línea
        \item Repetimos estos pasos cuanto sea necesario. Si el último resultado da 0, $x$ es una solución (raíz) válida

        \begin{center}
            \polyhornerscheme[x=1]{x^3-5x^2+8x-4}
        \end{center}
    \end{enumerate}

    Con los números resultantes resuelvo la ecuación de segundo grado correspondiente
    \[x^2-4x+4 = 0 \qquad \text{da como solución}\qquad x = 2\]
    Una ecuación cúbica debe tener 3 soluciones, por lo que sabemos que $x = 2$ es doble.
    \[\quad \hllc{r_1 = 1}\ \text{ y }\ \hllc{r_2 = 2} \text{ (doble) }\]

    \newpage
    \section{Resumen}

    \hspace*{-20px}
    \begin{tikzpicture}
        [
            %auto,rotate=90,transform shape,
            level 1/.style = {black, sibling distance = 7.5cm},
            level 2/.style = {black, sibling distance = 4cm},
            level 4/.style = {black, sibling distance = 6cm}
        ]

        \node {Relaciones de recurrencia}
            child {node [] {RRLHCC}
                child {node {$r^k - c_1 r^{k-1} - c_2 r^{k-2} - \dots - c_k = 0$}
                    child {node {$a_n = \alpha_1 r_1^n + \alpha_2 r_2^n+\dots + \alpha_kr_k^n$}
                        child {node {Calc $\alpha$ sustuyendo cond. ini.}
                            child {node {Sustituir $\alpha$ en ec. sol.}}
                        }
                    }
                }
            }
            child {node [] {RRLnHCC}
                child {node {$a_n = a_n^{(h)} + a_n^{(p)}$}
                    child {node {$a_n^{(h)}$}}
                    child {node {$a_n^{(p)}$}
                        child {node {$caso 1: "r_n \not = s"$}
                            child{node {$a_n^{(p)} = (\beta_0+\dots+\beta_t n^t)\hlly{s^n}$}
                                child {node {solve $\beta$ asumiendo $n = n+1$}
                                    child {node {$a_n=a_n^{(h)}+a_n^{(p)}$}
                                        child {node {sustituir cond. ini.}}
                                    }
                                }
                            }
                        }
                        child {node {$caso 2: "r_n = s"$}
                            child {node {$a_n^{(p)} = (\beta_0+\dots+\beta_t n^t)\hlly{n^m s^n}$}
                                child {node {solve $\beta$ asumiendo $n = n+1$}
                                    child {node {$a_n=a_n^{(h)}+a_n^{(p)}$}
                                        child {node {sust cond. ini.}}
                                    }
                                }
                            }
                        }
                    }
                }
            };
        \draw (-0.7,-4.5) [dashed, -to] to  (1.2,-4.5);
        %\draw (2.75,-6) to  (2.75,-7);
    \end{tikzpicture}
\end{document}
