#include "monticulo.h"
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>

void inicializar_semilla(){
    srand(time(NULL));
}

void imprimir_array(int v[], int n){
    int i;
    printf("Array: [");
    for(i=0;i<n;i++){
        printf("%d ", v[i]);
    }
    printf("]\n");
}

void ascendente(int v[], int n) {
    int i;
    for (i = 0; i < n; i++) {
        v[i]=i;
    }
}

void descendente(int v[], int n) {
    int i;
    for (i = n-1; i >= 0; i--) {
        v[n-i-1]=i;
    }
}

void aleatorio(int v [], int n) {
    int i, m = 2*n + 1;
    for (i=0; i < n; i++) {
        v[i] = (rand() % m) - n;
    }
}


double microsegundos() {
    struct timeval t;
    if (gettimeofday(&t, NULL) < 0 )
        return 0.0;
    return (t.tv_usec + t.tv_sec * 1000000.0);
}

void printData(int n, double t, double w, double x, double y, double z, bool smallTime){
    printf("----------------------------------------------------"
           "---------------------------------------+----------------+\n");
    printf("%s       %12d |%15.3f |%15.6f |%15.6f |%15.6f |%15.7f |\n",
           smallTime?"(*)":"   " ,n, t, w, x, y, z);
}

void test(monticulo *m){
    int i;
    int v[14] = {34,59,84,46,81,29,8,7,19,37,53,14,67,83};

    /*Test crear_monticulo y eliminar_mayor*/
    printf("Insertando secuencia: 34,59,84,46,81,29,8,7,19,37,53,14,67,83\n");
    crear_monticulo(v, 14, m);
    imprimir_monticulo(*m);

    for(i=0;i<15;i++){
        printf("Eliminar mayor: (%d) \n", eliminar_mayor(m));
        imprimir_monticulo(*m);
        printf("\n");
    }

    /*Test ordenacion0 por monticulos*/
    aleatorio(v,10);
    printf("\nArray inicial: ");
    imprimir_array(v,10);

    ord_monticulo(v,10);

    printf("Array ordenado: ");
    imprimir_array(v,10);

}



void tiempoCrearMonticulo(monticulo *m, int v[]){
    int k, i, n;
    double ta, tb, t;
    bool smallTime;

    smallTime = false;
    k=10;
    n = 4000;
    ascendente(v,TAM);
    printf("\nTabla de timepos para crear monticulo:\n");
    printf("\n---      %13c | %14s | %14s | %14s | %14s | %14s |\n",
           'n',"t(n)","t(n)/f(n)","t(n)/g(n)","t(n)/h(n)","t(n)/n");
    while(n <= TAM){
        smallTime = false;
        ta = microsegundos();
        crear_monticulo(v, n, m);
        tb = microsegundos();

        t = tb - ta;

        if(t < 500){
            ta = microsegundos();
            for (i = 0; i < k; i++) {
                crear_monticulo(v, n, m);
            }
            tb = microsegundos();
            t = (tb - ta) / k;
            smallTime = true;
        }
        printData(n, t, t / pow(n,0.8), t / n, t / pow(n, 1.2), t/n, smallTime);
        n = n*2;
    }
    printf("----------------------------------------------------"
           "---------------------------------------+----------------+\n\n");
    printf("f(n) =  n^(0.8)\ng(n) = n\nh(n) = n^(1.2)\n\n");
}

void tiempo(void (*vector)(int v[], int n), int k, double f, double g, double h){
    int n;
    int i;
    double ta, tb, t1, t2, t;
    bool smallTime;
    int v[TAM];

    printf("\n---      %13c | %14s | %14s | %14s | %14s | %14s|\n",
           'n',"t(n)","t(n)/f(n)","t(n)/g(n)","t(n)/h(n)","t(n)/(n*log(n))");
    for(n = 4000; n <= TAM; n = n*2){
        vector(v, n);

        smallTime = false;
        ta = microsegundos();
        ord_monticulo(v,n);
        tb = microsegundos();
        t = (tb-ta);

        if(t < 500){
            ta = microsegundos();
            for (i = 0; i < k; i++) {
                vector(v, n);
                ord_monticulo(v,n);
            }
            tb = microsegundos();
            t1 = tb - ta;

            ta = microsegundos();
            for (i = 0; i < k; i++) {
                vector(v, n);
            }
            tb = microsegundos();
            t2 = tb - ta;

            t = (t1 - t2) / k;
            smallTime = true;
        }
        printData(n, t, t / pow(n,f), t / (pow(n,g)*log(n)), t / pow(n, h), t / (n*log(n)), smallTime);
    }
    printf("----------------------------------------------------"
           "---------------------------------------+----------------+\n\n");
    printf("f(n) = n^%.1f\ng(n) = n^%.2f*log(n)\nh(n) = n^%.1f\n\n",f,g,h);
}



int main() {
    int v[TAM];
    monticulo m;

    inicializar_semilla();
    test(&m);

    tiempoCrearMonticulo(&m,v);
    tiempoCrearMonticulo(&m,v);

    printf("\nOrdenación por montículos de vector ascendente: \n");
    tiempo(&ascendente,100,1,0.97,1.17);

    printf("\nOrdenación por montículos de vector descendente: \n");
    tiempo(&descendente,100,1,0.97,1.17);

    printf("\nOrdenación por montículos de vector aleatorio: \n");
    tiempo(&aleatorio,100,1,1,1.2);

    return 0;
}
