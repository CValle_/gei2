#define TAM 256000

typedef struct {
    int vector[TAM];
    int ultimo;
} monticulo;

void crear_monticulo(int [], int, monticulo*);
int eliminar_mayor(monticulo*);
void ord_monticulo(int V[], int n);
void imprimir_monticulo(monticulo m);
