#include "monticulo.h"
#include <stdio.h>


void static hundir (monticulo* m, int i) {
    int j, hijoIzq, hijoDer, aux;
    i = i+1;
    do{
        hijoDer = 2*i;
        hijoIzq = 2*i + 1;
        j = i;
        if (hijoDer - 1 <= m->ultimo && m->vector[hijoDer-1] > m->vector[i-1]){
            i = hijoDer;
        }
        if (hijoIzq - 1 <= m->ultimo && m->vector[hijoIzq-1] > m->vector[i-1]){
            i = hijoIzq;
        }
        aux = m->vector[j-1];
        m->vector[j-1] = m->vector[i-1];
        m->vector[i-1] = aux;
    }while(j-1 != i-1);
}

void crear_monticulo(int a[], int n, monticulo* m) {
    int i;
    m->ultimo=n-1;

    for (i=0;i<n;i++){
        m->vector[i]=a[i];
    }

    for (i = n/2; i > -1 ;i--){
        hundir(m,i);
    }
}


int eliminar_mayor(monticulo* m){
    int x;
    if(m->ultimo == -1){
        printf("Error: montículo vacío\n");
        return -1;
    } else {
        x = m->vector[0];
        m->vector[0] = m->vector[m->ultimo];
        m->ultimo --;
        if(m->ultimo > -1){
            hundir(m,0);
        }
        return x;
    }
}

void ord_monticulo(int V[], int n){
    monticulo M;
    int i;

    crear_monticulo(V,n,&M);
    for(i = n-1; i >= 0; i--){
        V[i] = eliminar_mayor(&M);
    }
}

void imprimir_monticulo(monticulo m){
    int i;
    printf("Monticulo: [ ");
    for(i=0;i<=m.ultimo;i++){
        printf("%d ", m.vector[i]);
    }
    printf("]\n");
}
