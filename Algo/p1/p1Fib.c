/*
 * TITLE: Algorithms
 * SUBTITLE: Practical 1
 * AUTHOR 1: Raúl Mouzo Quiza LOGIN 1: raul.mouzo@udc.es
 * AUTHOR 2: Clara Valle Gómez LOGIN 2: clara.valle@udc.es
 * GROUP: 2.2
 * DATE: 23/09/2022
 */


#include <stdio.h>
#include <sys/time.h>
#include <math.h>
#include <stdbool.h>


double microsegundos() {
  struct timeval t;
  if (gettimeofday(&t, NULL) < 0 )
    return 0.0;
  return (t.tv_usec + t.tv_sec * 1000000.0);
}

void printData(int n, double t, double x, double y, double z,bool smallTime){
  printf("----------------------------------------------------"
      "---------------------------------------+\n");
  printf("%s       %12d |%15.4f |%15.6f |%15.6f |%15.8f |\n",
          smallTime?"(*)":"   " ,n, t, x, y, z);
}


int fib1(int n){
    if (n < 2) {
        return n;
    } else {
        return fib1(n-1) + fib1(n-2);
    }
}
int fib2(int n){
    int i = 1;
    int j = 0;
    int k;

    for (k = 1; k <= n; k++) {
        j = i + j;
        i = j - i;
    }
    return j;
}

int fib3(int n){
    int i = 1, j=0, k=0, h=1, t=0;

    while (n > 0) {
        if (n % 2 != 0) {
            t = j*h;
            j = i*h + j*k + t;
            i = i*k + t;
        }
        t = h*h;
        h = 2*k*h + t;
        k = k*k + t;
        n = n/2;
    }
    return j;
}

void test(int n){
    int i;

    printf("\tnum | \tfib1(n) | \tfib2(n) | \tfib3(n)\n");
    printf("-------------------------------------------------------\n");
    for (i = 0; i < n; i++) {
        printf("%11d | %9d | %13d | %13d\n",i,fib1(i),fib2(i), fib3(i));
    }
}

void timeFib1 (){
    int n, k, i;
    double t1, t2, t;
    bool smallTime;
    k=100000;

    printf("\n--- fib1 %13c | %14s | %14s | %15s | %14s |\n",
        'n',"t(n)","t(n)/1.1^n","t(n)/φ^n","t(n)/2^n");
    for(n = 2; n <= 32; n = n*2){
        smallTime = false;
        t1 = microsegundos();
        fib1(n);
        t2 = microsegundos();
        t = (t2-t1);

        if(t < 500){
            t1 = microsegundos();
            for (i = 0; i < k; i++) {
                fib1(n);
            }
            t2 = microsegundos();
            t = (t2 - t1) / k;
            smallTime = true;
        }
        printData(n, t, t / pow(1.1, n), t / pow((1 + sqrt(5)) / 2,n),
            t / pow(2,n), smallTime);
    }
    printf("----------------------------------------------------"
        "---------------------------------------+\n\n");
}

void timeFib2 (){
    int n,k,i;
    double t1, t2, t;
    bool smallTime;
    k=1000;

    printf("\n--- fib2 %13c | %14s | %14s | %14s | %14s |\n",
        'n',"t(n)","t(n)/n^0.8","t(n)/n","t(n)/n*log(n)");

    for(n = 1000; n <= 10000000; n = n*10){
        smallTime = false;
        t1 = microsegundos();
        fib2(n);
        t2 = microsegundos();
        t = (t2-t1);

        if(t < 500){
            t1 = microsegundos();
            for (i = 0; i < k; i++) {
                fib2(n);
            }
            t2 = microsegundos();
            t = (t2 - t1)/k;
            smallTime = true;
        }
        printData(n, t, t / pow(n, 0.8), t / n, t / (n * log(n)), smallTime);
    }
    printf("----------------------------------------------------"
        "---------------------------------------+\n\n");
}


void timeFib3 (){
    int n, k, i;
    double t1, t2, t;
    bool smallTime;
    k=10000;

    printf("\n--- fib3 %13c | %14s | %14s | %14s | %14s |\n",
        'n',"t(n)","t(n)/n^0.5","t(n)/log(n)","t(n)/n^0.5");
    for(n = 1000; n <= 10000000; n = n*10){
        smallTime = false;
        t1 = microsegundos();
        fib3(n);
        t2 = microsegundos();
        t = (t2-t1);

        if(t< 500){
            t1 = microsegundos();
            for (i = 0; i < k; i++) {
                fib3(n);
            }
            t2 = microsegundos();
            t = (t2 - t1) / k;
            smallTime = true;
        }
        printData(n, t, t / sqrt(log(n)), t / log(n),
            t / pow(n, 0.5), smallTime);
    }
    printf("----------------------------------------------------"
        "---------------------------------------+\n\n");
}


int main() {
    int i;

    printf("---TEST:\n");
    test(10);

    for (i = 1; i <= 3; i++){
        printf("\n\t %d ITERACIÓN: \n", i);
        timeFib1();
        timeFib2();
        timeFib3();
    }

    return 0;
}
