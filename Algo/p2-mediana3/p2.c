/*
 * TITLE: Algorithms
 * SUBTITLE: Practical 2
 * AUTHOR 1: Clara Valle Gómez LOGIN 2: clara.valle@udc.es
 * GROUP: 2.2
 * DATE: 21/10/2022
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>

#define UMBRAL 1

double microsegundos() {
  struct timeval t;
  if (gettimeofday(&t, NULL) < 0 )
    return 0.0;
  return (t.tv_usec + t.tv_sec * 1000000.0);
}

void printData(int n, double t, double x, double y, double z,bool smallTime){
  printf("----------------------------------------------------"
      "---------------------------------------+\n");
  printf("%s       %12d |%15.3f |%15.6f |%15.6f |%15.7f |\n",
          smallTime?"(*)":"   " ,n, t, x, y, z);
}

void inicializar_semilla(){
    srand(time(NULL));
}

/* se generan números pseudoaleatorio entre -n y +n */
void aleatorio(int v [], int n) {
    int i, m = 2*n + 1;
    for (i=0; i < n; i++) {
        v[i] = (rand() % m) - n;
    }
}

void ascendente(int v[], int n) {
    int i;
    for (i = 0; i < n; i++) {
        v[i]=i;
    }
}

void descendente(int v[], int n) {
    int i;
    for (i = n-1; i >= 0; i--) {
        v[n-i-1]=i;
    }
}

bool ordenado(int v[], int n){
    int i;
    for(i=0;i < n-1; i++){
        if(v[i] > v[i+1])
        return false;
    }
  return true;
}

void ordenacionPorInsercion(int v[], int n){
    int i, j, x;

    for(i = 1; i < n; i++){x = v[i];
        j = i - 1;
        while(j >= 0 && v[j] > x){
            v[j + 1] = v[j];
            j = j - 1;
        }
        v[j + 1] = x;
    }
}

void print_array(int v[],int n){
    int i;
    for (i = 0; i < n; i++) {
        printf("%d%s ",v[i],i != n-1? ",":"");
    }
    printf("\n");
}

void printDataInsAleatoria(int n, double t, bool smallTime) {
    printData(n, t, t / pow(n, 1.8), t / pow(n, 2), t / pow(n, 2.2), smallTime);
}

void printDataInsDescendente(int n, double t, bool smallTime) {
    printData(n, t, t / pow(n, 1.8), t / pow(n, 2), t / pow(n, 2.2), smallTime);
}

void printDataInsAscendente(int n, double t, bool smallTime) {
    printData(n, t, t / pow(n, 0.8), t / n, t / pow(n, 1.2), smallTime);
}

void testInsercion(int n){
    int v[n];
    int i;

    for(i = 0;i < 3; i++){
        switch(i){
            case 0:
                printf("Ordenacion por insercion con inicializacion aleatoria\n");
                aleatorio(v, n);
                break;
            case 1:
                printf("Ordenacion por insercion con inicializacion ascendente\n");
                ascendente(v, n);
                break;
            case 2:
                printf("Ordenacion por insercion con inicializacion descendente\n");
                descendente(v, n);
                break;
        }
        print_array(v, n);
        printf("Ordenado? %s",ordenado(v, n)? "si\n" : "no\nordenando...\n");
        ordenacionPorInsercion(v, n);
        print_array(v, n);
        printf("%sesta ordenado\n",ordenado(v, n)? "": "no ");
        printf("\n");
    }
}

// ------------------------ QUICKSORT


void intercambiar(int v[], int pos1, int pos2) {
    int aux = v[pos1];

    v[pos1] = v[pos2];
    v[pos2] = aux;
}

void mediana3(int v[], int izq, int der) {
    int centro = (izq + der) / 2;
    if (v[izq] > v[centro]) {
        intercambiar(v, izq, centro);
    }
    if (v[izq] > v[der]) {
        intercambiar(v, izq, der);
    }
    if (v[centro] > v[der]) {
        intercambiar(v, centro, der);
    }
    intercambiar(v, centro, der - 1);
}

// ordenarAux using median of 3
void ordenarAux(int v[], int izq, int der) {
    int i, j, pivote;
    if (izq + UMBRAL <= der) {
        mediana3(v, izq, der);
        pivote = v[der - 1];
        i = izq;
        j = der - 1;
        while (true) {
            while (v[++i] < pivote) {
            }
            while (v[--j] > pivote) {
            }
            if (i < j) {
                intercambiar(v, i, j);
            } else {
                break;
            }
        }
        intercambiar(v, i, der - 1);
        ordenarAux(v, izq, i - 1);
        ordenarAux(v, i + 1, der);
    } else {
        ordenacionPorInsercion(v, der);
    }
}

// void ordenarAux(int v[], int izq, int der) {
//     int x, i, j, pivote;
//     if (izq + UMBRAL <= der) {
//         x = rand() % (der - izq + 1) + izq;
//         pivote = v[x];
//         intercambiar (v, izq, x);
//         i = izq + 1;
//         j = der;
//         while (i <= j) {
//             while (i <= der && v[i] < pivote) {
//                 i++;
//             }
//             while (v[j] > pivote) {
//                 j--;
//             }
//             if (i <= j) {
//                 intercambiar (v, i, j);
//                 i++;
//                 j--;
//             }
//         }

//         intercambiar (v, izq, j);
//         ordenarAux(v, izq, j - 1);
//         ordenarAux(v, j + 1, der);
//     }
// }

void ordenacionRapida (int v [], int n) {
    ordenarAux(v, 0, n-1);
    if (UMBRAL > 1) {
        ordenacionPorInsercion(v, n);
    }
}

void sortTime(int nTime, int k, void (*ordenacion)(int[], int), void (*inicializacion)(int[], int), void (*printData)(int, double, bool)){
    int n = 500;
    int i;
    double ta, tb, t1, t2, t;
    bool smallTime;
    int v[nTime];

    for(n = 500; n <= 32000; n = n*2){
        (*inicializacion)(v, n);

        smallTime = false;
        ta = microsegundos();
        (*ordenacion)(v, n);
        tb = microsegundos();
        t = (tb-ta);

        if(t < 500){
            ta = microsegundos();
            for (i = 0; i < k; i++) {
                (*inicializacion)(v, n);
                (*ordenacion)(v, n);
            }
            tb = microsegundos();
            t1 = tb - ta;

            ta = microsegundos();
            for (i = 0; i < k; i++) {
                (*inicializacion)(v, n);
            }
            tb = microsegundos();
            t2 = tb - ta;

            t = (t1 - t2) / k;
            smallTime = true;
        }
        (*printData)(n, t, smallTime);
    }
    
    printf("----------------------------------------------------"
        "---------------------------------------+\n\n");
}

void printDataRapidaAleatoria(int n, double t, bool smallTime) {
    switch (UMBRAL){
        case 1:
            printData(n, t, t / pow(n, 1.7), t / pow(n, 1.98), t / pow(n, 2.3), smallTime);
            break;
        case 10:
            printData(n, t, t / pow(n, 1.7), t / pow(n, 1.98), t / pow(n, 2.3), smallTime);
            break;
        case 100:
            printData(n, t, t / pow(n, 1.7), t / pow(n, 1.98), t / pow(n, 2.3), smallTime);
            break;
    }
}

void printDataRapidaAscendente(int n, double t, bool smallTime) {
    switch (UMBRAL){
        case 1:
            printData(n, t, t / (pow(n,1.7)*log(n)), t / (pow(n,1.9)*log(n)), t / (pow(n,2.1)*log(n)), smallTime);
            break;
        case 10:
            printData(n, t, t / (pow(n,1.7)*log(n)), t / (pow(n,1.9)*log(n)), t / (pow(n,2.1)*log(n)), smallTime);
            break;
        case 100:
            printData(n, t, t / (pow(n,1.7)*log(n)), t / (pow(n,1.9)*log(n)), t / (pow(n,2.1)*log(n)), smallTime);
            break;
    }
}

void printDataRapidaDescendente(int n, double t, bool smallTime) {
    switch (UMBRAL){
        case 1:
            printData(n, t, t / pow(n,1.7), t / pow(n, 2), t / pow(n,2.3), smallTime);
            break;
        case 10:
            printData(n, t, t / pow(n,1.7), t / pow(n, 2), t / pow(n, 2.3), smallTime);
            break;
        case 100:
            printData(n, t, t / pow(n,1.7), t / pow(n, 2), t / pow(n, 2.3), smallTime);
            break;
    }
}

void testQuicksort(int n){
    int v[n];
    int i;

    for(i = 0;i < 3; i++){
        switch(i){
            case 0:
                printf("Ordenacion rapida con inicializacion aleatoria\n");
                aleatorio(v, n);
                break;
            case 1:
                printf("Ordenacion rapida con inicializacion ascendente\n");
                ascendente(v, n);
                break;
            case 2:
                printf("Ordenacion rapida con inicializacion descendente\n");
                descendente(v, n);
                break;
        }
        print_array(v, n);
        printf("Ordenado? %s",ordenado(v, n)? "si\n" : "no\nordenando...\n");
        ordenacionRapida(v, n);
        print_array(v, n);
        printf("%sesta ordenado\n",ordenado(v, n)? "": "no ");
        printf("\n");
    }
}

void printHeader(char* ord, char* init) {
    printf("Ordenacion por %s con inicializacion %s\n", ord, init);
    printf("\n--- %s  %13c | %14s | %14s | %14s | %14s |\n",
        init,'n',"t(n)","t(n)/f(n)","t(n)/g(n)","t(n)/h(n)");
}

int main(){
    const int nTest = 10;
    const int nTime = 32000;

    inicializar_semilla();

    printf("\n------------MEDICIONES ORDENACION POR INSERCION------------ \n\n");
    testInsercion(nTest);

    printHeader("insercion", "ale");
    sortTime(nTime, 10, ordenacionPorInsercion, aleatorio, printDataInsAleatoria);;
    printf("f(n) = n^1.8\ng(n) = n^2\nh(n) = n^2.2\n\n");

    printHeader("insercion", "des");
    sortTime(nTime, 10, ordenacionPorInsercion, descendente, printDataInsDescendente);;
    printf("f(n) = n^1.8\ng(n) = n^2\nh(n) = n^2.2\n\n");

    printHeader("insercion", "asc");
    sortTime(nTime, 1000, ordenacionPorInsercion, ascendente, printDataInsAscendente);;
    printf("f(n) = n^0.8\ng(n) = n\nh(n) = n^1.2\n\n");


    // Mediciones Quicksort
    printf("\n------------MEDICIONES ORDENACION RAPIDA------------ \n");
    testQuicksort(nTest);

    printf("UMBRAL %d: \n", UMBRAL);

    printHeader("rapida", "ale");
    sortTime(nTime, 100, ordenacionRapida, aleatorio, printDataRapidaAleatoria);;
    
    printHeader("rapida", "asc");
    sortTime(nTime, 100, ordenacionRapida, ascendente, printDataRapidaAscendente);;
        
    printHeader("rapida", "des");
    sortTime(nTime, 100, ordenacionRapida, descendente, printDataRapidaDescendente);;
}
