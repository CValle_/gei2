//write this pseudocode 
procedimiento intercalar (var v[izq..der], centro)
    a := izq; b := centro; c:= 1;
    {se usa un vector auxiliar aux de longitud der - izq +1}
    mientras a < centro y b <= der hacer
        si (v[a] <= v[b]) entonces
            aux[c] := v[a]; a := a+1
        si no
            aux[c] := v[b]; b:= b+1
        fin si
        c := c+1;
    fin mientras

    mientras a < centro hacer
        aux[c] := v[a]; a:=a+1; c:=c+1
    fin mientras

    mientras b <= der hacer
        aux[c]:= v[b]; b:=b+1; c:=c+1
    fin mientras
    c:= 1
    para a:= izq hasta der hacer
        v[a] := aux[c]; c:=c+1
    fin para
fin procedimiento

void intercalar (int v[], int izq, int der, int centro){
    int a = izq, b = centro, c = 1;
    int aux[der - izq + 1];
    while (a < centro && b <= der){
        if (v[a] <= v[b]){
            aux[c] = v[a];
            a++;
        } else {
            aux[c] = v[b];
            b++;
        }
        c++;
    }
    while (a < centro){
        aux[c] = v[a];
        a++;
        c++;
    }
    while (b <= der){
        aux[c] = v[b];
        b++;
        c++;
    }
    c = 1;
    for (a = izq; a <= der; a++){
        v[a] = aux[c];
        c++;
    }
}


// write this pseudocode in c
procedimiento ordenarAux (var v[izq..der])
    si (der-izq) > 0 entonces
    centro:= (izq + der) div 2;
    ordenarAux (v[izq..centro]);
    ordenarAux (v[centro+1..der]);
    intercalar(v[izq..der], centro);
    fin si
fin procedimiemto

//TA REGULAR
void ordenarAux (int v[], int izq, int der){
    if (der-izq) > 0 then {
        centro:= (izq + der) div 2;
        ordenarAux (v[izq..centro]);
        ordenarAux (v[centro+1..der]);
        intercalar(v[izq..der], centro);
    }
}