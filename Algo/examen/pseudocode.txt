1 Implemente en C el algoritmo propuesto; realice la implemetación tomando como base el siguiente código

procedimiento ordenar(var v[1..n])
    ordenarAux(v[1..n])
fin procedimiento

procedimiento ordenarAux (var v[izq..der])
    si (der-izq) > 0 entonces
    centro:= (izq + der) div 2;
    ordenarAux (v[izq..centro]);
    ordenarAux (v[centro+1..der]);
    intercalar(v[izq..der], centro);
    finsi
fin procedimiemto

procedimiento intercalar (var v[izq..der], centro)
    a := izq; b := centro; c:= 1;
    {se usa un vector auxiliar aux de longitud der - izq +1}
    mientras a < centro y b <= der hacer
        si (v[a] <= v[b]) entonces
            aux[c] := v[a]; a := a+1
        si no
            aux[c] := v[b]; b:= b+1
        fin si
        c := c+1;
    fin mientras

    mientras a < centro hacer
        aux[c] := v[a]; a:=a+1; c:=c+1
    fin mientras

    mientras b <= der hacer
        aux[c]:= v[b]; b:=b+1; c:=c+1
    fin mientras
    c:= 1
    para a:= izq hasta der hacer
        v[a] := aux[c]; c:=c+1
    fin para
fin procedimiento


2 Tome como referencia el siguiente código C
void ordenar(int v[], int n){
    ordenarAux/(v, 0, n-1);
}