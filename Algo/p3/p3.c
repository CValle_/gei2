#include "abb.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>

#define MAX 256000

double microsegundos() {
    struct timeval t;
    if (gettimeofday(&t, NULL) < 0 )
        return 0.0;
    return (t.tv_usec + t.tv_sec * 1000000.0);
}

void inicializar_semilla(){
    srand(time(NULL));
}

void aleatorio(int v [], int n) {
    int i, m = 2*n + 1;
    for (i=0; i < n; i++) {
        v[i] = (rand() % m) - n;
    }
}

void printData(int n, double t, double x, double y, double z,bool smallTime){



  printf("----------------------------------------------------"
      "---------------------------------------+\n");
  printf("%s       %12d |%15.3f |%15.6f |%15.6f |%15.7f |\n",
          smallTime?"(*)":"   " ,n, t, x, y, z);
}


void test(){
    int i, randNumber;
    arbol a;
    posicion p;

    inicializar_semilla();
    a = creararbol();

    printf("\nTEST:\n");
    printf("arbol vacio: ");
    visualizar(a);
    printf(".\n");

    printf("altura del arbol: %d\n",altura(a));

    for(i = 0;i < 6;i++){
        randNumber = rand()%6;
        printf("inserto un %d\n",randNumber);
        a = insertar(randNumber, a);
    }

    printf("arbol: ");
    visualizar(a);
    printf(".\n");

    printf("altura del arbol: %d\n",altura(a));

    for(i = 0;i < 7;i++){
        p = buscar(i, a);
        printf("busco %d y ",i);
        if(p == NULL){
            printf("no encuentro nada\n");
        } else {
            printf("encuentro %d repetido: %d veces\n", i, numerorepeticiones(p));
        }
    }

    printf("borro todos nodos liberando la memoria:\n");
    a = eliminararbol(a);

    printf("arbol vacio:");
    visualizar(a);
    printf(".\n");

    printf("altura del arbol: %d\n",altura(a));
}

void timeInsercion(int n, arbol a){
    int k, i, j;
    double ta, tb, t1, t2, t;
    bool smallTime;
    int v[MAX];

    smallTime = false;
    k=10;

    while(n <= MAX){
        aleatorio(v, n);
        ta = microsegundos();
        for(i = 0;i < n; i++){
            a = insertar(v[i],a);
        }
        tb = microsegundos();
        t = tb - ta;
        a = eliminararbol(a);
        smallTime = false;
        if(t < 500){
            ta = microsegundos();
            for (j = 0; j < k; j++) {
                for(i = 0;i < n; i++){
                    a = insertar(v[i],a);
                }
                a = eliminararbol(a);
            }
            tb = microsegundos();
            t1 = tb - ta;
            aleatorio(v, n*k);
            for(i = 0;i < n*k; i++){
                a = insertar(v[i],a);
            }
            ta = microsegundos();
            a = eliminararbol(a);
            tb = microsegundos();
            t2 = tb - ta;
            t = (t1 - t2) / k;
            smallTime = true;
        }
        printData(n, t, t / pow(n, 1.08), t / pow(n, 1.28), t / pow(n, 1.48), smallTime);
        n = n*2;
    }
}

void timeBuscar(int n, arbol a){
    int k, i, j;
    double ta, tb, t;
    bool smallTime;
    int v[MAX];

    smallTime = false;
    k=10;

    while(n <= MAX){
        aleatorio(v, n);
        for(i = 0;i < n; i++){
            a = insertar(v[i],a);
        }
        aleatorio(v, n);
        ta = microsegundos();
        for(i = 0;i < n; i++){
            buscar(v[i], a);
        }
        tb = microsegundos();
        t = tb - ta;

        smallTime = false;
        if(t < 500){
            ta = microsegundos();
            for (j = 0; j < k; j++) {
                for(i = 0;i < n; i++){
                    buscar(v[i], a);
                }
            }
            tb = microsegundos();
            t = (tb - ta) / k;
            smallTime = true;
        }
        printData(n, t, t / pow(n, 1.05), t / pow(n, 1.25), t / pow(n, 1.45), smallTime);
        n = n*2;
        a = eliminararbol(a);
    }
}


void print_data(int n){
    arbol a;
    a = creararbol();

    printf("Insercion de n elementos\n");
    printf("\n--- ins  %13c | %14s | %14s | %14s | %14s |\n",
        'n',"t(n)","t(n)/f(n)","t(n)/g(n)","t(n)/h(n)");

    timeInsercion(n,a);

    printf("----------------------------------------------------"
        "---------------------------------------+\n\n");
    printf("f(n) =  n^(1.05)\ng(n) = n^(1.25)\nh(n) = n^(1.45)\n\n");

    printf("Busqueda de n elementos\n");
    printf("\n--- bus  %13c | %14s | %14s | %14s | %14s |\n",
        'n',"t(n)","t(n)/f(n)","t(n)/g(n)","t(n)/h(n)");

    timeBuscar(n, a);

    printf("----------------------------------------------------"
        "---------------------------------------+\n\n");
    printf("f(n) =  n^(1.06)\ng(n) = n^(1.26)\nh(n) = n^(1.46)\n\n");

}

int main(){
    int n = 1000;
    test();
    print_data(n);
    print_data(n);
    print_data(n);
    print_data(n);

    return 0;
}
