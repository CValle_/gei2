#include "abb.h"

static struct nodo *crearnodo(int e) {
    struct nodo *p = malloc(sizeof(struct nodo));
    if (p == NULL) {
        printf("memoria agotada\n"); exit(EXIT_FAILURE);
    }
    p->elem = e;
    p->num_repeticiones = 1;
    p->izq = NULL;
    p->der = NULL;
    return p;
}

arbol insertar(int e, arbol a){
    if (a == NULL)
        return crearnodo(e);
    else if (e < a->elem)
        a->izq = insertar(e, a->izq);
    else if (e > a->elem)
        a->der = insertar(e, a->der);
    else
        a->num_repeticiones++;
    return a;
}

arbol creararbol() {
    arbol a = NULL;
    return a;
}

int esarbolvacio(arbol a) {
    return a == NULL;
}

posicion buscar(int key, arbol a){
    if (a == NULL){
        return NULL;
    } else if (key == a->elem){
        return a;
    } else if(key < a->elem){
        return buscar(key,a->izq);
    } else {
        return buscar(key,a->der);
    }
}

arbol eliminararbol(arbol a){
    if(a == NULL){
        return NULL;
    }
    eliminararbol(a->izq);
    eliminararbol(a->der);

    free(a);

    return NULL;
}

posicion hijoizquierdo(arbol a) {
    return (a->izq);
}

posicion hijoderecho(arbol a) {
    return (a->der);
}

int elemento(posicion p) {
    return (p->elem);
}

int numerorepeticiones(posicion p) {
    return (p->num_repeticiones);
}

int altura(arbol a){
    int auxIzq, auxDer;
    if(esarbolvacio(a)){
        return -1;
    } else {
        auxIzq = altura(a->izq) + 1;
        auxDer = altura(a->der) + 1;
        if(auxIzq > auxDer){
            return auxIzq;
        } else{
            return auxDer;
        }
    }
}


void visualizar(arbol a){
    if (!esarbolvacio(a)){
        if(a->izq != NULL){
            printf("(");
            visualizar(a->izq);
            printf(")");
        }

        printf(" %d ", a->elem);

        if(a->der != NULL){
            printf("(");
            visualizar(a->der);
            printf(")");
        }
    } else {
        printf("()");
    }
}
