package e4;

public enum MovieRating {
    NOT_RATED(-1),
    AWFUL(0),
    BAD(2),
    MEDIOCRE(4),
    GOOD(6),
    EXCELLENT(8),
    MASTERPIECE(10);

    private final int rate;

    MovieRating(int rate) {
        this.rate = rate;
    }
    int getNumericRating(){
        return this.rate;
    }

    public boolean isBetterThan(MovieRating movieRating){
        return this.rate > movieRating.getNumericRating();
    }
}
