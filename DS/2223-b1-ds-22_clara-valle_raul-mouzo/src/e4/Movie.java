package e4;

import java.util.ArrayList;

public class Movie {
    private final String title;

    ArrayList<MovieRating> movieList;
    public Movie(String title){
        this.title = title;
        this.movieList = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void insertRating(MovieRating movieRating) {
        movieList.add(movieRating);
    }

    private boolean isRated() {

        if (movieList.size() == 0) {
            return false;
        } else {
            for (int i = 0; i < movieList.size(); i++){
                if(movieList.get(i) != MovieRating.NOT_RATED){
                    return true;
                }
            }
        }
        return false;
    }

    public MovieRating maximumRating() {
        int max, i;
        MovieRating rate, aux;

        rate = MovieRating.NOT_RATED;
        max = MovieRating.NOT_RATED.getNumericRating();

        for (i = 0; i < movieList.size(); i++) {
            aux = movieList.get(i);
            if (aux.getNumericRating() > max) {
                max = aux.getNumericRating();
                rate = aux;
            }
        }
        return rate;
    }

    public double averageRating () {
        int i, count;
        double avg = 0;

        count = 0;

        for (i = 0; i < movieList.size(); i++) {
            if(movieList.get(i) != MovieRating.NOT_RATED){
                avg += movieList.get(i).getNumericRating();
                count++;
            }
        }

        if(count == 0) throw new java.util.NoSuchElementException();

        return avg/count;
    }
}
