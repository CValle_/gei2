package e2;

import java.util.Arrays;

public class SocialDistance {
    public static char[][] seatingPeople(char[][] layout) {
        char [][] checkingSites = null;
        int peopleAround;

        if(layout == null || !checkLayout(layout)) {
            throw new IllegalArgumentException();
        }

        while (!Arrays.deepEquals(checkingSites, layout)){

            checkingSites = copy2DArray(layout);

            //Comprueba si se puede sentar
            for (int y = 0; y < checkingSites.length; y++){
                for(int x = 0; x < checkingSites[y].length; x++){
                    peopleAround = getOccupiedSites(checkingSites, x , y);
                    if(peopleAround == 0 && checkingSites[y][x] == 'A'){
                        //se sienta
                        layout[y][x] = '#';
                    }
                }
            }

            checkingSites = copy2DArray(layout);

            //Comprueba si se va a levantar
            for (int y = 0; y < checkingSites.length; y++){
                for(int x = 0; x < checkingSites[y].length; x++){
                    peopleAround = getOccupiedSites(checkingSites, x , y);
                    if(peopleAround >= 4 && checkingSites[y][x] == '#'){
                        //se levanta
                        layout[y][x] = 'A';
                    }
                }
            }
        }
        return layout;
    }

    public static int getOccupiedSites(char [][] cS, int x, int y){
        int count = 0;
        int rows = cS.length;
        int cols = cS[0].length;

        if(y > 0  && x > 0 && cS[y-1][x-1] == '#'){ count++; }

        if(y > 0 && cS[y-1][x] == '#') { count++; }

        if(y > 0 && x < cols - 1 && cS[y-1][x+1] == '#') { count++; }

        if( x > 0 && cS[y][x-1] == '#') { count++; }

        if( x < cols - 1 && cS[y][x+1] == '#') { count++; }

        if( y < rows - 1 && x > 0 && cS[y+1][x-1] == '#') { count++; }

        if( y < rows - 1 && cS[y+1][x] == '#') { count++; }

        if( y < rows - 1 && x < cols - 1 && cS[y+1][x+1] == '#') { count++; }

        return count;
    }

    public static boolean checkLayout(char[][] arr){
        int i, j, rows, cols;

        rows = arr.length;
        cols = arr[0].length;

        for(i=0;i < rows;i++){
            if(arr[i].length != cols){
                return false;
            }
            for(j=0;j < arr[i].length;j++){
                if(arr[i][j] != 'A' && arr[i][j] != '#' && arr[i][j] != '.' ){
                    return false;
                }
            }
        }
        return true;
    }

    public static char[][] copy2DArray(char[][] arr){
        char[][] newArr = new char[arr.length][];
        for (int y = 0; y < arr.length; y++){
            newArr[y] = arr[y].clone();
        }
        return newArr;
    }

}
