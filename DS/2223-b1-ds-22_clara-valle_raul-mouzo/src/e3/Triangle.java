package e3;

import java.util.Arrays;
import java.util.Objects;

public record Triangle (int angle0, int angle1, int angle2) {

    public Triangle(int angle0, int angle1, int angle2) {
        this.angle0 = angle0;
        this.angle1 = angle1;
        this.angle2 = angle2;
        if(angle0 <= 0 || angle1 <= 0 || angle2 <= 0 || angle0 + angle1 + angle2 != 180 ){
            throw new IllegalArgumentException();
        }
    }
    public Triangle(Triangle t) {
        this(t.angle0, t.angle1, t.angle2);
    }

    public boolean isRight(){
        return angle0 == 90 || angle1 == 90 || angle2 == 90;
    }

    public boolean isAcute(){
        return angle0 < 90 && angle1 < 90 && angle2 < 90;
    }

    public boolean isObtuse(){
        return angle0 > 90 || angle1 > 90 || angle2 > 90;
    }

    public boolean isEquilateral(){
        return angle0 == angle1 && angle1 == angle2;
    }

    public boolean isIsosceles (){
        return (angle0 == angle1 && angle1 != angle2) || (angle1 == angle2 && angle1 != angle0);
    }

    public boolean isScalene () {
        return angle0 != angle1 && angle1 != angle2;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (this.getClass() != obj.getClass()) return false;

        var that = (Triangle) obj;

        int[] angles0 = { this.angle0, this.angle1, this.angle2 };
        Arrays.sort(angles0);

        int[] angles1 = { that.angle0, that.angle1, that.angle2 };
        Arrays.sort(angles1);

        return angles0[0] == angles1[0] &&
                angles0[1] == angles1[1] &&
                angles0[2] == angles1[2];
    }

    @Override
    public int hashCode() {
        int[] angles = { this.angle0, this.angle1, this.angle2 };
        Arrays.sort(angles);
        return Objects.hash(angles[0],angles[1],angles[2]);
    }


}
