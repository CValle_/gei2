package e1;

public class DateUtilities {

    public static boolean isLeap(int year){
        if(year == 0){
            throw new IllegalArgumentException();
        } else {
            return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
        }
    }

    public static int numberOfDays(int month, int year){
        switch (month) {
            case 1, 3, 5, 7, 8, 10, 12 -> {
                return 31;
            }
            case 4, 6, 9, 11 -> {
                return 30;
            }
            case 2 -> {
                if(isLeap(year)){
                    return 29;
                } else{
                    return 28;
                }
            }
            default ->
                throw new IllegalArgumentException(Integer.toString(month));
        }
    }
    public static String convertToISODate(String string){
        String month="0";
        String[] parts = string.split("[ ,]");
        switch (parts[0]) {
            case "January" -> month = "01";
            case "February" -> month = "02";
            case "March" -> month = "03";
            case "April" -> month = "04";
            case "May" -> month = "05";
            case "June" -> month = "06";
            case "July" -> month = "07";
            case "August" -> month = "08";
            case "September" -> month = "09";
            case "October" -> month = "10";
            case "November" -> month = "11";
            case "December" -> month = "12";
        }
        return String.format("%s-%s-%s",parts[3],month,parts[1]);
    }
    public static boolean checkISODate(String iSODate) {
        int ano, mes, dia;
        String[] parts = iSODate.split("-");

        if (parts.length != 3){
            return false;
        }

        try {
            ano = Integer.parseInt(parts[0]);
            mes = Integer.parseInt(parts[1]);
            dia = Integer.parseInt(parts[2]);
        } catch (NumberFormatException nfe){
            return false;
        }

        return ano > 0 && mes > 0 && mes <= 12 && dia > 0 && dia <= numberOfDays(mes, ano);
    }
}
