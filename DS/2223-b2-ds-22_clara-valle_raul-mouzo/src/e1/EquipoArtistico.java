package e1;

public abstract class EquipoArtistico extends EquipoHumano {
    public EquipoArtistico(String name, String surname, String dni, int tlf, String nat, float hours) {
        super(name, surname, dni, tlf, nat, hours);
    }
}
