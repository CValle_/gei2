package e1;

public abstract class EquipoTecnico extends EquipoHumano {
    private int percent;

    private String simpleCategory;

    public EquipoTecnico(String name, String surname, String dni, int tlf, String nat, float hours) {
        super(name, surname, dni, tlf, nat, hours);
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public void setSimpleCategory(String simpleCategory) {
        this.simpleCategory = simpleCategory;
    }

    public float calcRoyalties(float filmIncome) {
        return (this.percent * filmIncome) / 100;
    }
    public String getRoyaltiesString () {
        return this.getFullName() + " (" + this.simpleCategory + "): ";
    }
}
