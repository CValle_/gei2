package e1.EquipoTecnicoP;

import e1.EquipoTecnico;

public class Guionista extends EquipoTecnico {
    private final int hourlyRate = 70;
    private final int royaltiePercent = 5;
    private final int complement;
    private final float hours;

    boolean originalScript;


    public Guionista(String name, String surname, String dni, int tlf, String nat, float hours, boolean originalScript) {
        super(name, surname, dni, tlf, nat, hours);

        this.hours = hours;
        String category = "Screenwriter";

        //Print salaries
        this.originalScript = originalScript;
        complement = (originalScript) ? 4000 : 0 ;

        this.setCategoryString(category + (originalScript ? ", original screenplay" : ""));

        //Print royalties
        this.setPercent(royaltiePercent);
        this.setSimpleCategory(category);
    }

    public float calcSalary() {
        return hourlyRate * hours + complement;
    }
}

