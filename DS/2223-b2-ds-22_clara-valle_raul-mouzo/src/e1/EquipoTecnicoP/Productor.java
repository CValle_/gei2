package e1.EquipoTecnicoP;

import e1.EquipoTecnico;

public class Productor extends EquipoTecnico {

    private final int hourlyRate = 90;
    private final int royaltiePercent = 2;
    private final String category = "Producer";
    private final float hours;

    public Productor(String name, String surname, String dni, int tlf, String nat, float hours) {
        super(name, surname, dni, tlf, nat, hours);

        this.hours = hours;

        //Print salaries
        this.setCategoryString(category);

        //Print royalties
        this.setPercent(royaltiePercent);
        this.setSimpleCategory(category);
    }

    public float calcSalary() {return hourlyRate * hours;}
}
