package e1.EquipoTecnicoP;

import e1.EquipoTecnico;

public class Musico extends EquipoTecnico {

    private final int hourlyRate = 60;
    private final int royaltiePercent = 4;
    private final String category = "Musician";
    private final float hours;

    public Musico(String name, String surname, String dni, int tlf, String nat, float hours) {
        super(name, surname, dni, tlf, nat, hours);

        this.hours = hours;

        //Print salaries
        this.setCategoryString(category);

        //Print royalties
        this.setPercent(royaltiePercent);
        this.setSimpleCategory(category);

    }

    public float calcSalary() {return hourlyRate * hours;}
}
