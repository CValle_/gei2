package e1.EquipoTecnicoP;

import e1.EquipoTecnico;

public class Director extends EquipoTecnico {
    private final int hourlyRate = 100;
    private final int royaltiePercent = 5;
    private final int complement = 1000;
    private final float hours;
    private int experienceYears;

    public Director(String name, String surname, String dni, int tlf, String nat, float hours, int experienceYears) {
        super(name, surname, dni, tlf, nat, hours);

        this.hours = hours;
        this.experienceYears = experienceYears;

        //Print salaries
        String category = "Director";
        this.setCategoryString(category + ", " + experienceYears + " years of experience");

        //Print royalties
        this.setPercent(royaltiePercent);
        this.setSimpleCategory(category);
    }

    public float calcSalary() {
        return (hourlyRate * hours + experienceYears * complement);
    }
}
