package e1.EquipoArtisticoP;

import e1.EquipoArtistico;

public class Especialista extends EquipoArtistico {
    private final int hourlyRate = 40;
    private final int complement;
    private final float hours;
    private final boolean extraDanger;


    public Especialista(String name, String surname, String dni, int tlf, String nat, float hours, boolean extraDanger) {
        super(name, surname, dni, tlf, nat, hours);

        this.extraDanger = extraDanger;
        complement = (extraDanger) ? 1000 : 0 ;

        this.hours = hours;

        //Print salaries
        this.setCategoryString("Strunt performer" + (this.extraDanger ? " with extra for danger" : ""));
    }

    public float calcSalary() {
        return (hourlyRate * hours) + complement;
    }
}
