package e1.EquipoArtisticoP;

import e1.EquipoArtistico;
import e1.Rol;

public class Interprete extends EquipoArtistico {
    private final int hourlyRate = 200;
    private final float hours;
    private final Rol rol;


    public Interprete(String name, String surname, String dni, int tlf, String nat, float hours, Rol rol) {
        super(name, surname, dni, tlf, nat, hours);

        this.hours = hours;

        //Print salaries
        this.rol = rol;
        this.setCategoryString("Actor " + this.rol.toString().toLowerCase());
    }

    public float calcSalary() {
        return (hourlyRate * hours * rol.getMultiplier());
    }
}
