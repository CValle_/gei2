package e1.EquipoArtisticoP;

import e1.EquipoArtistico;

public class Doblador extends EquipoArtistico {
    private final int hourlyRate = 20;
    private final float hours;

    public Doblador(String name, String surname, String dni, int tlf, String nat, float hours) {
        super(name, surname, dni, tlf, nat, hours);

        this.hours = hours;


        //Print salaries
        this.setCategoryString("Dubber");
    }
    public float calcSalary() {
        return hourlyRate * hours;
    }
}
