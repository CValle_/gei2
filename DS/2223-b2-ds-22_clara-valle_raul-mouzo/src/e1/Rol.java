package e1;

public enum Rol {
    PROTAGONIST (3),
    SECONDARY (1),
    EXTRA (1);

    private final int multiplier;

    Rol(int multiplier) {
        this.multiplier = multiplier;
    }
    public int getMultiplier(){
        return this.multiplier;
    }
}
