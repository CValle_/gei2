package e1;

public abstract class EquipoHumano {
    private final String name;
    private final String surname;
    private final String dni;
    private final int tlf;
    private final String nat;
    private final float hours;

    private String categoryString;

    public EquipoHumano(String name, String surname, String dni, int tlf, String nat, float hours) {
        this.name = name;
        this.surname = surname;
        this.dni = dni;
        this.tlf = tlf;
        this.nat = nat;
        this.hours = hours;
    }
    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public void setCategoryString(String categoryString) {
        this.categoryString = categoryString;
    }

    public abstract float calcSalary();

    public String getFullName () {
        return this.getName() + " " + this.getSurname();
    }

    public String getSalaryString () {
        return getFullName() + " (" + this.categoryString + "): " + this.calcSalary() + " euro";
    }
}
