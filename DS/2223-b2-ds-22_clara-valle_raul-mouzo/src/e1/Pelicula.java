package e1;

import java.util.ArrayList;

public class Pelicula {
    private final ArrayList<EquipoHumano> humanList;
    private final String name;
    private final float filmIncome;

    public Pelicula(ArrayList<EquipoHumano> humanList, String name, float filmIncome) {
        this.humanList = humanList;
        this.name = name;
        this.filmIncome = filmIncome;
    }

    public String printSalaries () {
        int i;
        float totalSalaries = 0;
        StringBuilder salaries = new StringBuilder();

        for (i = 0; i < humanList.size(); i++){
            totalSalaries = totalSalaries + humanList.get(i).calcSalary();
            salaries.append(humanList.get(i).getSalaryString()).append("\n");
        }
        salaries.append("The total payroll for ").append(name).append(" is ").append(totalSalaries).append(" euro");
        return String.valueOf(salaries);
    }

    public String printRoyalties () {
        StringBuilder royalties = new StringBuilder();
        int i;
        for (i = 0; i < humanList.size(); i++){
            if (humanList.get(i) instanceof EquipoTecnico tecnico) {
                royalties.append(tecnico.getRoyaltiesString()).append(tecnico.calcRoyalties(filmIncome)).append(" euro");
                if (i != humanList.size() - 1 ){royalties.append("\n");}
            }
        }
        return String.valueOf(royalties);
    }
}
