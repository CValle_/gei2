package e2;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class RecorridoCircular<E> implements Iterator<E> {
    private int current = -1;
    private boolean illegalRemove = true;
    public ArrayList<E> elements; //los elementos del iterador son los candidates

    public RecorridoCircular(ArrayList<E> elements) {
        this.elements = elements;
    }

    @Override
    public boolean hasNext() {
        return elements.size() > 1;
    }

    @Override
    public E next() {
        illegalRemove = false;
        if (elements.size() == 0){
            throw new NoSuchElementException();
        } else if (current == -1 || current == elements.size() - 1) {
            current = 0;
        } else {
            current++;
        }
        return elements.get(current);
    }

    @Override
    public void remove() {
        if (current == -1 || illegalRemove) {
            throw new IllegalStateException();
        }else if (current == 0) {
            elements.remove(current);
            current = elements.size() -1;
        } else if (current == elements.size() - 1) {
            elements.remove(current);
            current = -1;
        } else {
            elements.remove(current);
            current--;
        }
        illegalRemove = true;
    }
}
