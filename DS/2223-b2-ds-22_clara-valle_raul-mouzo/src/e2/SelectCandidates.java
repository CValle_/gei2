package e2;

import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;

public class SelectCandidates {

    //Caso para los test
     public static String selectCandidates(TVRealityList<String> tvr, int k) {
        Iterator<String> iter = tvr.iterator();
        while(iter.hasNext()){
            for (int i = 0; i < k; i++){
                iter.next();
            }
            iter.remove();
        }
        return tvr.candidates.get(0);
    }

    //Caso para una k realmente aleatoria
    public static String selectCandidates(TVRealityList tvr){
        return selectCandidates(tvr,ThreadLocalRandom.current().nextInt(0,tvr.candidates.size() + 1));
    }
}
