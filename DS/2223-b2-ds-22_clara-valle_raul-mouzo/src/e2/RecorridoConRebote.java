package e2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class RecorridoConRebote<E> implements Iterator<E> {
    private int current = -1;
    private boolean moveForward = true;
    private boolean illegalRemove = true;
    public ArrayList<E> elements;
    public RecorridoConRebote(ArrayList<E> elements) {
        this.elements = elements;
    }

    @Override
    public boolean hasNext() {
        return elements.size() > 1;
    }

    @Override
    public E next() {
        illegalRemove = false;
        if (elements.size() == 0){
            throw new NoSuchElementException();
        } else if (current == -1) {
            moveForward = true;
            current = 0;
        } else if (current == 0) {
            moveForward = true;
            current ++;
        } else if (current == elements.size() - 1) {
            moveForward = false;
            current --;
        } else if (current == elements.size()) {
            current --;
        } else {
            if (moveForward) {
                current++;
            } else {
                current--;
            }

        }
        return elements.get(current);
    }

    @Override
    public void remove() {
        if (current == -1 || illegalRemove) {
            throw new IllegalStateException();
        }else if (current == 0) {
            elements.remove(current);
            current = -1;
        } else if (current == elements.size() - 1) {
            elements.remove(current);
            current = elements.size();
        } else {
            elements.remove(current);
            if (moveForward) {
                current--;
            } else if (current + 1 < elements.size()) {
                current++;
            }
        }
        illegalRemove = true;
    }
}
