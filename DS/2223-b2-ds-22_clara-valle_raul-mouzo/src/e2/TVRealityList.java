package e2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;

public class TVRealityList<String> implements Iterable<String>{
    final ArrayList<String> candidates;
    private final boolean recSelect;

    public TVRealityList(ArrayList<String> candidates, boolean recSelect) {
        this.candidates = candidates;
        this.recSelect = recSelect;
    }

    @Override
    public Iterator<String> iterator() {
        if (recSelect) {
            return new RecorridoCircular<>(candidates);
        } else {
            return new RecorridoConRebote<>(candidates);
        }
    }
}
