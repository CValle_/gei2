package e3.MfaStrategies;

import e3.MfaStrategy;

import java.util.UUID;

public class EmailCodeMfa implements MfaStrategy {

    //Este método genera un código alfanumérico de 8 dígitos que podría ser enviado por mail
    @Override
    public String generateString() {
        int n = 8;
        StringBuilder codeString = new StringBuilder();
        String randomString = UUID.randomUUID().toString();
        char indexC;

        for (int k = 0; k < randomString.length(); k++) {
            indexC = randomString.charAt(k);

            if (((indexC >= 'a' && indexC <= 'z')
                    || (indexC >= 'A' && indexC <= 'Z')
                    || (indexC >= '0' && indexC <= '9'))
                    && (n > 0)) {

                codeString.append(indexC);
                n--;
            }
        }
        return codeString.toString();
    }
}
