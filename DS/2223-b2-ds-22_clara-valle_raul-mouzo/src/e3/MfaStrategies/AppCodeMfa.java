package e3.MfaStrategies;

import e3.MfaStrategy;

import java.util.Random;

public class AppCodeMfa implements MfaStrategy {

    //Este método genera un int de 6 dígitos que podría ser enviado a una app
    @Override
    public String generateString() {
        Random rnd = new Random();
        int codeInt = 100000 + rnd.nextInt(900000);
        return Integer.toString(codeInt);
    }
}
