package e3.MfaStrategies;

import e3.MfaStrategy;

public class SmsCodeMfa implements MfaStrategy {

    @Override
    public String generateString() {
        //Genera un código de 4 dígitos que podría ser enviado por SMS
        int n = 4;
        String numericString = "0123456789";
        StringBuilder codeString = new StringBuilder(n);
        int index;

        for (int i = 0; i < n; i++) {
            index = (int)(numericString.length() * Math.random());

            codeString.append(numericString.charAt(index));
        }
        return codeString.toString();
    }
}
