package e3;

import java.util.Map;

public class LoginScreen implements PassAccess{
    private final Map<User, String> userMap;
    LoginStrategy loginStrategy;

    MfaStrategy mfaStrategy;

    public LoginScreen(Map<User, String> userMap) {
        this.userMap = userMap;
    }

    public void setLoginStrategy(LoginStrategy loginStrategy) {
        this.loginStrategy = loginStrategy;
    }
    public void setMfaStrategy(MfaStrategy mfaStrategy) {
        this.mfaStrategy = mfaStrategy;
    }



    @Override
    public Map<User, String> getMapPass() {
        return userMap;
    }
}