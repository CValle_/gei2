package e3;

import java.util.Map;

//Esta interfaz es útil para no permitir el acceso a UserLogin, EmailLogin y PhoneNumLogin a todos los métodos de LoginScreen
public interface PassAccess {
    Map<User, String> getMapPass();
}
