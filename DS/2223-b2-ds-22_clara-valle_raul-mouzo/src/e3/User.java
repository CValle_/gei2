package e3;

public class User {
    public String name, email, username;
    public int phoneNum;

    public User(String name, String email, String username, int phoneNum) {
        this.name = name;
        this.email = email;
        this.username = username;
        this.phoneNum = phoneNum;
    }
}
