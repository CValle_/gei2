package e3.LoginStrategies;

import e3.LoginStrategy;
import e3.PassAccess;
import e3.User;

import java.util.Map;
import java.util.regex.*;

public class UserLogin implements LoginStrategy {
    private final PassAccess passAccess;
    public UserLogin(PassAccess passAccess) {
        this.passAccess = passAccess;
    }

    @Override
    public boolean validateId(String id) {
        if (id == null) {
            return false;
        } else {
            String regex = "^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$";
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(id);
            return m.matches();
        }
    }

    @Override
    public boolean authenticatePassword(String id, String password) {
        Map<User, String> userMap = passAccess.getMapPass();
        for (User user:userMap.keySet()) {
            if(user.username.equals(id) && userMap.get(user).equals(password)){
                return true;
            }
        }
        return false;
    }
}
