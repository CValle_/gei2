package e3.LoginStrategies;

import e3.LoginStrategy;
import e3.PassAccess;
import e3.User;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumLogin implements LoginStrategy {

    private final PassAccess passAccess;

    public PhoneNumLogin(PassAccess passAccess) {
        this.passAccess = passAccess;
    }

    @Override
    public boolean validateId(String id) {
        if (id == null) {
            return false;
        } else {
            String regex = "^[9|6|7][0-9]{8}$";
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(id);
            return m.matches();
        }
    }

    @Override
    public boolean authenticatePassword(String id, String password) {
        if (id.isEmpty()){
            return false;
        }
        Map<User, String> userMap = passAccess.getMapPass();
        for (User user:userMap.keySet()) {
            if(user.phoneNum == Integer.parseInt(id)  && userMap.get(user).equals(password)){
                return true;
            }
        }
        return false;
    }
}