package e1;

import e1.EquipoArtisticoP.*;
import e1.EquipoTecnicoP.*;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;


import java.util.ArrayList;

public class E1Test {

    private static ArrayList<EquipoHumano> humanList = new ArrayList<>();
    private static final Pelicula m1 = new Pelicula(humanList, "Alcarras", 30000000);
    @BeforeAll
    static void insertHumans() {
        humanList.add(new Especialista("Josep", "Abad", "33344455X", 666666666, "Española", 100,true));
        humanList.add(new Especialista("Clara", "Valle", "33344455X", 666666666, "Española", 100,false));
        humanList.add(new Interprete("Ainet", "Jounou", "33344455X", 666666666, "Española", 90, Rol.PROTAGONIST));
        humanList.add(new Interprete("Xenia", "Roset", "33344455X", 666666666, "Española", 50, Rol.SECONDARY));
        humanList.add(new Doblador("Cristina", "Puig", "33344455X", 666666666, "Española", 20 ));
        humanList.add(new Director("Carla", "Simon", "33344455X", 666666666, "Española", 500, 7));
        humanList.add(new Productor("Maria", "Zamora", "33344455X", 666666666, "Española", 100));
        humanList.add(new Musico("Andrea", "Koch", "33344455X", 666666666, "Española", 200));
        humanList.add(new Guionista("Arnau", "Vilaro", "33344455X", 666666666, "Española", 300, true));
        humanList.add(new Guionista("José Luis", "Borau", "33344455X", 666666666, "Española", 300, false));
    }

    @Test
    void PrintSalaries() {
        assertEquals("""
                Josep Abad (Strunt performer with extra for danger): 5000.0 euro
                Clara Valle (Strunt performer): 4000.0 euro
                Ainet Jounou (Actor protagonist): 54000.0 euro
                Xenia Roset (Actor secondary): 10000.0 euro
                Cristina Puig (Dubber): 400.0 euro
                Carla Simon (Director, 7 years of experience): 57000.0 euro
                Maria Zamora (Producer): 9000.0 euro
                Andrea Koch (Musician): 12000.0 euro
                Arnau Vilaro (Screenwriter, original screenplay): 25000.0 euro
                José Luis Borau (Screenwriter): 21000.0 euro
                The total payroll for Alcarras is 197400.0 euro""", m1.printSalaries());
    }
    @Test
    void PrintRoyalties() {
        assertEquals("""
                Carla Simon (Director): 1500000.0 euro
                Maria Zamora (Producer): 600000.0 euro
                Andrea Koch (Musician): 1200000.0 euro
                Arnau Vilaro (Screenwriter): 1500000.0 euro
                José Luis Borau (Screenwriter): 1500000.0 euro""", m1.printRoyalties());
    }
}
