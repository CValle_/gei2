package e2;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class E2Test {
    private static final ArrayList<String> candidates = new ArrayList<>();

    @BeforeAll
    static void insertCandidates() {
        candidates.add("Kelly Michelle Lee Osbourne");
        candidates.add("Flula Borg");
        candidates.add("Matthew Langford Perry");
        candidates.add("Zachary David Alexander");
        candidates.add("Lisa Valerie Kudrow-Stern");
        
    }

    @Test
    void testRecorridoRebote() {
        TVRealityList<String> tvr = new TVRealityList<>((ArrayList<String>) candidates.clone(), false);
        assertEquals(SelectCandidates.selectCandidates(tvr, 3),"Kelly Michelle Lee Osbourne");
        TVRealityList<String> tvr2 = new TVRealityList<>((ArrayList<String>) candidates.clone(), false);
        assertEquals(SelectCandidates.selectCandidates(tvr2, 4),"Flula Borg");
        TVRealityList<String> tvr3 = new TVRealityList<>((ArrayList<String>) candidates.clone(), false);
        assertEquals(SelectCandidates.selectCandidates(tvr3, 2),"Kelly Michelle Lee Osbourne");
        TVRealityList<String> tvr4 = new TVRealityList<>((ArrayList<String>) candidates.clone(), false);
        assertEquals(SelectCandidates.selectCandidates(tvr4, 5),"Kelly Michelle Lee Osbourne");
    }

    @Test
    void testRecorridoCircular() {
        TVRealityList<String> tvr = new TVRealityList<>((ArrayList<String>) candidates.clone(), true);
        assertEquals(SelectCandidates.selectCandidates(tvr, 3),"Zachary David Alexander");
        TVRealityList<String> tvr2 = new TVRealityList<>((ArrayList<String>) candidates.clone(), true);
        assertEquals(SelectCandidates.selectCandidates(tvr2, 5),"Flula Borg");
        TVRealityList<String> tvr3 = new TVRealityList<>((ArrayList<String>) candidates.clone(), true);
        assertEquals(SelectCandidates.selectCandidates(tvr3, 4),"Kelly Michelle Lee Osbourne");

    }

}
