package e3;

import e3.LoginStrategies.EmailLogin;
import e3.LoginStrategies.PhoneNumLogin;
import e3.LoginStrategies.UserLogin;
import e3.MfaStrategies.AppCodeMfa;
import e3.MfaStrategies.EmailCodeMfa;
import e3.MfaStrategies.SmsCodeMfa;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


public class E3Test {
    private static final Map<User, String> userMap = new HashMap<>();
    private static final LoginScreen login = new LoginScreen(userMap);
    //Login Strategies
    private static final LoginStrategy username = new UserLogin(login);
    private static final LoginStrategy email = new EmailLogin(login);
    private static final LoginStrategy phoneNum = new PhoneNumLogin(login);

    //MFA Strategies
    private static final MfaStrategy appCode = new AppCodeMfa();
    private static final MfaStrategy emailCode = new EmailCodeMfa();
    private static final MfaStrategy smsCode = new SmsCodeMfa();

    private static final User user1 = new User("Raúl Mouzo","raul.mouzo@udc.es","raul.mouzo",674968301);
    private static final User user2 = new User("Clara Valle","clara.valle@udc.es","clara.valle",720069147);
    private static final User user3 = new User("Pablo Blanco","pablo.blanco.suarez@gmail.com","pablo_blanco1998",981778502);
    private static final User user4 = new User("Rosario Balasco","rosario2001@yahoo.es","rosario",779456254);

    @BeforeAll
    static void insertUsers() {

        userMap.put(user1,"raul_passwd");
        userMap.put(user2,"clara_passwd");
        userMap.put(user3,"pablo_passwd");
        userMap.put(user4,"rosario_passwd");
    }



    @Test
    void testValidateId(){
        login.setLoginStrategy(username);
        assertTrue(login.loginStrategy.validateId("raul.mouzo")); //Correcto
        assertFalse(login.loginStrategy.validateId("_claravalle")); //Error por empezar o terminar por '-','_' o '.'
        assertFalse(login.loginStrategy.validateId("abcd")); //Error por no tener más de 5 caracteres

        login.setLoginStrategy(email);
        assertTrue(login.loginStrategy.validateId("raul.mouzo@udc.es")); //Correcto
        assertFalse(login.loginStrategy.validateId("claravalle_udc.es")); //Error por no contener @
        assertFalse(login.loginStrategy.validateId("abcd@yahoo")); //Error por no contener un dominio de nivel superior

        login.setLoginStrategy(phoneNum);
        assertTrue(login.loginStrategy.validateId("674968301")); //Correcto
        assertFalse(login.loginStrategy.validateId("66997512")); //Error por no contener 9 digitos
        assertFalse(login.loginStrategy.validateId("98176587a")); //Error por tener una letra

    }
    @Test
    void testUsername(){
        login.setLoginStrategy(username);
        //authenticatePassword
        assertTrue(login.loginStrategy.authenticatePassword("raul.mouzo","raul_passwd")); // Correcto
        assertFalse(login.loginStrategy.authenticatePassword("clara.valle","clar_passwd")); // Contraseña incorrecta
        assertFalse(login.loginStrategy.authenticatePassword("wrong_user","rosario_passwd")); // Usuario incorrecto
        assertFalse(login.loginStrategy.authenticatePassword("pablo_blanco1998","")); // Contraseña vacía
        assertFalse(login.loginStrategy.authenticatePassword("","pablo_passwd")); // Usuario vacío
    }
    @Test
    void testEmail(){
        login.setLoginStrategy(email);
        assertTrue(login.loginStrategy.authenticatePassword("raul.mouzo@udc.es","raul_passwd")); //Correcto
        assertFalse(login.loginStrategy.authenticatePassword("clara.valle@udc.es","clar_passwd")); // Contraseña incorrecta
        assertFalse(login.loginStrategy.authenticatePassword("pablo.blanco.suarez@gmail.com","")); // Contraseña vacía
        assertFalse(login.loginStrategy.authenticatePassword("wrong_email@yahoo.es","rosario")); // Email incorrecto
        assertFalse(login.loginStrategy.authenticatePassword("","pablo_passwd")); // Email vacío
    }

    @Test
    void testPhoneNumber(){
        login.setLoginStrategy(phoneNum);
        assertTrue(login.loginStrategy.authenticatePassword("674968301","raul_passwd")); //Correcto
        assertFalse(login.loginStrategy.authenticatePassword("720069147","clar_passwd")); // Contraseña incorrecta
        assertFalse(login.loginStrategy.authenticatePassword("981778502","")); // Contraseña vacía
        assertFalse(login.loginStrategy.authenticatePassword("123","rosario")); // Número de tlf incorrecto
        assertFalse(login.loginStrategy.authenticatePassword("","pablo_passwd")); // Número de tlf vacío
    }

    public boolean isNumeric(String code) {
        try {
            Integer.parseInt(code);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    @Test
    void testMfaStrategies(){
        login.setMfaStrategy(appCode);
        assertEquals(6, login.mfaStrategy.generateString().length()); //Correcto
        assertNotEquals(4, login.mfaStrategy.generateString().length()); //Código demasiado corto
        assertTrue(isNumeric(login.mfaStrategy.generateString())); //Correcto, es numérico
        assertNotEquals(login.mfaStrategy.generateString(), login.mfaStrategy.generateString()); //Correcto, no son iguales

        login.setMfaStrategy(emailCode);
        assertEquals(8, login.mfaStrategy.generateString().length()); //Correcto
        assertNotEquals(4, login.mfaStrategy.generateString().length()); //Código demasiado corto
        assertFalse(isNumeric(login.mfaStrategy.generateString())); //Correcto, es alfanumérico
        assertNotEquals(login.mfaStrategy.generateString(), login.mfaStrategy.generateString()); //Correcto, no son iguales


        login.setMfaStrategy(smsCode);
        assertEquals(4, login.mfaStrategy.generateString().length()); //Correcto
        assertNotEquals(7, login.mfaStrategy.generateString().length()); //Código demasiado largo
        assertTrue(isNumeric(login.mfaStrategy.generateString())); //Correcto, es numérico
        assertNotEquals(login.mfaStrategy.generateString(), login.mfaStrategy.generateString()); //Correcto, no son iguales


    }
}
