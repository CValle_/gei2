package e2;

public interface Observer {
    //se le pasan Subjects
    void update(Object object);
}
