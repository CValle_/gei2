package e2;

public enum AlertType {
    ROJA (1),
    NARANJA (0);

    private final int priority;

    AlertType(int priority) {
        this.priority = priority;
    }
    public int getPriority(){
        return this.priority;
    }
}
