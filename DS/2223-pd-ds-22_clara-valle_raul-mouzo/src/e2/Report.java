package e2;

public class Report implements Comparable<Report>{
    private AlertType alertType;

    private String tankName;

    private String tankLocation;
    private SensorType sensorType;
    private float sensorValue;
    private String dateTime;

    public Report(AlertType alertType, String tankName, String tankLocation, SensorType sensorType, float sensorValue, String dateTime) {
        this.alertType = alertType;
        this.tankName = tankName;
        this.tankLocation = tankLocation;
        this.sensorType = sensorType;
        this.sensorValue = sensorValue;
        this.dateTime = dateTime;
    }

    public AlertType getAlertType() {
        return alertType;
    }

    @Override
    public int compareTo(Report otherReport) {
        if (this.alertType.getPriority() < otherReport.alertType.getPriority()) {
            return 1;
        } else if (this.alertType.getPriority() > otherReport.alertType.getPriority()) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "* Alerta " + alertType+": \n" +
                tankName + ", " + tankLocation + '\n' +
                "Control de "+ sensorType.toString().toLowerCase() +": " +
                "parámetro " + sensorType +
                ", nivel " + sensorValue +
                "\n" + dateTime +
                '\n';
    }
}
