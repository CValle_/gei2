package e2;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.PriorityQueue;

public class Staff implements Observer {
    PriorityQueue<Report> reportPriorityQueue = new PriorityQueue<>();

    private final String name;

    private boolean checkUpdateTest = false;

    public Staff(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void update(Object a) {
        Alert alert = (Alert) a;

        if(alert.getSensor().getTank() == null){throw new IllegalArgumentException("Ningún tanque asignado a ese sensor\n");}
        AlertType alertType = alert.getAlertType();
        String tankName = alert.getSensor().getTank().getName();
        String tankLocation = alert.getSensor().getTank().getLocation();
        SensorType sensorType = alert.getSensor().getSensorType();
        float sensorValue = alert.getSensor().getValue();

        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/yyyy");
        String formatDateTime = now.format(formatter);

        //crear nuevo informe con los datos de la alerta
        reportPriorityQueue.add(new Report(alertType, tankName, tankLocation, sensorType, sensorValue, formatDateTime));
        checkUpdateTest = true;
    }

    public String getReports() {
        boolean printOrangeAlerts = false;
        StringBuilder report = new StringBuilder();
        report.append("Alertas de: ").append(this.name).append("\n");
        if (reportPriorityQueue.isEmpty()) {
            report.append("No hay alertas\n");
        } else {
            report.append("Alertas ROJAS: \n");
            while (!reportPriorityQueue.isEmpty()) {
                if (reportPriorityQueue.peek().getAlertType() == AlertType.NARANJA && !printOrangeAlerts){
                    printOrangeAlerts = true;
                    report.append("Alertas NARANJAS: \n");
                }
                report.append(reportPriorityQueue.remove()).append("\n");
            }
        }
        return report.toString();
    }

    public void printReports(){
        System.out.println(getReports());
    }

    //For tests
    public boolean staffUpdated(){
        return checkUpdateTest;
    }
}
