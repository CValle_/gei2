package e2;

import java.util.ArrayList;
import java.util.List;

public class Tank {
    private final String name;
    private final String location;
    private final List<Sensor> sensors = new ArrayList<>();

    public Tank(String name, String location) {
        this.name = name;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public void addSensor(Sensor sensor){
        if(sensor.getTank() == null){
            sensors.add(sensor);
            sensor.setTank(this);
        } else if(!sensor.getTank().equals(this)){
            sensor.getTank().removeSensor(sensor);
            sensor.setTank(this);
            sensors.add(sensor);
        }
    }
    public void removeSensor(Sensor sensor){
        sensors.remove(sensor);
        sensor.setTank(null);
    }

    //For tests
    public boolean hasSensor(Sensor testSensor) {
        boolean sensorInList = false;
        for (Sensor s : sensors) {
            if (s == testSensor) {
                sensorInList = true;
                break;
            }
        }
        return sensorInList;
    }
}
