package e2;

import java.util.ArrayList;
import java.util.List;

public class Alert implements Observer {

    private final List<Observer> observers = new ArrayList<>();
    private AlertType alertType;
    private Sensor sensor;


    private final int MIN_NORMAL;
    private final int MAX_NORMAL;
    private final int MIN_ORANGE_ALERT;
    private final int MAX_ORANGE_ALERT;

    public Alert(Sensor sensor, int MIN_NORMAL, int MAX_NORMAL, int MIN_ORANGE_ALERT, int MAX_ORANGE_ALERT) {
        sensor.addObserver(this);
        this.sensor = sensor;

        this.MIN_NORMAL = MIN_NORMAL;
        this.MAX_NORMAL = MAX_NORMAL;
        this.MIN_ORANGE_ALERT = MIN_ORANGE_ALERT;
        this.MAX_ORANGE_ALERT = MAX_ORANGE_ALERT;

        //¿Los dispositivos de control son + observadores de la alerta?
    }

    public AlertType getAlertType() {
        return alertType;
    }

    public Sensor getSensor() {
        return sensor;
    }
    void setSensor(Sensor sensor){
        this.sensor = sensor;
    }
    public void addObserver(Observer observer){
        observers.add(observer);
    }

    public void removeObserver(Observer observer){
        observers.remove(observer);
    }

    //Es necesario para comprobar que una device solo pueda estar asociado a una alerta
    public void addObserver(ControlDevice device){
        if(device.getAlert() == null){
            observers.add(device);
            device.setAlert(this);
        } else if(!device.getAlert().equals(this)){
            device.getAlert().removeObserver(device);
            device.setAlert(this);
            observers.add(device);
        }
    }

    public void notifyObservers(){
        for(Observer observer: observers){
            observer.update(this);
        }
    }

    @Override
    public void update(Object s) {
        Sensor sensor = (Sensor) s;

        if(sensor.getValue() < MIN_ORANGE_ALERT || sensor.getValue() > MAX_ORANGE_ALERT){
            alertType = AlertType.ROJA;
            notifyObservers();
        } else if(sensor.getValue() < MIN_NORMAL || sensor.getValue() > MAX_NORMAL){
            alertType = AlertType.NARANJA;
            notifyObservers();
        }
    }

    //For tests
    public boolean hasDevice(ControlDevice testDevice) {
        boolean deviceInList = false;
        for (Observer a : observers) {
            if (a == testDevice) {
                deviceInList = true;
                break;
            }
        }
        return deviceInList;
    }
}
