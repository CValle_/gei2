package e2;

public class ControlDevice implements Observer {

    private Alert alert;
    @Override
    public void update(Object a) {
        Alert alert = (Alert) a;

        System.out.println("Alerta "+ alert.getAlertType().toString().toLowerCase() + " detectada, actuando...");
    }


    //For tests
    public String getDeviceStatus(){
        if(alert == null){
            throw new IllegalArgumentException("No hay ninguna alerta asociada a ese dispositivo de control");
        }
        Sensor sensor = alert.getSensor();
        Tank tank = sensor.getTank();
        return "- Dispositivo de control actuando sobre:\n" +
                "Tanque: " + tank.getName() + "\n" +
                "Localización: " + tank.getLocation()  + "\n" +
                "Tipo de alerta: " + alert.getAlertType().toString();
    }

    public Alert getAlert(){
        return alert;
    }
    void setAlert(Alert alert){
        this.alert = alert;
    }
}
