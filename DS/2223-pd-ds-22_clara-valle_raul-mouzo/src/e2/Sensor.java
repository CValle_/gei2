package e2;

import java.util.ArrayList;
import java.util.List;

public class Sensor {

    private Tank tank;
    private float value;
    private final SensorType sensorType;
    private List<Observer> alerts = new ArrayList<>();

    public Sensor(SensorType sensorType){
        this.sensorType = sensorType;
    }

    public void notifyAlerts() {
        for (Observer alert : alerts) {
            alert.update(this);
        }
    }
    public float getValue(){
        return value;
    }
    public void setValue(int value){
        this.value = value;
        notifyAlerts();
    }

    public Tank getTank() {
        return tank;
    }

    void setTank(Tank tank) {
        this.tank = tank;
    }

    public SensorType getSensorType() {
        return sensorType;
    }

    public void addObserver(Observer observer){
        alerts.add(observer);
    }
    public void addObserver(Alert alert){
        if(alert.getSensor() == null) {
            alerts.add(alert);
            alert.setSensor(this);
        } else if(!alert.getSensor().equals(this)){
            alert.getSensor().removeAlert(alert);
            alert.setSensor(this);
            alerts.add(alert);
        }
    }
    public void removeAlert(Alert alert){ alerts.remove(alert); }

    //For tests
    public boolean hasAlert(Alert testAlert) {
        boolean alertInList = false;
        for (Observer a : alerts) {
            if (a == testAlert) {
                alertInList = true;
                break;
            }
        }
        return alertInList;
    }
}
