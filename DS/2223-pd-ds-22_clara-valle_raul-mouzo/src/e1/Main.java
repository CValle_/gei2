package e1;

import e1.order.Order;
public class Main {
    public static void main(String[] args) {
        Product pr1 = new Product(111, 15);
        Product pr2 = new Product(222, 10);
        Product pr3 = new Product(333, 8);
        Product pr4 = new Product(444, 8);

        Order order = new Order(1111);

        order.printScreenInfo();
        order.addProductOrder(pr1,10);
        order.addProductOrder(pr2,2);
        order.addProductOrder(pr3,3);
        order.addProductOrder(pr4,4);
        order.removeProductOrder(pr4);

        order.checkOut();
        order.printScreenInfo();

        order.modifyProductQuantity(pr1,1);

        order.payment();
        order.printScreenInfo();
        order.cancelled();
        order.printScreenInfo();

        System.out.println("--------\nLOG:");
        order.getLog().forEach(System.out::println);
        System.out.println("--------\n");


    }
}
