package e1.order;

import e1.Product;

public class Cancelled implements OrderState{

    final private String name = "Cancelled Order";

    @Override
    public String getStateName() {
        return name;
    }

    //Instancia única
    private static final Cancelled instance = new Cancelled();
    public static Cancelled getInstance(){ return instance;}

    //Métodos
    @Override
    public String screenInfo(Order order) {
        return name;
    }
    @Override
    public void removeProduct(Product product, Order order){}
}
