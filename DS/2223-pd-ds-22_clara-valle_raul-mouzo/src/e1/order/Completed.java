package e1.order;

import e1.Product;


public class Completed implements OrderState {
    final String name = "Completed Order";

    @Override
    public String getStateName() {
        return name;
    }

    //Instancia única
    private static final Completed instance = new Completed();
    public static Completed getInstance(){ return instance;}

    //Métodos
    @Override
    public void removeProduct(Product product, Order order){}

}
