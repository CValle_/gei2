package e1.order;

import e1.Product;

public class ProductOrder {
    private int quantity = 0;
    private final Product product;

    public ProductOrder(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }
    public int getQuantity() {
        return quantity;
    }

    public void addQuantity(int quantityToAdd) {
        if (quantityToAdd > 0) {
            this.product.removeStock(quantityToAdd);
            this.quantity += quantityToAdd;
        } else {
            throw new IllegalArgumentException("Quantity must be positive");
        }
    }

    public void removeQuantity(int quantityToRemove) {
        if (quantityToRemove > 0 && quantityToRemove <= this.quantity) {
            this.product.addStock(quantityToRemove);
            this.quantity -= quantityToRemove;
        } else {
            throw new IllegalArgumentException("Quantity not valid");
        }
    }

    public void modifyQuantity(int quantity){
        if(quantity < 0){
            throw new IllegalArgumentException("Quantity must be positive");
        }
        if (quantity > this.quantity) {
            this.addQuantity(quantity - this.quantity);
        } else if(quantity < this.quantity){
            this.removeQuantity(this.quantity - quantity);
        }
    }
}
