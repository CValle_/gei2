package e1.order;


import e1.Product;

import java.util.ArrayList;
import java.util.List;

public interface OrderState {
    //Estados
    default void shoppingCart(Order order) {}
    default void checkOut(Order order) {}
    default void payment(Order order) {}
    default void cancelled(Order order){}
    default void completed(Order order){}


    //Métodos
    String getStateName();

    default String screenInfo(Order order){
        return (this.getStateName() + ": " + order.getProductList().size() + " products\n");
    }

    default void addProduct(Product product, int quantity, Order order){}
    default void modifyProductQuantity(Product product, int quantity, Order order){}

    default void removeProduct(Product product,Order order){
        ArrayList<ProductOrder> productOrderList = order.getProductList();
        ProductOrder productOrder = getProductOrder(product, productOrderList);
        if(productOrder != null) {
            productOrder.removeQuantity(productOrder.getQuantity());
            productOrderList.remove(productOrder);
            order.addLog("- Remove: Item: " + product.getProductId() +  " -> " + this.getStateName() + " -- Products: "
                    + order.getProductList().size());
        } else {
            throw new IllegalArgumentException("Product is not in list");
        }
    }

    default ProductOrder getProductOrder(Product product, List<ProductOrder> productOrderList){
        for (ProductOrder productOrder:productOrderList) {
            if (productOrder.getProduct().equals(product)) {
                return productOrder;
            }
        }
        return null;
    }
}
