package e1.order;

import e1.Product;

import java.time.format.DateTimeFormatter;


public class Payment implements OrderState {

    private final String name = "Paid order";

    @Override
    public String getStateName() {
        return name;
    }

    //Instancia única
    private static final Payment instance = new Payment();
    public static Payment getInstance(){ return instance;}


    //Cambios de estado
    @Override
    public void cancelled(Order order){
        order.setState(Cancelled.getInstance());
        order.addLog("Order " + order.getOrderNum() + ": " + order.getState().getStateName());
        for (ProductOrder productOrder:order.getProductList()){
            productOrder.removeQuantity(productOrder.getQuantity());
        }
    }
    @Override
    public void completed(Order order){
        order.setState(Completed.getInstance());
        order.addLog("Order " + order.getOrderNum() + ": " + order.getState().getStateName());
    }

    //Métodos

    @Override
    public String screenInfo(Order order) {
        return (name + " : " + order.getProductList().size() + " products -- date " +
                order.getPaymentDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\n");
    }

    @Override
    public void removeProduct(Product product, Order order){}


}
