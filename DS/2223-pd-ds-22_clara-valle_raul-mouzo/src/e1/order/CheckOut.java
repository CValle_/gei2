package e1.order;

import e1.Product;


import java.time.LocalDateTime;
import java.util.ArrayList;

public class CheckOut implements OrderState {
    private final String name = "CheckOut Order";
    @Override
    public String getStateName() {
        return name;
    }

    //Instancia única
    private static final CheckOut instance = new CheckOut();
    public static CheckOut getInstance(){ return instance;}

    //Cambios de estado
    @Override
    public void shoppingCart(Order order) {
        order.addLog("Order " + order.getOrderNum() + ": Shopping Phase");
        order.setState(ShoppingCart.getInstance());
    }
    @Override
    public void payment(Order order) {
        order.addLog("Order " + order.getOrderNum() + ": Payment Phase");
        order.setPaymentDate(LocalDateTime.now()); //Se establece fecha de pago
        order.setState(Payment.getInstance());
    }

    //Métodos

    @Override
    public void addProduct(Product product, int quantity, Order order) {
    }

    @Override
    public void modifyProductQuantity(Product product, int quantity, Order order){
        ArrayList<ProductOrder> productOrderList = order.getProductList();
        ProductOrder productOrder = getProductOrder(product, productOrderList);
        if(productOrder != null) {
            productOrder.modifyQuantity(quantity);
            if(quantity == 0){
                productOrderList.remove(productOrder);
            }
            order.addLog("- Modify: Item: " + product.getProductId() + " - Quantity: " + quantity + " -> " + this.name + " -- Products: "
                    + order.getProductList().size());
        } else {
            throw new IllegalArgumentException("Product is not in list");
        }
    }
}
