package e1.order;

import e1.Product;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Order {
    private final int orderNum;
    private OrderState orderState;
    private final ArrayList<ProductOrder> productList = new ArrayList<>();
    private final ArrayList<String> log = new ArrayList<>();
    private LocalDateTime paymentDate;

    public Order(int orderNum) {
        this.orderNum = orderNum;
        this.orderState = ShoppingCart.getInstance();
        addLog("Order " + orderNum + ": Shopping Phase");
    }
    public OrderState getState(){
        return orderState;
    }

    public ArrayList<ProductOrder> getProductList() {
        return productList;
    }

    public int getOrderNum() {
        return orderNum;
    }

    void setState(OrderState orderState){
        this.orderState = orderState;
    }

    void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }
    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public ArrayList<String> getLog() {
        return log;
    }

    public ProductOrder getProductOrder(Product product){
        return orderState.getProductOrder(product, productList);
    }

    void addLog(String log){
        this.log.add(log);
    }

    public void shoppingCart(){
        orderState.shoppingCart(this);
    }
    public void checkOut(){
        orderState.checkOut(this);
    }

    public void payment(){
        orderState.payment(this);
    }
    public void completed(){
        orderState.completed(this);
    }

    public void cancelled(){
        orderState.cancelled(this);
    }

    public String screenInfo(){
        return "Order Number:" + orderNum + "\n" + orderState.screenInfo(this);
    }

    public void printScreenInfo() {
        System.out.println(screenInfo());
    }

    public void addProductOrder(Product product, int quantity) {
        orderState.addProduct(product, quantity, this);
    }

    public void removeProductOrder(Product product) {
        orderState.removeProduct(product, this);
    }

    public void modifyProductQuantity(Product product, int quantity){
        orderState.modifyProductQuantity(product,quantity,this);
    }

}
