package e1.order;

import e1.Product;

import java.util.ArrayList;

public class ShoppingCart implements OrderState {
    private final String name = "Shopping Cart";
    @Override
    public String getStateName() {
        return name;
    }

    //Instancia única
    private static final ShoppingCart instance = new ShoppingCart();
    public static ShoppingCart getInstance(){ return instance;}

    //Cambios de estado
    public void checkOut(Order order) {
        order.addLog("Order " + order.getOrderNum() + ": Check Out Phase");
        order.setState(CheckOut.getInstance());
    }

    //Métodos

    @Override
    public String screenInfo(Order order) {
        if(order.getProductList().size() == 0) {
            return (name + " -- Welcome to online shop\n");
        } else {
            return (name + " -- " + order.getProductList().size() + " products\n");
        }
    }

    @Override
    public void addProduct(Product product, int quantity, Order order){
        ArrayList<ProductOrder> productOrderList = order.getProductList();
        ProductOrder productOrder = getProductOrder(product, productOrderList);

        if(productOrder != null){
            productOrder.addQuantity(quantity);
        } else if (quantity > 0){
            productOrder = new ProductOrder(product);
            productOrder.addQuantity(quantity);
            productOrderList.add(productOrder);
        } else {
            throw new IllegalArgumentException("Quantity must be positive");
        }
        order.addLog("- Add: Item: " + product.getProductId() + " - Quantity: " + quantity + " -> " + this.name + " -- Products: " + order.getProductList().size());
    }
}
