package e1;

public class Product {
        private final int productId;
        private int stock;

        public Product(int productId, int stock) {
                this.productId = productId;
                this.stock = stock;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Product product = (Product) o;
                return productId == product.getProductId();
        }

        public int getProductId() {
                return productId;
        }

        public int getStock() {
                return stock;
        }

        //Hace falta volver a comprobar si la cantidad es positiva, ya que estos métodos pueden llamarse de forma independiente
        public void removeStock(int stockToRemove) {
                if (stockToRemove > 0 && this.stock >= stockToRemove) {
                        this.stock -= stockToRemove;
                } else {
                        throw new IllegalArgumentException("Not Enough Stock");
                }
        }

        public void addStock(int stockToAdd) {
                if (stockToAdd > 0) {
                        this.stock += stockToAdd;
                } else {
                        throw new IllegalArgumentException("Quantity must be positive");
                }
        }
}
