package e2;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.*;

public class E2Test {
    Tank tank1 = new Tank("Piscina de las focas","exterior");

    Tank tank2 = new Tank("Recinto de cocodrilos","exterior");

    Sensor sensor1 = new Sensor(SensorType.OXIGENO);
    Sensor sensor2 = new Sensor(SensorType.PH);

    Sensor sensor3 = new Sensor(SensorType.OXIGENO);

    Sensor sensor4 = new Sensor(SensorType.PH);
    Alert alert1 = new Alert(sensor1,5,20,0,35);
    Alert alert2 = new Alert(sensor1,0,10,-10,20);
    Alert alert3 = new Alert(sensor3,0,5,-5,10);
    ControlDevice device1 = new ControlDevice();
    ControlDevice device2 = new ControlDevice();
    Staff staff1 = new Staff("Mantenimiento Focas");
    Staff staff2 = new Staff("Mantenimiento de cocodrilos");



    public void addObservers() {
        alert1.addObserver(staff1); //Orange, s1
        alert2.addObserver(staff1); //Red, s1
        alert3.addObserver(staff2);
    }


    public void addSensors(){
        tank1.addSensor(sensor1);
        tank1.addSensor(sensor2);
        tank2.addSensor(sensor3);
        tank2.addSensor(sensor4);
    }


    @Test
    public void checkAddSensor(){
        addSensors();
        assertTrue(tank1.hasSensor(sensor1));
        assertTrue(tank1.hasSensor(sensor2));

        assertTrue(tank2.hasSensor(sensor3));
        assertTrue(tank2.hasSensor(sensor4));

        //Añadir sensor de tanque 2 a tanque 1
        tank1.addSensor(sensor3);
        assertTrue(tank1.hasSensor(sensor3));
        assertFalse(tank2.hasSensor(sensor3));
    }

    @Test
    public void checkAddAlerts(){
        assertTrue(sensor1.hasAlert(alert1));
        assertTrue(sensor1.hasAlert(alert2));
        assertTrue(sensor3.hasAlert(alert3));

        //Añadir alerta de sensor1 en sensor2
        sensor2.addObserver(alert2);
        assertFalse(sensor1.hasAlert(alert2));
        assertTrue(sensor2.hasAlert(alert2));
    }

    @Test
    public void checkAddDevices(){
        alert1.addObserver(device1);
        alert2.addObserver(device2);

        assertTrue(alert1.hasDevice(device1));
        assertTrue(alert2.hasDevice(device2));
        alert2.addObserver(device2);

        assertFalse(alert1.hasDevice(device2));
        assertTrue(alert2.hasDevice(device2));
    }


    @Test
    public void checkNoAlerts(){
        //Si no se cambia el valor del sensor no puede haber ninguna alerta

        assertThrows(IllegalArgumentException.class,() -> device1.getDeviceStatus());
        assertFalse(staff1.staffUpdated());

        assertEquals(staff1.getReports(),"Alertas de: Mantenimiento Focas\nNo hay alertas\n");
    }

    @Test
    public void checkAlerts(){
        addSensors();
        addObservers();
        alert1.addObserver(device1);
        alert2.addObserver(device2);

        sensor1.setValue(-5);

        assertEquals("""
                Alertas de: Mantenimiento Focas
                Alertas ROJAS:\s
                * Alerta ROJA:\s
                Piscina de las focas, exterior
                Control de oxigeno: parámetro OXIGENO, nivel -5.0
                """ + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/yyyy")) +"\n\n"+
                """
                Alertas NARANJAS:\s
                * Alerta NARANJA:\s
                Piscina de las focas, exterior
                Control de oxigeno: parámetro OXIGENO, nivel -5.0
                """ + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/yyyy")) +"\n\n"
                ,staff1.getReports()) ;


        assertEquals(alert1.getAlertType(), AlertType.ROJA);
        assertEquals(alert2.getAlertType(), AlertType.NARANJA);
        assertEquals("""
            - Dispositivo de control actuando sobre:
            Tanque: Piscina de las focas
            Localización: exterior
            Tipo de alerta: ROJA""",device1.getDeviceStatus());

        assertEquals("""
            - Dispositivo de control actuando sobre:
            Tanque: Piscina de las focas
            Localización: exterior
            Tipo de alerta: NARANJA""",device2.getDeviceStatus());

        sensor3.setValue(-1);
        sensor3.setValue(-30);
        sensor3.setValue(-35);


        assertEquals("""
            Alertas de: Mantenimiento de cocodrilos
            Alertas ROJAS:\s
            * Alerta ROJA:\s
            Recinto de cocodrilos, exterior
            Control de oxigeno: parámetro OXIGENO, nivel -30.0
            """ + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/yyyy")) +"\n\n"+
            """
            * Alerta ROJA:\s
            Recinto de cocodrilos, exterior
            Control de oxigeno: parámetro OXIGENO, nivel -35.0
            """ +  LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/yyyy")) +"\n\n"+
            """
            Alertas NARANJAS:\s
            * Alerta NARANJA:\s
            Recinto de cocodrilos, exterior
            Control de oxigeno: parámetro OXIGENO, nivel -1.0
            """ + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/yyyy")) +"\n\n"
                ,staff2.getReports()) ;

        assertTrue(staff1.staffUpdated());
    }
}
