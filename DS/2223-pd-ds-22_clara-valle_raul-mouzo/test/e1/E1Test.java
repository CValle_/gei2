package e1;

import e1.order.*;
import org.junit.Test;



import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class E1Test {
    private static final Product product1 = new Product(111, 15);
    private static final Product product2 = new Product(222, 10);
    private static final Product product3 = new Product(333, 15);
    private static final Product product4 = new Product(444,10);
    private static final Product product5 = new Product(555,30);


    private static final Order order = new Order(0000);
    private static final Order order1 = new Order(1111);
    private static final Order order2 = new Order(2222);
    private static final Order order3 = new Order(3333);
    private static final Order order4 = new Order(4444);




    void addProducts(Order order){
        order.addProductOrder(product1,10);
        order.addProductOrder(product2,2);
        order.addProductOrder(product3,3);
        order.addProductOrder(product4,4);
    }

    void removeProducts(Order order){
        order.removeProductOrder(product1);
        order.removeProductOrder(product2);
        order.removeProductOrder(product3);
        order.removeProductOrder(product4);
    }

    @Test
    public void productsOperations(){
        addProducts(order);

        //Añadir productos a la lista
        order.addProductOrder(product1,5);
        assertEquals(15,order.getProductOrder(product1).getQuantity());
        order.addProductOrder(product2,5);
        assertEquals(7,order.getProductOrder(product2).getQuantity());
        order.addProductOrder(product3,1);
        assertEquals(4,order.getProductOrder(product3).getQuantity());
        order.addProductOrder(product4,3);
        assertEquals(7,order.getProductOrder(product4).getQuantity());

        //Borrar productos de la lista
        order.removeProductOrder(product1);
        order.removeProductOrder(product2);
        order.removeProductOrder(product3);
        order.removeProductOrder(product4);
        assertTrue(order.getProductList().isEmpty());

        addProducts(order);

        //Cantidades negativas
        assertThrows(IllegalArgumentException.class, ()-> order.addProductOrder(product1,-1));
        assertEquals(10,order.getProductOrder(product1).getQuantity());
        order.removeProductOrder(product1);
        assertThrows(IllegalArgumentException.class, ()-> order.addProductOrder(product1,-1));

        //Añadir más productos de los que hay en el stock
        order.addProductOrder(product1,15);
        assertThrows(IllegalArgumentException.class, ()-> order.addProductOrder(product1,2));
        assertEquals(15,order.getProductOrder(product1).getQuantity());


        //Borrar un producto que no está en la lista
        assertThrows(IllegalArgumentException.class, ()-> order.removeProductOrder(product5));

        order.checkOut();

        //Modificar cantidades de productos
        order.modifyProductQuantity(product1,1);
        assertEquals(1,order.getProductOrder(product1).getQuantity());
        order.modifyProductQuantity(product2,5);
        assertEquals(5,order.getProductOrder(product2).getQuantity());

        //Modificar cantidades que superan el stock
        assertThrows(IllegalArgumentException.class, ()-> order.modifyProductQuantity(product2,20));

        //Modificar cantidad de un producto que no está en la lista
        assertThrows(IllegalArgumentException.class, ()-> order.modifyProductQuantity(product5,20));

        removeProducts(order);

        //Modificar el stock (administradores de la tienda online)
        assertThrows(IllegalArgumentException.class, ()-> product1.addStock(-1));
        assertThrows(IllegalArgumentException.class, ()-> product1.removeStock(-1));

    }
    @Test
    public void states(){
        assertEquals(ShoppingCart.getInstance(), order1.getState());

        //Ir a estado no permitido
        order1.payment();
        assertEquals(ShoppingCart.getInstance(), order1.getState());

        order1.checkOut();
        order2.checkOut();
        assertEquals(CheckOut.getInstance(), order1.getState());
        assertEquals(CheckOut.getInstance(), order2.getState());

        //Volver a ShoppingCart desde checkout.
        order1.shoppingCart();
        order2.shoppingCart();
        assertEquals(ShoppingCart.getInstance(), order1.getState());
        assertEquals(ShoppingCart.getInstance(), order2.getState());

        order1.checkOut();
        order2.checkOut();
        order1.payment();
        order2.payment();
        assertEquals(Payment.getInstance(), order1.getState());
        assertEquals(Payment.getInstance(), order2.getState());

        //Cancelled
        order1.cancelled();
        assertEquals(Cancelled.getInstance(), order1.getState());
        order1.completed();
        assertEquals(Cancelled.getInstance(),order1.getState());

        //Completed
        order2.completed();
        assertEquals(Completed.getInstance(), order2.getState());
        order2.cancelled();
        assertEquals(Completed.getInstance(),order2.getState());


    }


    @Test
    public void log(){
        addProducts(order3);
        order3.removeProductOrder(product4);
        order3.addProductOrder(product4,5);

        order3.checkOut();

        order3.modifyProductQuantity(product1,1);
        order3.payment();
        order3.cancelled();
        ArrayList<String> expectedLog = new ArrayList<>(
                Arrays.asList("Order 3333: Shopping Phase",
                        "- Add: Item: 111 - Quantity: 10 -> Shopping Cart -- Products: 1",
                        "- Add: Item: 222 - Quantity: 2 -> Shopping Cart -- Products: 2",
                        "- Add: Item: 333 - Quantity: 3 -> Shopping Cart -- Products: 3",
                        "- Add: Item: 444 - Quantity: 4 -> Shopping Cart -- Products: 4",
                        "- Remove: Item: 444 -> Shopping Cart -- Products: 3",
                        "- Add: Item: 444 - Quantity: 5 -> Shopping Cart -- Products: 4",
                        "Order 3333: Check Out Phase",
                        "- Modify: Item: 111 - Quantity: 1 -> CheckOut Order -- Products: 4",
                        "Order 3333: Payment Phase",
                        "Order 3333: Cancelled Order"
                )
        );
        assertArrayEquals(expectedLog.toArray(),order3.getLog().toArray());
        removeProducts(order3);
    }

    @Test
    public void screenInfo(){
        assertEquals("Order Number:4444\nShopping Cart -- Welcome to online shop\n",order4.screenInfo());

        order4.addProductOrder(product1,10);
        order4.addProductOrder(product2,5);
        order4.addProductOrder(product3,2);
        assertEquals("Order Number:4444\nShopping Cart -- 3 products\n",order4.screenInfo());

        order4.addProductOrder(product4,2);
        order4.checkOut();
        assertEquals("Order Number:4444\nCheckOut Order: 4 products\n",order4.screenInfo());

        order4.payment();
        assertEquals("Order Number:4444\nPaid order : 4 products -- date " +
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\n",order4.screenInfo());

        order4.cancelled();
        assertEquals("Order Number:4444\nCancelled Order",order4.screenInfo());
        removeProducts(order4);
    }
}
