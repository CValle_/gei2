package es.udc.redes.tutorial.tcp.server;
import java.net.*;
import java.io.*;

/** Thread that processes an echo server connection. */

public class ServerThread extends Thread {

  private Socket socket;

  public ServerThread(Socket s) {
    // Store the socket s
    this.socket = s;
  }

  public void run() {
    String msgIn;

    try {
      // Set the input channel
      BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      // Set the output channel
      PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

      // Receive the message from the client
      while ((msgIn = in.readLine()) != null) {
        System.out.println("SERVER: recived " + msgIn + " from " + socket.getInetAddress().toString() + ":" + socket.getPort());

        // Sent the echo message to the client
        System.out.println("SERVER: sending " + msgIn + " to " + socket.getInetAddress().toString() + ":" + socket.getPort());
        out.println(msgIn);
      }

      // Close the streams
      if (in != null) {
        in.close();
      }
      if (out != null) {
        out.close();
      }

    // Uncomment next catch clause after implementing the logic
     } catch (SocketTimeoutException e) {
      System.err.println("Nothing received in 300 secs");
    } catch (Exception e) {
      System.err.println("Error: " + e.getMessage());
      } finally {
	// Close the socket
      try{
        socket.close();
      }catch (IOException ioe){
        System.err.println("Server socket is null: " + ioe.getMessage());
      }
    }
  }
}
