package es.udc.redes.tutorial.tcp.server;

import java.net.*;
import java.io.*;

/**
 * MonoThread TCP echo server.
 */
public class MonoThreadTcpServer {

    public static void main(String argv[]) {
        if (argv.length != 1) {
            System.err.println("Format: es.udc.redes.tutorial.tcp.server.MonoThreadTcpServer <port>");
            System.exit(-1);
        }

        ServerSocket serverSocket = null;

        try {
            // Create a server socket
            serverSocket = new ServerSocket(Integer.valueOf(argv[0]));
            // Set a timeout of 300 secs
            serverSocket.setSoTimeout(300000);

            while (true) {
                // Wait for connections
                Socket clientSocket = serverSocket.accept();
                // Set the input channel
                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                // Set the output channel
                PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                // Receive the client message
                String msgIn = in.readLine();
                System.out.println("SERVER: recived " + msgIn + " from " + clientSocket.getInetAddress().toString() + ":" + clientSocket.getPort());
                // Send response to the client
                out.println(msgIn);
                System.out.println("SERVER: sending " + msgIn + " to " + clientSocket.getInetAddress().toString() + ":" + clientSocket.getPort());
                // Close the streams
                in.close();
                out.close();
                clientSocket.close();
            }
        // Uncomment next catch clause after implementing the logic            
        } catch (SocketTimeoutException e) {
            System.err.println("Nothing received in 300 secs ");
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
        } finally {
	        //Close the socket
            try{
                serverSocket.close();
            }catch (IOException ioe){
                System.err.println("Server socket is null: " + ioe.getMessage());
            }
        }
    }
}
