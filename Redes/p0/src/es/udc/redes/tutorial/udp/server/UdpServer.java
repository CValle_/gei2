package es.udc.redes.tutorial.udp.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;

/**
 * Implements a UDP echo server.
 */
public class UdpServer {

    public static void main(String argv[]) {
        if (argv.length != 1) {
            System.err.println("Format: es.udc.redes.tutorial.udp.server.UdpServer <port_number>");
            System.exit(-1);
        }

        DatagramSocket dSocket = null;
        byte[] buf = new byte[1024];

        try {
            // Create a server socket
            dSocket = new DatagramSocket(Integer.valueOf(argv[0]));
            // Set maximum timeout to 300 secs
            dSocket.setSoTimeout(300000);
            while (true) {
                // Prepare datagram for reception
                DatagramPacket packet = new DatagramPacket(buf, buf.length);

                // Receive the message
                dSocket.receive(packet);
                InetAddress clientIp = packet.getAddress();
                int clientPort = packet.getPort();
                packet = new DatagramPacket(buf, buf.length, clientIp, clientPort);

                String msgIn = new String(packet.getData(), 0, packet.getLength());
                String [] partsIn = msgIn.split("\u0000");
                String msg = partsIn[0];

                System.out.println("SERVER: Received " + msg + " from " + clientIp + ":" + clientPort);
                // Prepare datagram to send response
                DatagramPacket dgramSent = new DatagramPacket(msg.getBytes(),
                        msg.getBytes().length, clientIp, clientPort);
                // Send response
                System.out.println("SERVER: Sending " + msg + " to " + clientIp + ":" + clientPort);
                dSocket.send(dgramSent);
            }
          
        // Uncomment next catch clause after implementing the logic
        } catch (SocketTimeoutException e) {
            System.err.println("No requests received in 300 secs ");
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            // Close the socket
            dSocket.close();
        }
    }
}
