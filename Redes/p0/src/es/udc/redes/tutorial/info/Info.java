package es.udc.redes.tutorial.info;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.DosFileAttributes;
import java.nio.file.attribute.FileTime;

public class Info {
    public static String getFileType(String fileName){
        String fileType = "Undetermined";
        final File file = new File(fileName);
        try{
            fileType = Files.probeContentType(file.toPath());
        } catch (IOException ioException){
            System.out.println("Tipo no detectado" + fileName);
        }
        return fileType;
    }

    public static void main(String[] args) {
        Path path = Paths.get(args[0]);
        File f = new File(path.toString());

        try {
            long bytes = Files.size(path);
            FileTime lastMod = Files.getLastModifiedTime(path);
            String fileName = path.getFileName().toString();
            String[] parts = fileName.split("\\.");
            String type = getFileType(fileName);
            String absolute = f.getAbsolutePath();

            System.out.println(String.format("Size: %,d bytes", bytes));
            System.out.println("Last mod: " + lastMod);
            System.out.println("Name: " + parts[0]);
            System.out.println("Extension: ." + parts[1]);
            System.out.println("Type: " + type);
            System.out.println(absolute);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
