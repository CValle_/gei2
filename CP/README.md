
# Básicos de paralelismo

## Estructura de un programa
* **MPMD** (Multiple Program Multiple Data): cada proceso/tarea tiene su **propio programa con comunicaciones asíncronas** entre ellos.
* **SPMD** (Single Program Multiple Data): todos los procesos/tareas **comparten un mismo programa/binario** aunque en su lógica interna las tareas se pueden ejecutar de forma condicional dependiendo del proceso.

## MPI

* `mpicc mpi-hello.c -o mpi-hello` to compile.
* `mpirun -np 4 ./mpi-hello` to execute.

* `int MPI_Init (int *argc, char ***argv)`: Initialize the MPI execution environment.
    * argc: pointer to the number of arguments   
    * argv: pointer to the argument vector
* `int MPI_Comm_size(MPI_Comm comm, int *size)`: Determines the size of the group associated with a communicator.
    * comm: communicator (handle)
    * size: number of processes in the group of comm (integer)
* `int MPI_Comm_rank(MPI_Comm comm, int *rank)`: Determines the rank of the calling process in the communicator
    * comm: communicator (handle)
    * rank: rank of the calling process in the group of comm (integer)
* `int MPI_Finalize( void )`: Terminates MPI execution environment
* `MPI_COMM_WORLD`: global communicator, includes all processes.

### Operaciones punto a punto (De un proceso de origen a un proceso destino)

* `MPI_Send(void* data, int count, MPI_Datatype datatype, int destination, int tag, MPI_Comm communicator)`
    * Envía un mensaje al proceso `dest` en el comunicador `comm`
    * El mensaje está almacenado en `buff` y consta de al menos `count` items del tipo `datatype`
    * El mensaje está etiquetado con un `tag`
    * La llamada a MPI Send finaliza cuando `buff` puede ser reusado (generalmente cuando el mensaje ha sido recibido en el destino)
* `MPI_Recv(void* data, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm communicator, MPI_Status* status)`
    * Recibe un mensaje del proceso `source` del comunicador `comm` con la etiqueta `tag`
        * Puede recibir de cualquier proceso con `MPI_ANY_SOURCE`
        * Puede recibir con cualquier etiqueta con `MPI_ANY_TAG`
    * Los datos *source* o *tag* se recuperan con `status.MPI_SOURCE` o `status.MPI_TAG`
    * El mensaje se recibe en `buff` y consta de un máximo de `count` items del tipo `datatype`
    * La llamada a *MPI Recv* finaliza cuando se ha recibido el mensaje en buff


#### Tipos de datos MPI 


|      MPI datatype      |      C equivalent      |
|------------------------|------------------------|
| MPI_SHORT              | short int              |
| MPI_INT                | int                    |
| MPI_LONG               | long int               |
| MPI_LONG_LONG          | long long int          |
| MPI_UNSIGNED_CHAR      | unsigned char          |
| MPI_UNSIGNED_SHORT     | unsigned short int     |
| MPI_UNSIGNED           | unsigned int           |
| MPI_UNSIGNED_LONG      | unsigned long int      |
| MPI_UNSIGNED_LONG_LONG | unsigned long long int |
| MPI_FLOAT              | float                  |
| MPI_DOUBLE             | double                 |
| MPI_LONG_DOUBLE        | long double            |
| MPI_BYTE               | char                   |

### Operaciones colectivas

* `int MPI_Barrier(MPI_Comm comm)`: establece una barrera que bloquea el programa hasta que todos los procesos han alcanzado esta rutina.
* `int MPI_Bcast(void *buf, int count, MPI_Datatype datatype, int root , MPI_Comm comm)`: comunicación uno a todos de *count* datos del tipo *datatype* desde el proceso raíz (*root*) al resto de procesos del comunicador *comm*.
<p align="center">
    <img src="https://i.ibb.co/x5r3F5K/image.png" alt="MPI_Bcast" style="width:40%;">
</p>

* `MPI_Scatter(void *buff, int sendcnt, MPI_Datatype sendtype, void *recvbuff , int recvcnt , MPI_Datatype recvtype , int root , MPI_Comm comm)`: comunicación uno a todos de *count* datos del tipo *datatype* desde el proceso raíz (*root*) al resto de procesos del comunicador *comm*.

<p align="center">
    <img src="https://i.ibb.co/XJ1CqL7/image.png" alt="MPI_Scatter" style="width:40%;">
</p>


* `MPI_Gather(void *buff, int sendcnt, MPI_Datatype sendtype, void *recvbuff , int recvcnt , MPI_Datatype recvtype , int root , MPI_Comm comm)`: recibe en el proceso *root*, en *recvbuff*, *recvcnt* elementos de tipo *recvtype* desde todos los procesos del comunicador *comm*.

<p align="center">
    <img src="https://i.ibb.co/mt6T45h/image.png" alt="MPI_Gather" style="width:40%;">
</p>


* `int MPI_Reduce(void *buff, void *recvbuff, int count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm)`: realiza una reducción todos a uno, reduciendo los datos de *buff*, *count* elementos de tipo *datatype*, y guardando el resultado en *recvbuff* del proceso root.
    * Operaciones *op* disponibles:
        * MPI {MAX, MIN, SUM, PROD}
        * MPI {LAND, LOR, LEXOR}
        * MPI {BAND, BOR, BXOR} 
        * MPI {MAXLOC, MINLOC}

<p align="center">
    <img src="https://i.ibb.co/1zxY268/image.png" alt="MPI_Reduce" style="width:40%;">
</p>

* `int MPI_Scatterv(const void *sendbuf, const int *sendcounts, const int *displs, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm)`
    * sendbuf: address of send buffer (choice, significant only at root)
    * sendcounts: integer array (of length group size) specifying the number of elements to send to each processor
    * displs: integer array (of length group size). Entry i specifies the displacement (relative to sendbuf from which to take the outgoing data to process i)
    * sendtype: data type of send buffer elements (handle)
    * revcount: number of elements in receive buffer (integer)
    * recvtype: data type of receive buffer elements (handle)
    * root: rank of sending process (integer)
    * comm: communicator (handle)




> Ejemplo de `MPI_Bcast`
```
#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv) {
    int rank, size;
    int data = 42;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    MPI_Bcast(&data, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (rank == 0) {
        // Proceso raíz
        printf("Proceso %d envió el dato %d a los demás procesos\n", rank, data);
    } else {
        // Otros procesos
        printf("Proceso %d recibió el dato %d del proceso raíz\n", rank, data);
    }

    MPI_Finalize();
    return 0;
}
```

> Ejemplo de `MPI_Scatter` y `MPI_Gather`
```
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    int size, rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int array_size = 10;
    int array[array_size];
    float local_sum = 0.0;
    float global_sum = 0.0;

    if (rank == 0) {
        // Inicializamos el array en el proceso raíz
        for (int i = 0; i < array_size; i++) {
            array[i] = i;
        }
    }

    // Enviamos partes del array a cada proceso
    int sendcount = array_size / size;
    int recvbuf[sendcount];

    MPI_Scatter(array, sendcount, MPI_INT, recvbuf, sendcount, MPI_INT, 0, MPI_COMM_WORLD);

    // Calculamos el promedio local
    for (int i = 0; i < sendcount; i++) {
        local_sum += recvbuf[i];
    }
    float local_average = local_sum / sendcount;

    // Recolectamos los promedios locales en el proceso raíz
    MPI_Gather(&local_average, 1, MPI_FLOAT, &global_sum, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);

    if (rank == 0) {
        // Calculamos el promedio global e imprimimos el resultado
        global_sum /= size;
        printf("El promedio del array es: %f\n", global_sum);
    }

    MPI_Finalize();
    return 0;
}
```
