#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <mpi.h>
#include <math.h>

#define DEBUG 1

/* Translation of the DNA bases
   A -> 0
   C -> 1
   G -> 2
   T -> 3
   N -> 4*/

#define M  1000000 // Number of sequences
#define N  200  // Number of bases per sequence

unsigned int g_seed = 0;

int fast_rand(void) {
    g_seed = (214013*g_seed+2531011);
    return (g_seed>>16) % 5;
}

/* The distance between two bases*/
int base_distance(int base1, int base2){

  if((base1 == 4) || (base2 == 4)){
    return 3;
  }

  if(base1 == base2) {
    return 0;
  }

  if((base1 == 0) && (base2 == 3)) {
    return 1;
  }

  if((base2 == 0) && (base1 == 3)) {
    return 1;
  }

  if((base1 == 1) && (base2 == 2)) {
    return 1;
  }

  if((base2 == 2) && (base1 == 1)) {
    return 1;
  }

  return 2;
}

int main(int argc, char *argv[] ) {

  int i, j;
  int *sendcounts, *displs; 
  int *data1, *data2;
  int *recv_data1, *recv_data2;
  int *result, *global_result;
  int numprocs, rank, namelen;
  int procs_div, procs_mod;
  int comm_time, calc_time, total_comm_time, total_calc_time;


  struct timeval  tv1, tv2; 

  MPI_Init(&argc, &argv); 
  MPI_Comm_size(MPI_COMM_WORLD , &numprocs); // Number of processes
  MPI_Comm_rank(MPI_COMM_WORLD , &rank); // Rank of the calling process


  /* Allocate memory */
  sendcounts = (int *) malloc(numprocs*sizeof(int));
  displs = (int *) malloc(numprocs*sizeof(int));
  data1 = (int *) malloc(M*N*sizeof(int));
  data2 = (int *) malloc(M*N*sizeof(int));
  recv_data1 = (int *) malloc(M*N*sizeof(int));
  recv_data2 = (int *) malloc(M*N*sizeof(int));
  result = (int *) malloc(M*sizeof(int));
  global_result = (int *) malloc(M*sizeof(int));
  
  procs_div = M / numprocs; // Compute the number of rows to distribute to each process
  procs_mod = M % numprocs; // Compute the remainder of rows that can't be evenly distributed

  if(rank == 0){
    /* Initialize Matrices */
    for(i=0;i<M;i++) {
      for(j=0;j<N;j++) {
        /* random with 20% gap proportion */
        data1[i*N+j] = fast_rand();
        data2[i*N+j] = fast_rand();
      }
    }

    displs[0] = 0; // Initialize displacement of process 0 to 0

    /* Calculate number of elements to send to each process and their displacements (Scaterv)*/
    for (i = 0; i < numprocs; i++) {
      if (i < procs_mod) { // if the process number is less than the remainder after evenly distributing elements, give it an extra row
        sendcounts[i] = (procs_div + 1) * N;
      } else { 
        sendcounts[i] = procs_div * N;
      }
      if (i != 0) { // Calculate displacement for each process based on previous process displacement and send count, except for process 0.
        displs[i] = displs[i-1] + sendcounts[i-1];
      }
    }
  }

  /* Elements to be sent by each specific process */
  if(rank < procs_mod){
    sendcounts[rank] = (procs_div + 1) * N;
  } else {
    sendcounts[rank] = procs_div * N;
  }

  gettimeofday(&tv1, NULL);
  MPI_Scatterv(data1, sendcounts, displs,  MPI_INT, recv_data1, sendcounts[rank], MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Scatterv(data2, sendcounts, displs,  MPI_INT, recv_data2, sendcounts[rank], MPI_INT, 0, MPI_COMM_WORLD);
  gettimeofday(&tv2, NULL);


  comm_time = (tv2.tv_usec - tv1.tv_usec)+ 1000000 * (tv2.tv_sec - tv1.tv_sec); //Scatterv time

  gettimeofday(&tv1, NULL);

  for(i = 0;i < sendcounts[rank]/ N; i++) {
    result[i] = 0;
    for(j = 0;j < N;j++) {
      result[i] += base_distance(recv_data1[i*N+j], recv_data2[i*N+j]);
    }
  }

  gettimeofday(&tv2, NULL);

  calc_time = (tv2.tv_usec - tv1.tv_usec)+ 1000000 * (tv2.tv_sec - tv1.tv_sec); //Calc time
  

  /* Calculate number of elements to send to each process and their displacements (Gatherv)*/
  if(rank == 0){
    for(i = 0;i < numprocs;i++){
      if(i < procs_mod){
        sendcounts[i] = procs_div + 1;
      } else {
        sendcounts[i] = procs_div;
      }
      if(i != 0)
        displs[i] = displs[i-1] + sendcounts[i-1];
    }
  }

  if(rank < procs_mod){
    sendcounts[rank] = procs_div + 1;
  } else {
    sendcounts[rank] = procs_div;
  }


  gettimeofday(&tv1, NULL); 
  MPI_Gatherv(result, sendcounts[rank], MPI_INT, global_result, sendcounts, displs, MPI_INT, 0, MPI_COMM_WORLD); //Get results
  gettimeofday(&tv2, NULL);

  comm_time += (tv2.tv_usec - tv1.tv_usec)+ 1000000 * (tv2.tv_sec - tv1.tv_sec); //Scatterv + Gatherv time

  //printf("Process %d: Calc time (seconds) = %lf\n", rank, (double) calc_time/1E6);
  //printf("Process %d: Comm time (seconds) = %lf\n", rank, (double) comm_time/1E6);

  /* Time communications */
  MPI_Reduce(&comm_time, &total_comm_time, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&calc_time, &total_calc_time, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);


  if(rank == 0){
    /* Display result */
    if (DEBUG == 1) {
      int checksum = 0;
      for(i=0;i<M;i++) {
        checksum += global_result[i];
      }
      printf("Checksum: %d\n ", checksum);
    } else if (DEBUG == 2) {
      for(i=0;i<M;i++) {
        printf(" %d \t ",global_result[i]);
      }
    } else {
      /* Time results */
      
      //printf("Total calculation time (seconds) = %lf\n", total_calc_time/ 1E6); 
      //printf("Total communication time (seconds) = %lf\n", total_comm_time/ 1E6);
      
      printf("Average communication time (seconds) = %lf\n", (total_comm_time / numprocs) / 1E6);
      printf("Average calculation time (seconds) = %lf\n\n", (total_calc_time / numprocs) / 1E6);
      
    }    
  }

  free(data1); free(data2); free(result);
  free(recv_data1); free(recv_data2); free(global_result);
  free(sendcounts); free(displs);

  MPI_Finalize();


  return 0;
}
