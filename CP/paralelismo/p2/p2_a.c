#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

void inicializaCadena(char *cadena, int n){
    int i;
    for(i=0; i<n/2; i++){
        cadena[i] = 'A';
    }
    for(i=n/2; i<3*n/4; i++){
        cadena[i] = 'C';
    }
    for(i=3*n/4; i<9*n/10; i++){
        cadena[i] = 'G';
    }
    for(i=9*n/10; i<n; i++){
        cadena[i] = 'T';
    }
}

int main(int argc, char *argv[])
{

    int numprocs, rank, namelen;
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Init(&argc, &argv); 
    MPI_Comm_size(MPI_COMM_WORLD , &numprocs); //El comunicador COMM_WORLD -> General y variable int que devuelve el numero de procesos
    MPI_Comm_rank(MPI_COMM_WORLD , &rank); // La rank dice que proceso estamos usando
    MPI_Get_processor_name(processor_name , &namelen);

    int i, n, count = 0, count_global = 0;
    char *cadena;
    char L; 

	

	if(argc != 3){
		printf("Numero incorrecto de parametros\n"
               "La sintaxis debe ser: program n L\n  program es el nombre del ejecutable\n"
               "  n es el tamaño de la cadena a generar\n  L es la letra de la que se quiere contar apariciones (A, C, G o T)\n");
		exit(1); 
	}

    if(rank == 0){
        n = atoi(argv[1]);
		L = *argv[2];
        cadena = (char *) malloc(n*sizeof(char));
        inicializaCadena(cadena, n);

    } 

    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if(rank != 0){
        cadena = (char *) malloc(n*sizeof(char));
    }
    MPI_Bcast(&L, 1, MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Bcast(cadena, n, MPI_CHAR, 0, MPI_COMM_WORLD);

    
	for(i=rank; i<n; i+=numprocs){ //Cada proceso va contando en base a su num de proceso
		if(cadena[i] == L){
			count++;
		}
	}
	
	MPI_Reduce(&count, &count_global, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	if(rank == 0){
		printf("El numero de apariciones de la letra %c es %d\n", L, count_global);
	}

	MPI_Finalize(); //Espera por todos los procesos 
	free(cadena);
	exit(0);
}
 