#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>


int MPI_BinomialBcast(void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm){
    int numprocs, myrank, i, result;
    MPI_Comm_size(MPI_COMM_WORLD , &numprocs); 
    MPI_Comm_rank(MPI_COMM_WORLD , &myrank); 
    
    
    if(root == 0){
        if(myrank != 0){
            result = MPI_Recv(buffer, count, datatype, MPI_ANY_SOURCE, 0, comm, MPI_STATUS_IGNORE);
            if(result != MPI_SUCCESS){
                return result;
            }
        } 
        for(i = 1;(myrank + pow(2,i-1)) < numprocs;i++){
            if(myrank < pow(2,i-1)){
                result = MPI_Send(buffer, count, datatype, myrank + pow(2,i-1), 0, comm);
                if(result != MPI_SUCCESS){
                    return result;
                }
            }
        }
    } else {
        printf("Error: root distinto de 0");
        return MPI_ERR_ROOT;
    }
    
    return MPI_SUCCESS; 
}

int MPI_FlattreeColectiva(const void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype, MPI_Op op,int root, MPI_Comm comm){
    int numprocs, myrank, result, i, j;
    int local_buf = 0, current_buf = 0, remote_buf = 0;

    MPI_Comm_size(MPI_COMM_WORLD , &numprocs); 
    MPI_Comm_rank(MPI_COMM_WORLD , &myrank);

    if (count == 0) return MPI_SUCCESS; //Si count es 0 no hacemos nada

    //Comprobar posibles errores
    if(op != MPI_SUM){
        return MPI_ERR_OP;
    } else if (root != 0){
        return MPI_ERR_ROOT;
    } else if(datatype != MPI_INT){
        return MPI_ERR_TYPE;
    }

    
    for(i = 0; i < count; i++){
        //Suma local count
        current_buf = *(const int*)sendbuf + i * sizeof(datatype);
        local_buf += current_buf;
    }
    
    // Enviar la operación de reducción local al proceso raíz
    if (myrank != root) {
        result = MPI_Send(&local_buf, count, datatype, root, 0, comm);
        if(result != MPI_SUCCESS){
            return result;
        }
    } else {
        for(i = 0; i < numprocs; i++){
            if(i != root){
                result = MPI_Recv(&remote_buf, count, datatype, i, 0, comm, MPI_STATUS_IGNORE);
                if(result != MPI_SUCCESS){
                    return result;
                }
                for(j = 0; j < count; j++){
                    //Suma local general
                    current_buf = remote_buf + j * sizeof(datatype);
                    local_buf += remote_buf;
                }
            }
        }
    }
    // Copiar la operación de reducción global al búfer de recepción del proceso raíz
    *(int*) recvbuf = local_buf;

    return MPI_SUCCESS;
}


void inicializaCadena(char *cadena, int n){
    int i;
    for(i=0; i<n/2; i++){
        cadena[i] = 'A';
    }
    for(i=n/2; i<3*n/4; i++){
        cadena[i] = 'C';
    }
    for(i=3*n/4; i<9*n/10; i++){
        cadena[i] = 'G';
    }
    for(i=9*n/10; i<n; i++){
        cadena[i] = 'T';
    }
}

int main(int argc, char *argv[])
{

    int numprocs, rank, namelen;
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Init(&argc, &argv); 
    MPI_Comm_size(MPI_COMM_WORLD , &numprocs); //El comunicador COMM_WORLD -> General y variable int que devuelve el numero de procesos
    MPI_Comm_rank(MPI_COMM_WORLD , &rank); // La rank dice que proceso estamos usando
    MPI_Get_processor_name(processor_name , &namelen);

    int i, n, count = 0, count_global = 0;
    char *cadena;
    char L; 

	

	if(argc != 3){
		printf("Numero incorrecto de parametros\n"
               "La sintaxis debe ser: program n L\n  program es el nombre del ejecutable\n"
               "  n es el tamaño de la cadena a generar\n  L es la letra de la que se quiere contar apariciones (A, C, G o T)\n");
		exit(1); 
	}

    if(rank == 0){
        n = atoi(argv[1]);
		L = *argv[2];
        cadena = (char *) malloc(n*sizeof(char));
        inicializaCadena(cadena, n);

    } 

    MPI_BinomialBcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if(rank != 0){
        cadena = (char *) malloc(n*sizeof(char));
    }
    MPI_BinomialBcast(&L, 1, MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_BinomialBcast(cadena, n, MPI_CHAR, 0, MPI_COMM_WORLD);

	for(i=rank; i<n; i+=numprocs){ //Cada proceso va contando en base a su num de proceso
		if(cadena[i] == L){
			count++;
		}
	}
	MPI_FlattreeColectiva(&count, &count_global, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	if(rank == 0){
		printf("El numero de apariciones de la letra %c es %d\n", L, count_global);
	}

	MPI_Finalize(); //Espera por todos los procesos 
	free(cadena);
	exit(0);
}
 