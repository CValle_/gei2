#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

void inicializaCadena(char *cadena, int n){
	int i;
	for(i=0; i<n/2; i++){
		cadena[i] = 'A';
	}
	for(i=n/2; i<3*n/4; i++){
		cadena[i] = 'C';
	}
	for(i=3*n/4; i<9*n/10; i++){
		cadena[i] = 'G';
	}
	for(i=9*n/10; i<n; i++){
		cadena[i] = 'T';
	}
}

int main(int argc, char *argv[])
{
	if(argc != 3){
		printf("Numero incorrecto de parametros\n"
               "La sintaxis debe ser: program n L\n  program es el nombre del ejecutable\n"
               "  n es el tamaño de la cadena a generar\n  L es la letra de la que se quiere contar apariciones (A, C, G o T)\n");
		exit(1); 
	}

	int numprocs, rank, namelen;
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	MPI_Init(&argc, &argv); 
	MPI_Comm_size(MPI_COMM_WORLD , &numprocs); //El comunicador COMM_WORLD -> General y variable int que devuelve el numero de procesos
	MPI_Comm_rank(MPI_COMM_WORLD , &rank); // La rank dice que proceso estamos usando
	MPI_Get_processor_name(processor_name , &namelen);
		
	int i, n, count = 0, count_global = 0;
	char *cadena;
	char L; 


	if(rank == 0){
		n = atoi(argv[1]);
		L = *argv[2];
		cadena = (char *) malloc(n*sizeof(char));
		inicializaCadena(cadena, n);
		for(i = 1; i < numprocs; i++){
			MPI_Send(&n, 1, MPI_INT, i, 0, MPI_COMM_WORLD); // Enviamos letras string
			MPI_Send(&L, 1, MPI_CHAR, i, 0, MPI_COMM_WORLD); //Enviamos 
			MPI_Send(cadena, n, MPI_CHAR, i, 0, MPI_COMM_WORLD);
		}
	} else {
		MPI_Recv(&n, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(&L, 1, MPI_CHAR, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		cadena = (char *) malloc(n*sizeof(char));
		MPI_Recv(cadena, n, MPI_CHAR, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

	
	for(i=rank; i<n; i+=numprocs){ //Cada proceso va contando en base a su num de proceso
		if(cadena[i] == L){
			count++;
		}
	}

	
	if(rank == 0){
		count_global = count; //Asignamos a count_global el count del proceso 0
		for(i = 1; i< numprocs; i++){
			MPI_Recv(&count, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); //El proceso 0 recibe de todos los proceso
			count_global += count;		
		}
		
		printf("El numero de apariciones de la letra %c es %d\n", L, count_global);

	} else {
		MPI_Send(&count, 1, MPI_INT, 0, 0, MPI_COMM_WORLD); //Todos los procesos menos el 0 envian
	}
	
	MPI_Finalize(); //Espera por todos los procesos 
	free(cadena);
	exit(0);
}
