\documentclass[a4paper, 12pt, titlepage]{extarticle}
\newcommand\tab[1][1cm]{\hspace*{#1}}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[spanish]{babel}
\usepackage{fancyhdr}

\setlength\parindent{0pt}
%deja un margen bonito entre footnote y texto
%\setlength{\skip\footins}{1.1cm}
\usepackage{spacingtricks}

%math
\usepackage{amsmath, mathtools, amssymb, amsfonts, mathrsfs}
\usepackage{pifont}
\usepackage{array}
\usepackage{multicol}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,
    filecolor=magenta,      
    urlcolor=blue,
    }

\usepackage{graphicx}
\graphicspath{ {media/} }

\usepackage{color, xcolor, soul}
\usepackage{listings}

\definecolor{Lilac}{HTML}{c3cde6}
\definecolor{mGray}{rgb}{0.5,0.5,0.5}
\definecolor{backgroundColour}{rgb}{0.95,0.95,0.92}

\lstset{escapeinside={{*@}{@*}}}

\lstdefinelanguage{pseudo}{
    classoffset = 1,
    morekeywords = {lock, unlock, return, mtx_lock, mtx_unlock, sem_t, sem_init, sem_destroy, sem_wait, sem_post, if, exit},
    morecomment=[l]{//},
    morecomment=[s]{/*}{*/}
}

\lstdefinestyle{pseudocode}{
    basicstyle=\ttfamily\footnotesize,
    language = pseudo,
    commentstyle=\color{mGray},
    keywordstyle=\color{blue},
    stringstyle=\color{Lilac},
    breakatwhitespace=false,       
    breaklines=true
}

\def\code#1{\texttt{#1}}
\newcommand{\hlfancy}[2]{\sethlcolor{#1}\hl{#2}}
\def\boxed#1{\fbox{\parbox{\textwidth}{#1}}}
\newcommand{\cmark}{\ding{51}} %needs pifont
\newcommand{\xmark}{\ding{55}}

%--- MAKETITLE ------------------------------------------------------------------------------

\def\myauthor{Clara Valle Gómez}
\author{\myauthor}

\title{
    \begin{Huge}
        {\textbf{Concurrencia y Paralelismo}}\\
    \end{Huge}
    \vspace{50px}
    \textbf{Tema I}\\
    Principios de programación concurrente\\
    \href{https://guiadocente.udc.es/guia_docent/index.php?centre=614&ensenyament=614G01&assignatura=614G01018&fitxa_apartat=3&any_academic=2022_23&idioma_assig=}{Guia docente: Contidos}
    \date{\today}%\today
}

%--- CABECERA Y PIE DE PÁGINA -------------------------------------------------------------------------
\setlength{\headheight}{16.00pt} %warning
\pagestyle{fancy}
\lhead{CP - T1}
\chead{}
\rhead{C.Valle} 
\lfoot{}
\cfoot{}
\rfoot{\thepage}    

%--- DOCUMENTO -------------------------------------------------------------------------------

\begin{document}
    \maketitle

    
%--- ÍNDICE -------------------------------------------------------------------------
    \newpage
    \tableofcontents

%--- DOC ----------------------------------------------------------------------------   
    \newpage
    \section{Introducción a la Concurrencia}
    \boxed{Un programa concurrente está diseñado para ejecutar varias tareas posiblemente distintas de forma simultánea, para un uso eficiente del procesador.}\\

    Permite aprovechar recursos, por ejemplo cuando hay mucha E/S (mientras un proceso espera, otro puede usar el procesador) en los siguientes sistemas:

    \begin{itemize}
        \item \textbf{Sistemas multiprocesador}: más de un procesador (CPU) en un mismo sistema (cada uno en un circuito integrado).
    \end{itemize}

    \begin{center}
        \includegraphics[width=260px]{sistemaMultiprocesador.png}
    \end{center}

    \begin{itemize}
        \item \textbf{Sistemas multinúcleo}: cada procesador tiene varios núcleos contenidos (en un mismo circuito integrado, con sus cachés y el bus)
    \end{itemize}

    \begin{center}
        \includegraphics[width=260px]{sistemaMultinucleo.png}
    \end{center}

    \begin{center}
        \footnotesize
        En la figura se muestra un sistema multiprocesador y multinúcleo
    \end{center}

    Esto ocasiona dos problemas: el acceso a recursos compartidos y comunicar las tareas entre si. Solución: Multithreading.

    \subsection{Multithreading VS Procesos}

    Con multithreading cada núcleo puede ejecutar más de una tarea concurrentemente. Hay dos tipos:

    \begin{itemize}
        \item \textbf{Temporal}: El núcleo cambia periódicamente la tarea que se está ejecutando, permitiendo usar el procesador donde habría burbujas.
        \item \textbf{Simultánea}: El núcleo ejecuta varias tareas al mismo tiempo. Requiere duplicar partes del núcleo.
    \end{itemize}

    \begin{multicols}{2}
        \textbf{Procesos}
        \begin{itemize}
            \itemsep0px
            \item Es una instancia de la ejecución de un programa.
            \item Puede haber más de un proceso en un programa
            \item Cada uno tiene sus propios recursos (memoria, registros, etc)
            \item \textbf{Un proceso nuevo copia los recursos del padre}
        \end{itemize}
        \columnbreak
        \textbf{Threads}
        \begin{itemize}
            \itemsep0px
            \item Es un hilo de ejecución dentro de un proceso
            \item Puede haber más de un hilo en un proceso
            \item Comparten recursos de un mismo proceso (memoria: la tabla de páginas es la misma, \textbf{pero tienen un stack propio}, registros, etc)
        \end{itemize} 
    \end{multicols}
    \vspace{0.2px}
    \begin{multicols}{2}
        \begin{center}
            \includegraphics[width=45px]{procVD.png}
        \end{center}
        \columnbreak
        \begin{center}
            \includegraphics[width=45px]{threadVD.png}
        \end{center} 
    \end{multicols}
    
    \begin{center}
        \scriptsize
        El \textbf{Stack} contiene las variables locales/parámetros. \textbf{MAP} espacio de direcciones virtuales (memoria). \textbf{Heap} la memoria dinámica (variables globales y estáticas). \textbf{Data} las variables globales y constantes.
    \end{center}

    Existen técnicas para comunicar threads y procesos: \textbf{Memoria Compartida}

    \subsection{Memoria Compartida}
    \begin{itemize}
        \itemsep0px
        \item \textbf{Threads}: Todos los hilos de un mismo proceso comparten tabla de páginas (mismo espacio de direcciones virtual), así como heap (una variable global es vista por todos los threads) y datos. El stack es privado de cada thread (las variables locales no se comparten)
        \footnote{Al compartir TP podría acceder a través de un puntero (peligro de sobreescritura)}.
        \item \textbf{Procesos}: Cada proceso tiene su propia tabla de páginas, el hijo copia la del padre (copy on write, por defecto no se comparte nada), hay que crear zonas de memoria compartida de forma explícita con MMAP: \texttt{mmap(addr, size, prot, flags, fd, offset)}.
        \footnote{\href{https://man7.org/linux/man-pages/man2/mmap.2.html}{Manual mmap}}
    \end{itemize}

    \section{Sección crítica y exclusión mutua}
    \boxed{Una \textbf{sección crítica} es una parte del código que accede a un recurso compartido con otro hilo. Podría producir inconsistencias (lost update)\footnotemark.} \footnotetext{Una \textbf{Lost update} se produce cuando dos hilos modificando una variable global se solapan y esta solo guarda el valor del último hilo ejecutado}

\begin{lstlisting}[style=pseudocode]
            int i = 0; //v. global

            int increment () {
                //Critical section
                int v = i; //v. local
                v++;
                i = v;
                return 0;
            }

            void main() {
                thread_t thr;
                thrd_create(&thr, *@\textcolor{red}{increment}@*, NULL);
            }
\end{lstlisting}

\boxed{La \textbf{exclusión mutua} es un mecanismo que evita que dos o más hilos accedan a la sección crítica al mismo tiempo. Hay que gestionar los accesos.}\\

Para gestionar los accesos del hilo a esta parte del código usaremos \code{lock} y \code{unlock}. El primer thread que llegue al \code{lock} bloquea el acceso a los demás, cuando este haga \code{unlock} se permite el paso al siguiente. Esto es un mutex.\\

\subsection{Mutex}

\boxed{Un \textbf{mutex} es una zona de acceso compartido en la que solo puede haber un hilo ejecutándose a la vez y el resto esperan hasta que se desbloquea.}\\

\begin{multicols}{2}

    \begin{lstlisting}[style=pseudocode]
        //Thread1
        int increment () {*@\textcolor{red}{\\mtx\_lock();\\
            int v = i;\\
            v++;\\
            i = v;\\
            mtx\_unlock();
            }@*
            return 0;
        }
    \end{lstlisting}
    \columnbreak
    \begin{lstlisting}[style=pseudocode]
        //Thread2
        int increment () {
            *@\textcolor{red}{mtx\_lock();}@*
            int v = i;
            v++;
            i = v;
            mtx_unlock();
            return 0;
        }
    \end{lstlisting}
    \end{multicols}

En la librería \code{threads.h} podemos encontrar diferentes tipos de mutex para \textbf{proteger las variables globales}:

\begin{itemize}
    \footnotesize
    \item \code{mtx\_lock(mtx\_t *m)}, \code{mtx\_unlock(mtx\_t *m)}
    \item \code{mtx\_trylock(mtx\_t *m)}: Intenta bloquear el mutex, si no puede devuelve un error
    \item \code{mtx\_timedlock(mtx\_t *m, struct timespec *time)}: Bloquea el hilo con un tiempo máximo de espera para resolver el mutex
\end{itemize}

Hay procesadores que proporcionan instrucciones que se ejecutan de forman de forma atómica (se ven como una única operación). Estas instrucciones se usan para implementar bloqueos.

\subsection{Instrucciones atómicas: Test and Set}

\code{test\_and\_set} es una instucción atómica que lee un valor de memoria, lo guarda en un registro y almacena un valor distinto de cero en dicha dirección de memoria en `una única instrucción' (se puede usar para implemetar mutex, ya que normalmente devuelve 1 que se usa para salir de un \code{while}).


\subsection{Semáforos}
\boxed{Los \textbf{semáforos} son otro mecanismo de acceso a la sección crítica. Un semáforo es un contador que se decrementa al bloquearlo y se incrementa al liberarlo. Cuando el valor del semáforo es 0, el hilo se bloquea.}\\

Un semáforo contiene las instrucciones P(bloquear), V(liberar), son funciones atómicas. La estructura \code{sem\_t *sem} tiene que estar compartida entre hilos.

\begin{multicols}{2}
\begin{lstlisting}[style=pseudocode]
    int main() {
        //shared mem
        sem_t *sem; int i;
    
        sem_init(sem, 1, 1);
        if (fork()==0){
            proc_fun(sem, i);
            exit(0);
        }
        if (fork()==0){
            proc_fun(sem, i);
            exit(0);
        }
        sem_destroy(sem);
        return 0;
    }
\end{lstlisting}
    \columnbreak
\begin{lstlisting}[style=pseudocode]
    void proc_fun(sem, i) {
        sem_wait(sem);
        i++;
        sem_post(sem);
    }
\end{lstlisting}
\end{multicols}

\section{Interbloqueo e inanición}
\boxed{El \textbf{interbloqueo} o \textbf{deadlock} es una situación en la que dos (o más) procesos están esperando por un recurso que tiene ocupado el otro de forma indefinida.}\\

\begin{multicols}{2}
    \begin{lstlisting}[style=pseudocode]
    //Thread1
    int add(v1, m1, v2, m2) {
        *@\textcolor{red}{mtx\_lock(\&m1);}@*
        *@\textcolor{orange}{mtx\_lock(\&m2);}@*
        //critical section
        int x = v1 + v2;
        mtx_unlock(&m2);
        mtx_unlock(&m1);
        return x;
    }
    \end{lstlisting}
        \columnbreak
\begin{lstlisting}[style=pseudocode]
    //Thread2
    int add(v1, m1, v2, m2) {
        *@\textcolor{orange}{mtx\_lock(\&m2);}@*
        *@\textcolor{red}{mtx\_lock(\&m1);}@*
        //critical section
        int x = v2 + v1;
        mtx_unlock(&m1);
        mtx_unlock(&m2);
        return x;
    }
\end{lstlisting}
    \end{multicols}

    \begin{center}
        \scriptsize Ambos thread están esperando por un mutex que tiene el otro (INTERBLOQUEO)
    \end{center}

    Condiciones necesarias para que se produzca interbloqueo:

    \begin{itemize}
        \item \textbf{Exclusión mutua}: Existen recursos que no se comparten.
        \item \textbf{Hold and wait}: Mantiene recursos reservados mientras espera otros.
        \item \textbf{No apropiación}: No se liberan recursos hasta que el proceso termine.
        \item \textbf{Espera circular}: Los procesos esperan por recursos reservados por procesos que esperan los primeros.
    \end{itemize}

    Negar cualquiera de las condiciones anteriores lo evita. Se pueden usar diferentes técnicas:

    \begin{itemize}
        \item \textbf{Detección}: Permite que ocurra el interbloqueo y el SO lo corrige después (hoy en dia no pasa, el SO ignoraría el problema).
        \item \textbf{Prevención}: Trata de evitar alguna de las 4 condiciones necesarias:
        \begin{itemize}
            \item \textbf{Exclusión mutua}: Evitar que los procesos tengan acceso exclusivo a los recursos (esencial para concurrencia) \xmark
            \item \textbf{Hold and wait}: Evitar que los procesos esperen por recursos (reservando todo a la vez, con \code{trylock}) \cmark
            \item \textbf{No apropiación}: Conllevaría implementar un roll-back (matar procesos) \xmark
            \item \textbf{Espera circular}: Se puede evitar con una reserva ordenada de recursos \cmark
        \end{itemize}
    \end{itemize}

    \begin{multicols}{2}
    \begin{lstlisting}[style=pseudocode]
//Sol hold and wait
int add(v1, m1, v2, m2) {
    while(1){
        *@\textcolor{red}{mtx\_lock(m1);}@*
        if(*@\textcolor{red}{mtx\_trylock(m2)=fail}@*){
            mtx_unlock(m1);
            continue;
        }
        //critical section
        int x = v1 + v2;
        mtx_unlock(m1);
        mtx_unlock(m2);
        break;
    }
    return x;
}
    \end{lstlisting}
            \columnbreak
    \begin{lstlisting}[style=pseudocode]
    //Sol espera circular
    int add(v1, m1, v2, m2) {
        if (ord1 < ord2) {
            *@\textcolor{red}{mtx\_lock(m1);}@*
            *@\textcolor{red}{mtx\_lock(m2);}@*
        } else {
            *@\textcolor{red}{mtx\_lock(m2);}@*
            *@\textcolor{red}{mtx\_lock(m1);}@*
        }
        //critical section
        int x = v1 + v2;
        mtx_unlock(m2);
        mtx_unlock(m1);
        return x;
    }
    \end{lstlisting}
        \end{multicols}

    \textbf{Cual es la mejor solución?}
    Cuando hay \hlfancy{Lilac}{mucha contencion} (hilos siempre ocupados) es mejor hacer las reservas por orden (con \code{trylock} se esperaría mucho tiempo). Cuando hay \hlfancy{Lilac}{poca contencion} es mejor con \code{trylock}.

    \subsection{Inanición}

    \boxed{La \textbf{inanición} es una situación en la que un proceso está esperando indefinidamente por recursos que necesita (generalmente muchos).}\\
    \begin{multicols}{2}
        \begin{lstlisting}[style=pseudocode]
//thread w many blocks
mtx *m1, m2, m3;
int v1, v2, v3;
int lot(void *args) {
    while(1){
        mtx_lock(m1);
        if (trylock(m2) &&
            trylock(m3)) {
            v1=v1+v2+v3;
            mtx_unlock(m1);
            mtx_unlock(m2);
            mtx_unlock(m3);
        } else {
            mtx_unlock(m1);
        }
    }
}
        \end{lstlisting}
                \columnbreak
    \begin{lstlisting}[style=pseudocode]
    //Threads blocking m
    void blockm1(void *args) {
        mtx_lock(m1);
        //use m1
        mtx_unlock(m1);
    }
    void blockm2(void *args) {
        mtx_lock(m2);
        //use m2
        mtx_unlock(m2);
    }
    void blockm3(void *args) {
        mtx_lock(m3);
        //use m3
        mtx_unlock(m3);
    }
    \end{lstlisting}
    \end{multicols}

    Un si hay muchos hilos bloqueando m1, m2 y m3 y un hilo necesita estos mutex, es muy probable que en todo momento alguno esté bloqueado, por lo que este thread no es capaz de entrar en su sección crítica.

    \section{Productores/Consumidores}

    Eso ocurre cuando existen dos tiposd e threads: uno que añade elementos a una cola compartida (\textbf{buffer}) y otro que los quita.\\
    
    Si el buffer está lleno, el productor debe esperar a que los consumidores eliminen elementos. Si el buffer se vacía, el consumidor debe esperar a que el productor añada elementos.\\

    Estas esperas son \textbf{esperas activas}, es decir compueban contínuamente si se puede realizar la operación. Esto tiene un coste muy alto de CPU, por lo que es mejor `suspender' un thread cuando su condición esté lista.

\end{document}




