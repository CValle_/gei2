#include <stdlib.h>
#include <threads.h>
#include <stdbool.h>
#include <stdio.h>

// circular array
typedef struct _queue {
    int size;
    int used;
    int first;
    void **data;
    mtx_t *mutex;
    bool *done;
    cnd_t *buffer_full;
    cnd_t *buffer_empty;
} _queue;

#include "queue.h"

queue q_create(int size) {
    queue q = malloc(sizeof(_queue));

    q->size  = size;
    q->used  = 0;
    q->first = 0;
    q->data  = malloc(size * sizeof(void *));
    q->mutex = malloc(sizeof(mtx_t));
    q->done = malloc(sizeof(bool));
    q->buffer_full = malloc(sizeof(cnd_t));
    q->buffer_empty = malloc(sizeof(cnd_t));

    mtx_init(q->mutex, mtx_plain);
    cnd_init(q->buffer_empty);
    cnd_init(q->buffer_full);
    


    *q->done = false;
    return q;
}

int q_elements(queue q) {
    int aux;
    mtx_lock(q->mutex);
    aux = q->used;
    mtx_unlock(q->mutex);
    return aux;
}

void q_insert_done(queue q){ 
    mtx_lock(q->mutex);
    (*q->done) = true;
    cnd_broadcast(q->buffer_empty);
    cnd_broadcast(q->buffer_full);
    mtx_unlock(q->mutex);
}

int q_insert(queue q, void *elem) {
    mtx_lock(q->mutex);
    while(q->size == q->used ){
        cnd_wait(q->buffer_full, q->mutex);
    }

    q->data[(q->first + q->used) % q->size] = elem;
    q->used++;


    cnd_broadcast(q->buffer_empty);
    mtx_unlock(q->mutex);
    return 0;
}

void *q_remove(queue q) {
    void *res;
    mtx_lock(q->mutex);
    while(q->used == 0 && !(*q->done)) { //Cola vacía y no done
        cnd_wait(q->buffer_empty,q->mutex);
    }  
    while(q->used == 0 && *q->done){
        mtx_unlock(q->mutex);
        return NULL;
    }   
    res = q->data[q->first];
    q->first = (q->first + 1) % q->size;
    q->used--;

    cnd_broadcast(q->buffer_full);
    mtx_unlock(q->mutex);

    return res;
}

void q_destroy(queue q) {
    mtx_destroy(q->mutex);
    free(q->data);
    free(q->mutex);
    free(q);
    
}
