#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <threads.h>
#include "options.h"

#define DELAY_SCALE 1000


struct array {
    int size;
    int *arr;
};

struct thr{
    thrd_t thr;
    struct args *args;
};

struct args {
    int id;
    int iterations;
    int delay;
    struct array *arr;
    mtx_t *mutex;
};


void apply_delay(int delay) {
    for(int i = 0; i < delay * DELAY_SCALE; i++); // waste time
}


int increment(int id, int iterations, int delay, struct array *arr, mtx_t *mutex)
{
    int pos, val;

    for(int i = 0; i < iterations; i++) {
        pos = rand() % arr->size;
        mtx_lock(&mutex[pos]);

        printf("%d increasing position %d\n", id, pos);

        val = arr->arr[pos];
        apply_delay(delay);

        val ++;
        apply_delay(delay);

        arr->arr[pos] = val;
        apply_delay(delay);
        mtx_unlock(&mutex[pos]);
        apply_delay(delay);
    }

    return 0;
}

int move_value(int id, int iterations, int delay, struct array *arr, mtx_t *mutex){
    int pos1, pos2, val1, val2;

    for(int i = 0; i < iterations; i++) {
        pos1 = rand() % arr->size;

        while(1){
            pos2 = rand() % arr->size;
            if(pos1 != pos2) {
                break;
            }
        }

        // deadlock
        while(1){
            mtx_lock(&mutex[pos1]);
            if(mtx_trylock(&mutex[pos2]) == thrd_success){
                printf("%d changing values %d and %d\n", id, pos1, pos2);

                val1 = arr->arr[pos1];
                val2 = arr->arr[pos2];
                apply_delay(delay);

                val1 --;
                val2 ++;
                apply_delay(delay);

                arr->arr[pos1] = val1;
                arr->arr[pos2] = val2;
                apply_delay(delay);

                mtx_unlock(&mutex[pos2]);
                mtx_unlock(&mutex[pos1]);
                apply_delay(delay);
                break;
            } else {
                mtx_unlock(&mutex[pos1]);
                apply_delay(delay);
            }
        }
    }

    return 0;
}


void print_array(struct array arr) {
    int total = 0;

    for(int i = 0; i < arr.size; i++) {
        total += arr.arr[i];
        printf("%d ", arr.arr[i]);
    }

    printf("\nTotal: %d\n", total);
}

int thread_function(void *p) {
    struct args *args = p;
    return increment(args->id, args->iterations, args->delay, args->arr, args->mutex);
}

int thread_function2(void *p) {
    struct args *args = p;
    return move_value(args->id, args->iterations, args->delay, args->arr, args->mutex);
}

int main (int argc, char **argv)
{
    struct options       opt;
    struct array         arr;

    srand(time(NULL));

    struct thr *threads;
    mtx_t *mutex;
    int i;


    // Default values for the options
    opt.num_threads  = 5;
    opt.size         = 10;
    opt.iterations   = 100;
    opt.delay        = 1000;

    read_options(argc, argv, &opt);


    arr.size = opt.size;
    arr.arr  = malloc(arr.size * sizeof(int));
    if (arr.arr == NULL){
        printf("ERROR: Not enough memory\n");
        exit(1);
    }

    memset(arr.arr, 0, arr.size * sizeof(int));

    //Threads

    threads = malloc(sizeof(struct thr) * (opt.num_threads * 2 + 1));
    if (threads == NULL){
        printf("ERROR: Not enough memory\n");
        exit(1);
    }
    mutex = malloc(sizeof(mtx_t) * arr.size);
    if (mutex == NULL){
        printf("ERROR: Not enough memory\n");
        exit(1);
    }

    for(i=0;i<arr.size;i++){
        if(mtx_init(&mutex[i], mtx_plain) == thrd_error){
            printf("Error: mtx_init\n");
            exit(1);
        }
    }


    for(i = 0; i < opt.num_threads * 2; i++){
        threads[i].args = malloc(sizeof(struct args));
        if (threads[i].args == NULL){
            printf("ERROR: Not enough memory\n");
            exit(1);
        }
        threads[i].args -> id = i;
        threads[i].args -> iterations = opt.iterations;
        threads[i].args -> delay = opt.delay;
        threads[i].args -> arr = &arr;
        threads[i].args -> mutex = mutex;
        if(i < opt.num_threads){
            if(thrd_create(&threads[i].thr,thread_function,threads[i].args) == thrd_error){
                printf("Error: thrd_create\n");
                exit(1);
            }
        } else {
            if(thrd_create(&threads[i].thr,thread_function2,threads[i].args) == thrd_error){
                printf("Error: thrd_create\n");
                exit(1);
            }
        }
    }
    for (i = 0; i < opt.num_threads * 2; i++){
		thrd_join(threads[i].thr,NULL);
    }


    print_array(arr);

    for(i=0; i < arr.size; i++){
        mtx_destroy(&mutex[i]);
    }
    for(i=0; i < opt.num_threads * 2; i++){
        free(threads[i].args);
    }
    free(threads);
    free(arr.arr);
    free(mutex);


    return 0;
}
