#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <threads.h>
#include "options.h"

#define DELAY_SCALE 1000


struct array {
    int size;
    int *arr;
};

struct thr{
    thrd_t thr;
    struct args *args;
};

struct args {
    int id;
    int iterations;
    int delay;
    struct array *arr;
    mtx_t *mutex;
};


void apply_delay(int delay) {
    for(int i = 0; i < delay * DELAY_SCALE; i++); // waste time
}


int increment(int id, int iterations, int delay, struct array *arr, mtx_t *mutex)
{
    int pos, val;

    for(int i = 0; i < iterations; i++) {
        mtx_lock(mutex);
        pos = rand() % arr->size;

        printf("%d increasing position %d\n", id, pos);

        val = arr->arr[pos];
        apply_delay(delay);

        val ++;
        apply_delay(delay);

        arr->arr[pos] = val;
        apply_delay(delay);
        mtx_unlock(mutex);
        apply_delay(delay);
    }

    return 0;
}


void print_array(struct array arr) {
    int total = 0;

    for(int i = 0; i < arr.size; i++) {
        total += arr.arr[i];
        printf("%d ", arr.arr[i]);
    }

    printf("\nTotal: %d\n", total);
}

int thread_function(void *p) {
    struct args *args = p;
    return increment(args->id, args->iterations, args->delay, args->arr, args->mutex);
}

int main (int argc, char **argv)
{
    struct options       opt;
    struct array         arr;

    srand(time(NULL));

    struct thr *threads;
    mtx_t *mutex;
    int i;


    // Default values for the options
    opt.num_threads  = 5;
    opt.size         = 10;
    opt.iterations   = 100;
    opt.delay        = 1000;

    read_options(argc, argv, &opt);


    arr.size = opt.size;
    arr.arr  = malloc(arr.size * sizeof(int));
    if (arr.arr == NULL){
        printf("ERROR: Not enough memory\n");
        exit(1);
    }

    memset(arr.arr, 0, arr.size * sizeof(int));

    //Threads

    threads = malloc(sizeof(struct thr) * (opt.num_threads + 1));
    if (threads == NULL){
        printf("ERROR: Not enough memory\n");
        exit(1);
    }
    mutex = malloc(sizeof(mtx_t));
    if (mutex == NULL){
        printf("ERROR: Not enough memory\n");
        exit(1);
    }

    if(mtx_init(mutex, mtx_plain) == thrd_error){
        printf("Error: mtx_init\n");
        exit(1);
    }

    for(i = 0; i < opt.num_threads; i++){
        threads[i].args = malloc(sizeof(struct args));
        if (threads[i].args == NULL){
            printf("ERROR: Not enough memory\n");
            exit(1);
        }
        threads[i].args -> id = i;
        threads[i].args -> iterations = opt.iterations;
        threads[i].args -> delay = opt.delay;
        threads[i].args -> arr = &arr;
        threads[i].args -> mutex = mutex;

        if(thrd_create(&threads[i].thr,thread_function,threads[i].args) == thrd_error){
	        printf("Error: thrd_create\n");
	        exit(1);
        }
    }
    for (i = 0; i < opt.num_threads; i++){
		thrd_join(threads[i].thr,NULL);
    }


    print_array(arr);

    mtx_destroy(mutex);
    for(i=0; i < opt.num_threads; i++){
        free(threads[i].args);
    }
    free(threads);
    free(arr.arr);
    free(mutex);


    return 0;
}
